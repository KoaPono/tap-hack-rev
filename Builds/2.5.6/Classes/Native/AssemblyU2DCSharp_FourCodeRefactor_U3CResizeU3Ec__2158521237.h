﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourCodeRefactor/<Resize>c__Iterator0
struct  U3CResizeU3Ec__Iterator0_t2158521237  : public Il2CppObject
{
public:
	// UnityEngine.GameObject FourCodeRefactor/<Resize>c__Iterator0::position
	GameObject_t1756533147 * ___position_0;
	// UnityEngine.Vector3 FourCodeRefactor/<Resize>c__Iterator0::<normalsize>__0
	Vector3_t2243707580  ___U3CnormalsizeU3E__0_1;
	// System.Single FourCodeRefactor/<Resize>c__Iterator0::<expandNumber>__0
	float ___U3CexpandNumberU3E__0_2;
	// UnityEngine.Vector3 FourCodeRefactor/<Resize>c__Iterator0::<expandedSize>__0
	Vector3_t2243707580  ___U3CexpandedSizeU3E__0_3;
	// System.Object FourCodeRefactor/<Resize>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean FourCodeRefactor/<Resize>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 FourCodeRefactor/<Resize>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___position_0)); }
	inline GameObject_t1756533147 * get_position_0() const { return ___position_0; }
	inline GameObject_t1756533147 ** get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(GameObject_t1756533147 * value)
	{
		___position_0 = value;
		Il2CppCodeGenWriteBarrier(&___position_0, value);
	}

	inline static int32_t get_offset_of_U3CnormalsizeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U3CnormalsizeU3E__0_1)); }
	inline Vector3_t2243707580  get_U3CnormalsizeU3E__0_1() const { return ___U3CnormalsizeU3E__0_1; }
	inline Vector3_t2243707580 * get_address_of_U3CnormalsizeU3E__0_1() { return &___U3CnormalsizeU3E__0_1; }
	inline void set_U3CnormalsizeU3E__0_1(Vector3_t2243707580  value)
	{
		___U3CnormalsizeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CexpandNumberU3E__0_2() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U3CexpandNumberU3E__0_2)); }
	inline float get_U3CexpandNumberU3E__0_2() const { return ___U3CexpandNumberU3E__0_2; }
	inline float* get_address_of_U3CexpandNumberU3E__0_2() { return &___U3CexpandNumberU3E__0_2; }
	inline void set_U3CexpandNumberU3E__0_2(float value)
	{
		___U3CexpandNumberU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CexpandedSizeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U3CexpandedSizeU3E__0_3)); }
	inline Vector3_t2243707580  get_U3CexpandedSizeU3E__0_3() const { return ___U3CexpandedSizeU3E__0_3; }
	inline Vector3_t2243707580 * get_address_of_U3CexpandedSizeU3E__0_3() { return &___U3CexpandedSizeU3E__0_3; }
	inline void set_U3CexpandedSizeU3E__0_3(Vector3_t2243707580  value)
	{
		___U3CexpandedSizeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResizeU3Ec__Iterator0_t2158521237, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
