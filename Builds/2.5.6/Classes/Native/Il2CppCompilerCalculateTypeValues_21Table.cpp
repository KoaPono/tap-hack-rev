﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AlphaButtonClickMask1798481236.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1108113546.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4021787953.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3608854452.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2592441618.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U245510835.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2688848816.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2107922160.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2127994057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4075247181.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U859033236.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EventSystemChecker3549249260.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1823436477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1277509062.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1597325334.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U584591591.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2968437806.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ForcedReset935499500.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3525149852.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1181024807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2090656575.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2926400505.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U190286178.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3769115865.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U935951820.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1911586150.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1317702990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ut32383032.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2548964113.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2871423837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U769078069.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U420114983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1135469853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ut56702769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U707185157.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1764217217.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3133065407.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2375210762.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2985503331.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U318924311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2206407592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1659392090.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (AlphaButtonClickMask_t1798481236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	AlphaButtonClickMask_t1798481236::get_offset_of__image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (AutoMobileShaderSwitch_t1108113546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	AutoMobileShaderSwitch_t1108113546::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (ReplacementDefinition_t4021787953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	ReplacementDefinition_t4021787953::get_offset_of_original_0(),
	ReplacementDefinition_t4021787953::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (ReplacementList_t3608854452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	ReplacementList_t3608854452::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (AutoMoveAndRotate_t2592441618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	AutoMoveAndRotate_t2592441618::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2592441618::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2592441618::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2592441618::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (Vector3andSpace_t245510835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[2] = 
{
	Vector3andSpace_t245510835::get_offset_of_value_0(),
	Vector3andSpace_t245510835::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (CameraRefocus_t2688848816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[5] = 
{
	CameraRefocus_t2688848816::get_offset_of_Camera_0(),
	CameraRefocus_t2688848816::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t2688848816::get_offset_of_Parent_2(),
	CameraRefocus_t2688848816::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t2688848816::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (CurveControlledBob_t2107922160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[9] = 
{
	CurveControlledBob_t2107922160::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2107922160::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2107922160::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2107922160::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2107922160::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2107922160::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2107922160::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2107922160::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2107922160::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (DragRigidbody_t2127994057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t2127994057::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4075247181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (DynamicShadowSettings_t859033236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[11] = 
{
	DynamicShadowSettings_t859033236::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t859033236::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t859033236::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t859033236::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t859033236::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t859033236::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t859033236::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t859033236::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t859033236::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t859033236::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t859033236::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (EventSystemChecker_t3549249260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (FOVKick_t1823436477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[6] = 
{
	FOVKick_t1823436477::get_offset_of_Camera_0(),
	FOVKick_t1823436477::get_offset_of_originalFov_1(),
	FOVKick_t1823436477::get_offset_of_FOVIncrease_2(),
	FOVKick_t1823436477::get_offset_of_TimeToIncrease_3(),
	FOVKick_t1823436477::get_offset_of_TimeToDecrease_4(),
	FOVKick_t1823436477::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t1277509062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1597325334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (FPSCounter_t584591591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[6] = 
{
	0,
	FPSCounter_t584591591::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t584591591::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t584591591::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t584591591::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (FollowTarget_t2968437806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[2] = 
{
	FollowTarget_t2968437806::get_offset_of_target_2(),
	FollowTarget_t2968437806::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (ForcedReset_t935499500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (LerpControlledBob_t3525149852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[3] = 
{
	LerpControlledBob_t3525149852::get_offset_of_BobDuration_0(),
	LerpControlledBob_t3525149852::get_offset_of_BobAmount_1(),
	LerpControlledBob_t3525149852::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1181024807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (ObjectResetter_t2090656575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	ObjectResetter_t2090656575::get_offset_of_originalPosition_2(),
	ObjectResetter_t2090656575::get_offset_of_originalRotation_3(),
	ObjectResetter_t2090656575::get_offset_of_originalStructure_4(),
	ObjectResetter_t2090656575::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t2926400505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (ParticleSystemDestroyer_t190286178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[4] = 
{
	ParticleSystemDestroyer_t190286178::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t190286178::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t190286178::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t190286178::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CStartU3Ec__Iterator0_t3769115865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[10] = 
{
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (PlatformSpecificContent_t935951820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	PlatformSpecificContent_t935951820::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t935951820::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t935951820::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t935951820::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (BuildTargetGroup_t1911586150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[3] = 
{
	BuildTargetGroup_t1911586150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (SimpleActivatorMenu_t1317702990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[3] = 
{
	SimpleActivatorMenu_t1317702990::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t1317702990::get_offset_of_objects_3(),
	SimpleActivatorMenu_t1317702990::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (SimpleMouseRotator_t32383032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[10] = 
{
	SimpleMouseRotator_t32383032::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t32383032::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t32383032::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t32383032::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t32383032::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t32383032::get_offset_of_relative_7(),
	SimpleMouseRotator_t32383032::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t32383032::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t32383032::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t32383032::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (SmoothFollow_t2548964113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[5] = 
{
	SmoothFollow_t2548964113::get_offset_of_target_2(),
	SmoothFollow_t2548964113::get_offset_of_distance_3(),
	SmoothFollow_t2548964113::get_offset_of_height_4(),
	SmoothFollow_t2548964113::get_offset_of_rotationDamping_5(),
	SmoothFollow_t2548964113::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (TimedObjectActivator_t2871423837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[1] = 
{
	TimedObjectActivator_t2871423837::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (Action_t769078069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2130[6] = 
{
	Action_t769078069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Entry_t420114983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[3] = 
{
	Entry_t420114983::get_offset_of_target_0(),
	Entry_t420114983::get_offset_of_action_1(),
	Entry_t420114983::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (Entries_t1135469853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[1] = 
{
	Entries_t1135469853::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (U3CActivateU3Ec__Iterator0_t56702769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[4] = 
{
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (U3CDeactivateU3Ec__Iterator1_t707185157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t1764217217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (TimedObjectDestructor_t3133065407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[2] = 
{
	TimedObjectDestructor_t3133065407::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t3133065407::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (WaypointCircuit_t2375210762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[16] = 
{
	WaypointCircuit_t2375210762::get_offset_of_waypointList_2(),
	WaypointCircuit_t2375210762::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t2375210762::get_offset_of_numPoints_4(),
	WaypointCircuit_t2375210762::get_offset_of_points_5(),
	WaypointCircuit_t2375210762::get_offset_of_distances_6(),
	WaypointCircuit_t2375210762::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t2375210762::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t2375210762::get_offset_of_p0n_9(),
	WaypointCircuit_t2375210762::get_offset_of_p1n_10(),
	WaypointCircuit_t2375210762::get_offset_of_p2n_11(),
	WaypointCircuit_t2375210762::get_offset_of_p3n_12(),
	WaypointCircuit_t2375210762::get_offset_of_i_13(),
	WaypointCircuit_t2375210762::get_offset_of_P0_14(),
	WaypointCircuit_t2375210762::get_offset_of_P1_15(),
	WaypointCircuit_t2375210762::get_offset_of_P2_16(),
	WaypointCircuit_t2375210762::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (WaypointList_t2985503331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	WaypointList_t2985503331::get_offset_of_circuit_0(),
	WaypointList_t2985503331::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (RoutePoint_t318924311)+ sizeof (Il2CppObject), sizeof(RoutePoint_t318924311 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	RoutePoint_t318924311::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoutePoint_t318924311::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (WaypointProgressTracker_t2206407592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[15] = 
{
	WaypointProgressTracker_t2206407592::get_offset_of_circuit_2(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForTargetOffset_3(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForTargetFactor_4(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForSpeedOffset_5(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForSpeedFactor_6(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressStyle_7(),
	WaypointProgressTracker_t2206407592::get_offset_of_pointToPointThreshold_8(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CtargetPointU3Ek__BackingField_9(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CspeedPointU3Ek__BackingField_10(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CprogressPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t2206407592::get_offset_of_target_12(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressDistance_13(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressNum_14(),
	WaypointProgressTracker_t2206407592::get_offset_of_lastPosition_15(),
	WaypointProgressTracker_t2206407592::get_offset_of_speed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (ProgressStyle_t1659392090)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[3] = 
{
	ProgressStyle_t1659392090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
