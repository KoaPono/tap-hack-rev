﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_U3CenlargeSlicesU32419316972.h"
#include "AssemblyU2DCSharp_WMG_Radar_Graph3136528123.h"
#include "AssemblyU2DCSharp_WMG_Random_Graph3588078778.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph2652186449.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_DataLabeler2502756599.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_TextureChanger922487265.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_ColorChanger23166627.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_BandColorsChanger2486711437.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_U3CupdateLabelLin2727947773.h"
#include "AssemblyU2DCSharp_WMG_Ring_Graph_U3CupdateLabelLin2727947774.h"
#include "AssemblyU2DCSharp_WMG_Change_Obj2314932812.h"
#include "AssemblyU2DCSharp_WMG_Change_Obj_ObjChangedHandler3393583158.h"
#include "AssemblyU2DCSharp_WMG_EnumFlagAttribute207918915.h"
#include "AssemblyU2DCSharp_WMG_Enums1964715972.h"
#include "AssemblyU2DCSharp_WMG_Enums_labelTypes3771076890.h"
#include "AssemblyU2DCSharp_WMG_Reflection2243432107.h"
#include "AssemblyU2DCSharp_WMG_Util2978859006.h"
#include "AssemblyU2DCSharp_WMG_Axis1943926323.h"
#include "AssemblyU2DCSharp_WMG_Axis_labelTypes3895720373.h"
#include "AssemblyU2DCSharp_WMG_Axis_AxisLabelLabeler3200445994.h"
#include "AssemblyU2DCSharp_WMG_Bezier_Band2296745749.h"
#include "AssemblyU2DCSharp_WMG_ComputeLineGraph_Data2188547140.h"
#include "AssemblyU2DCSharp_WMG_Compute_Shader1525039799.h"
#include "AssemblyU2DCSharp_WMG_Data_Source3507445670.h"
#include "AssemblyU2DCSharp_WMG_Data_Source_WMG_DataSourceTyp539563441.h"
#include "AssemblyU2DCSharp_WMG_Data_Source_WMG_VariableType3350654562.h"
#include "AssemblyU2DCSharp_WMG_Graph_Auto_Anim872713172.h"
#include "AssemblyU2DCSharp_WMG_Graph_Auto_Anim_U3CSeriesAut3569240739.h"
#include "AssemblyU2DCSharp_WMG_Graph_Manager1488325314.h"
#include "AssemblyU2DCSharp_WMG_Legend2854767033.h"
#include "AssemblyU2DCSharp_WMG_Legend_legendTypes3974389206.h"
#include "AssemblyU2DCSharp_WMG_Legend_Entry2252178308.h"
#include "AssemblyU2DCSharp_WMG_Link1117412714.h"
#include "AssemblyU2DCSharp_WMG_Node2125495066.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_Slice957532058.h"
#include "AssemblyU2DCSharp_WMG_Ring3086780720.h"
#include "AssemblyU2DCSharp_WMG_Ring_U3CanimBandFillU3Ec__Ano667296401.h"
#include "AssemblyU2DCSharp_WMG_Series1402980291.h"
#include "AssemblyU2DCSharp_WMG_Series_comboTypes283680567.h"
#include "AssemblyU2DCSharp_WMG_Series_areaShadingTypes4207240862.h"
#include "AssemblyU2DCSharp_WMG_Series_SeriesDataLabeler652605318.h"
#include "AssemblyU2DCSharp_WMG_Series_TooltipPointAnimator1694657494.h"
#include "AssemblyU2DCSharp_WMG_Series_SeriesAutoAnimStarted3393216812.h"
#include "AssemblyU2DCSharp_WMG_Series_PointCreatedHandler115080874.h"
#include "AssemblyU2DCSharp_WMG_Series_PointSpriteUpdatedHan1824833182.h"
#include "AssemblyU2DCSharp_WMG_Series_PointShadingSpriteUpda653935292.h"
#include "AssemblyU2DCSharp_WMG_Series_U3CSetDelayedAreaShad2852142050.h"
#include "AssemblyU2DCSharp_AnimationSounds3025961406.h"
#include "AssemblyU2DCSharp_FadeOut1804734986.h"
#include "AssemblyU2DCSharp_FourCodeRefactor1853282147.h"
#include "AssemblyU2DCSharp_FourCodeRefactor_U3CResizeU3Ec__2158521237.h"
#include "AssemblyU2DCSharp_FourCodeRefactor_U3CResetConfett2785433295.h"
#include "AssemblyU2DCSharp_FourCodeRefactor_U3CEnableGameOv4246290121.h"
#include "AssemblyU2DCSharp_Grapher1242508077.h"
#include "AssemblyU2DCSharp_MenuRefactor148745019.h"
#include "AssemblyU2DCSharp_WeeklyScore1892813563.h"
#include "AssemblyU2DCSharp_DailyScore1940687647.h"
#include "AssemblyU2DCSharp_ScoreTracker1973445098.h"
#include "AssemblyU2DCSharp_ScrollTexture3348695036.h"
#include "AssemblyU2DCSharp_SplashScreenScript1054838360.h"
#include "AssemblyU2DCSharp_SplashScreenScript_U3CWaitStartLo407139158.h"
#include "AssemblyU2DCSharp_SplashScreenScript_U3CLoadNextSc4270145904.h"
#include "AssemblyU2DCSharp_TutorialScript3587597509.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3842535002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1749519406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1746754562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C900829694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2691167515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2157404822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3369627127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2144252492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3675451859.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3634411257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2607665220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3273007553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3398611001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1039424009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cr69389957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4103805620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1952940174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3887193949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C113868641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3347016329.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_E706380940.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1174895067.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2348924364.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3442124285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1741822950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_E641737877.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_E455343489.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2151802939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2590751060.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4212470145.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3377687064.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1461062090.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2638123957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2794485791.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_U639433180.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (U3CenlargeSlicesU3Ec__AnonStorey2_t2419316972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[2] = 
{
	U3CenlargeSlicesU3Ec__AnonStorey2_t2419316972::get_offset_of_sliceNum_0(),
	U3CenlargeSlicesU3Ec__AnonStorey2_t2419316972::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (WMG_Radar_Graph_t3136528123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[29] = 
{
	WMG_Radar_Graph_t3136528123::get_offset_of__dataSeriesColors_105(),
	WMG_Radar_Graph_t3136528123::get_offset_of_dataSeriesColors_106(),
	WMG_Radar_Graph_t3136528123::get_offset_of__labelStrings_107(),
	WMG_Radar_Graph_t3136528123::get_offset_of_labelStrings_108(),
	WMG_Radar_Graph_t3136528123::get_offset_of_randomData_109(),
	WMG_Radar_Graph_t3136528123::get_offset_of__numPoints_110(),
	WMG_Radar_Graph_t3136528123::get_offset_of__offset_111(),
	WMG_Radar_Graph_t3136528123::get_offset_of__degreeOffset_112(),
	WMG_Radar_Graph_t3136528123::get_offset_of__radarMinVal_113(),
	WMG_Radar_Graph_t3136528123::get_offset_of__radarMaxVal_114(),
	WMG_Radar_Graph_t3136528123::get_offset_of__numGrids_115(),
	WMG_Radar_Graph_t3136528123::get_offset_of__gridLineWidth_116(),
	WMG_Radar_Graph_t3136528123::get_offset_of__gridColor_117(),
	WMG_Radar_Graph_t3136528123::get_offset_of__numDataSeries_118(),
	WMG_Radar_Graph_t3136528123::get_offset_of__dataSeriesLineWidth_119(),
	WMG_Radar_Graph_t3136528123::get_offset_of__labelsColor_120(),
	WMG_Radar_Graph_t3136528123::get_offset_of__labelsOffset_121(),
	WMG_Radar_Graph_t3136528123::get_offset_of__fontSize_122(),
	WMG_Radar_Graph_t3136528123::get_offset_of__hideLabels_123(),
	WMG_Radar_Graph_t3136528123::get_offset_of_grids_124(),
	WMG_Radar_Graph_t3136528123::get_offset_of_dataSeries_125(),
	WMG_Radar_Graph_t3136528123::get_offset_of_radarLabels_126(),
	WMG_Radar_Graph_t3136528123::get_offset_of_createdLabels_127(),
	WMG_Radar_Graph_t3136528123::get_offset_of_changeObjs_128(),
	WMG_Radar_Graph_t3136528123::get_offset_of_radarGraphC_129(),
	WMG_Radar_Graph_t3136528123::get_offset_of_gridsC_130(),
	WMG_Radar_Graph_t3136528123::get_offset_of_labelsC_131(),
	WMG_Radar_Graph_t3136528123::get_offset_of_dataSeriesC_132(),
	WMG_Radar_Graph_t3136528123::get_offset_of_hasInit2_133(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (WMG_Random_Graph_t3588078778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[19] = 
{
	WMG_Random_Graph_t3588078778::get_offset_of_nodePrefab_19(),
	WMG_Random_Graph_t3588078778::get_offset_of_linkPrefab_20(),
	WMG_Random_Graph_t3588078778::get_offset_of_numNodes_21(),
	WMG_Random_Graph_t3588078778::get_offset_of_minAngle_22(),
	WMG_Random_Graph_t3588078778::get_offset_of_minAngleRange_23(),
	WMG_Random_Graph_t3588078778::get_offset_of_maxAngleRange_24(),
	WMG_Random_Graph_t3588078778::get_offset_of_minRandomNumberNeighbors_25(),
	WMG_Random_Graph_t3588078778::get_offset_of_maxRandomNumberNeighbors_26(),
	WMG_Random_Graph_t3588078778::get_offset_of_minRandomLinkLength_27(),
	WMG_Random_Graph_t3588078778::get_offset_of_maxRandomLinkLength_28(),
	WMG_Random_Graph_t3588078778::get_offset_of_centerPropogate_29(),
	WMG_Random_Graph_t3588078778::get_offset_of_noLinkIntersection_30(),
	WMG_Random_Graph_t3588078778::get_offset_of_noNodeIntersection_31(),
	WMG_Random_Graph_t3588078778::get_offset_of_noNodeIntersectionRadiusPadding_32(),
	WMG_Random_Graph_t3588078778::get_offset_of_maxNeighborAttempts_33(),
	WMG_Random_Graph_t3588078778::get_offset_of_noLinkNodeIntersection_34(),
	WMG_Random_Graph_t3588078778::get_offset_of_noLinkNodeIntersectionRadiusPadding_35(),
	WMG_Random_Graph_t3588078778::get_offset_of_createOnStart_36(),
	WMG_Random_Graph_t3588078778::get_offset_of_debugRandomGraph_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (WMG_Ring_Graph_t2652186449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[94] = 
{
	WMG_Ring_Graph_t2652186449::get_offset_of__bandColors_19(),
	WMG_Ring_Graph_t2652186449::get_offset_of_bandColors_20(),
	WMG_Ring_Graph_t2652186449::get_offset_of__values_21(),
	WMG_Ring_Graph_t2652186449::get_offset_of_values_22(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labels_23(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labels_24(),
	WMG_Ring_Graph_t2652186449::get_offset_of__ringIDs_25(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringIDs_26(),
	WMG_Ring_Graph_t2652186449::get_offset_of__hideRings_27(),
	WMG_Ring_Graph_t2652186449::get_offset_of_hideRings_28(),
	WMG_Ring_Graph_t2652186449::get_offset_of_animateData_29(),
	WMG_Ring_Graph_t2652186449::get_offset_of_animDuration_30(),
	WMG_Ring_Graph_t2652186449::get_offset_of_animEaseType_31(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringPrefab_32(),
	WMG_Ring_Graph_t2652186449::get_offset_of_extraRing_33(),
	WMG_Ring_Graph_t2652186449::get_offset_of_background_34(),
	WMG_Ring_Graph_t2652186449::get_offset_of_zeroLine_35(),
	WMG_Ring_Graph_t2652186449::get_offset_of_zeroLineText_36(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringsParent_37(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringLabelsParent_38(),
	WMG_Ring_Graph_t2652186449::get_offset_of_contentParent_39(),
	WMG_Ring_Graph_t2652186449::get_offset_of_valuesDataSource_40(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labelsDataSource_41(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringIDsDataSource_42(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labelLineSprite_43(),
	WMG_Ring_Graph_t2652186449::get_offset_of_botLeftCorners_44(),
	WMG_Ring_Graph_t2652186449::get_offset_of_botRightCorners_45(),
	WMG_Ring_Graph_t2652186449::get_offset_of_computeShader_46(),
	WMG_Ring_Graph_t2652186449::get_offset_of_textureResolution_47(),
	WMG_Ring_Graph_t2652186449::get_offset_of__bandMode_48(),
	WMG_Ring_Graph_t2652186449::get_offset_of__pieMode_49(),
	WMG_Ring_Graph_t2652186449::get_offset_of__pieModePaddingDegrees_50(),
	WMG_Ring_Graph_t2652186449::get_offset_of__pieModeDegreeOffset_51(),
	WMG_Ring_Graph_t2652186449::get_offset_of__innerRadiusPercentage_52(),
	WMG_Ring_Graph_t2652186449::get_offset_of__degrees_53(),
	WMG_Ring_Graph_t2652186449::get_offset_of__minValue_54(),
	WMG_Ring_Graph_t2652186449::get_offset_of__maxValue_55(),
	WMG_Ring_Graph_t2652186449::get_offset_of__bandColor_56(),
	WMG_Ring_Graph_t2652186449::get_offset_of__autoUpdateBandAlpha_57(),
	WMG_Ring_Graph_t2652186449::get_offset_of__autoUpdateBandAlphaReverse_58(),
	WMG_Ring_Graph_t2652186449::get_offset_of__ringColor_59(),
	WMG_Ring_Graph_t2652186449::get_offset_of__ringWidth_60(),
	WMG_Ring_Graph_t2652186449::get_offset_of__ringPointWidthFactor_61(),
	WMG_Ring_Graph_t2652186449::get_offset_of__bandPadding_62(),
	WMG_Ring_Graph_t2652186449::get_offset_of__hideZeroLabelLine_63(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelStartCenteredOnBand_64(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelPointSize_65(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelLinePadding_66(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelTextOffset_67(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelLineColor_68(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelPointColor_69(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelsFontSize_70(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelsColor_71(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelsFontStyle_72(),
	WMG_Ring_Graph_t2652186449::get_offset_of__labelsFont_73(),
	WMG_Ring_Graph_t2652186449::get_offset_of__showDataLabels_74(),
	WMG_Ring_Graph_t2652186449::get_offset_of__dataLabelsFontSize_75(),
	WMG_Ring_Graph_t2652186449::get_offset_of__dataLabelsColor_76(),
	WMG_Ring_Graph_t2652186449::get_offset_of__dataLabelsFontStyle_77(),
	WMG_Ring_Graph_t2652186449::get_offset_of__dataLabelsFont_78(),
	WMG_Ring_Graph_t2652186449::get_offset_of__leftRightPadding_79(),
	WMG_Ring_Graph_t2652186449::get_offset_of__topBotPadding_80(),
	WMG_Ring_Graph_t2652186449::get_offset_of__antiAliasing_81(),
	WMG_Ring_Graph_t2652186449::get_offset_of__antiAliasingStrength_82(),
	WMG_Ring_Graph_t2652186449::get_offset_of__useComputeShader_83(),
	WMG_Ring_Graph_t2652186449::get_offset_of_U3CMaxDataValU3Ek__BackingField_84(),
	WMG_Ring_Graph_t2652186449::get_offset_of_U3CMinDataValU3Ek__BackingField_85(),
	WMG_Ring_Graph_t2652186449::get_offset_of_origGraphWidth_86(),
	WMG_Ring_Graph_t2652186449::get_offset_of_containerWidthCached_87(),
	WMG_Ring_Graph_t2652186449::get_offset_of_containerHeightCached_88(),
	WMG_Ring_Graph_t2652186449::get_offset_of_U3CringsU3Ek__BackingField_89(),
	WMG_Ring_Graph_t2652186449::get_offset_of_extraRingSprite_90(),
	WMG_Ring_Graph_t2652186449::get_offset_of_texColors_91(),
	WMG_Ring_Graph_t2652186449::get_offset_of_changeObjs_92(),
	WMG_Ring_Graph_t2652186449::get_offset_of_numberRingsC_93(),
	WMG_Ring_Graph_t2652186449::get_offset_of_bandColorC_94(),
	WMG_Ring_Graph_t2652186449::get_offset_of_ringColorC_95(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labelLineColorC_96(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labelsC_97(),
	WMG_Ring_Graph_t2652186449::get_offset_of_degreesC_98(),
	WMG_Ring_Graph_t2652186449::get_offset_of_aRingValC_99(),
	WMG_Ring_Graph_t2652186449::get_offset_of_radiusC_100(),
	WMG_Ring_Graph_t2652186449::get_offset_of_textureC_101(),
	WMG_Ring_Graph_t2652186449::get_offset_of_hideRingsC_102(),
	WMG_Ring_Graph_t2652186449::get_offset_of_labelLineC_103(),
	WMG_Ring_Graph_t2652186449::get_offset_of_computeShaderC_104(),
	WMG_Ring_Graph_t2652186449::get_offset_of_aRingValChangeIndices_105(),
	WMG_Ring_Graph_t2652186449::get_offset_of_beforeValCount_106(),
	WMG_Ring_Graph_t2652186449::get_offset_of_afterValCount_107(),
	WMG_Ring_Graph_t2652186449::get_offset_of_hasInit_108(),
	WMG_Ring_Graph_t2652186449::get_offset_of_dataLabeler_109(),
	WMG_Ring_Graph_t2652186449::get_offset_of_textureChanger_110(),
	WMG_Ring_Graph_t2652186449::get_offset_of_colorChanger_111(),
	WMG_Ring_Graph_t2652186449::get_offset_of_bandColorsChanger_112(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (DataLabeler_t2502756599), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (TextureChanger_t922487265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (ColorChanger_t23166627), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (BandColorsChanger_t2486711437), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CupdateLabelLineAringU3Ec__AnonStorey0_t2727947773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[3] = 
{
	U3CupdateLabelLineAringU3Ec__AnonStorey0_t2727947773::get_offset_of_ring_0(),
	U3CupdateLabelLineAringU3Ec__AnonStorey0_t2727947773::get_offset_of_numOverlapping_1(),
	U3CupdateLabelLineAringU3Ec__AnonStorey0_t2727947773::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CupdateLabelLineAringU3Ec__AnonStorey1_t2727947774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	U3CupdateLabelLineAringU3Ec__AnonStorey1_t2727947774::get_offset_of_degOffset_0(),
	U3CupdateLabelLineAringU3Ec__AnonStorey1_t2727947774::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (WMG_Change_Obj_t2314932812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[3] = 
{
	WMG_Change_Obj_t2314932812::get_offset_of_changesPaused_0(),
	WMG_Change_Obj_t2314932812::get_offset_of_changePaused_1(),
	WMG_Change_Obj_t2314932812::get_offset_of_OnChange_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (ObjChangedHandler_t3393583158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (WMG_EnumFlagAttribute_t207918915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[1] = 
{
	WMG_EnumFlagAttribute_t207918915::get_offset_of_enumName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (WMG_Enums_t1964715972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (labelTypes_t3771076890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[9] = 
{
	labelTypes_t3771076890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (WMG_Reflection_t2243432107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (WMG_Util_t2978859006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (WMG_Axis_t1943926323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[67] = 
{
	WMG_Axis_t1943926323::get_offset_of_graph_2(),
	WMG_Axis_t1943926323::get_offset_of__axisLabels_3(),
	WMG_Axis_t1943926323::get_offset_of_axisLabels_4(),
	WMG_Axis_t1943926323::get_offset_of_AxisTitle_5(),
	WMG_Axis_t1943926323::get_offset_of_GridLines_6(),
	WMG_Axis_t1943926323::get_offset_of_AxisTicks_7(),
	WMG_Axis_t1943926323::get_offset_of_AxisLine_8(),
	WMG_Axis_t1943926323::get_offset_of_AxisArrowUR_9(),
	WMG_Axis_t1943926323::get_offset_of_AxisArrowDL_10(),
	WMG_Axis_t1943926323::get_offset_of_AxisObj_11(),
	WMG_Axis_t1943926323::get_offset_of_AxisLabelObjs_12(),
	WMG_Axis_t1943926323::get_offset_of__AxisMinValue_13(),
	WMG_Axis_t1943926323::get_offset_of__AxisMaxValue_14(),
	WMG_Axis_t1943926323::get_offset_of__AxisNumTicks_15(),
	WMG_Axis_t1943926323::get_offset_of__MinAutoGrow_16(),
	WMG_Axis_t1943926323::get_offset_of__MaxAutoGrow_17(),
	WMG_Axis_t1943926323::get_offset_of__MinAutoShrink_18(),
	WMG_Axis_t1943926323::get_offset_of__MaxAutoShrink_19(),
	WMG_Axis_t1943926323::get_offset_of__AxisLinePadding_20(),
	WMG_Axis_t1943926323::get_offset_of__AxisUseNonTickPercent_21(),
	WMG_Axis_t1943926323::get_offset_of__AxisNonTickPercent_22(),
	WMG_Axis_t1943926323::get_offset_of__HideAxisArrowTopRight_23(),
	WMG_Axis_t1943926323::get_offset_of__HideAxisArrowBotLeft_24(),
	WMG_Axis_t1943926323::get_offset_of__AxisArrowTopRight_25(),
	WMG_Axis_t1943926323::get_offset_of__AxisArrowBotLeft_26(),
	WMG_Axis_t1943926323::get_offset_of__AxisTicksRightAbove_27(),
	WMG_Axis_t1943926323::get_offset_of__AxisTick_28(),
	WMG_Axis_t1943926323::get_offset_of__hideTick_29(),
	WMG_Axis_t1943926323::get_offset_of__LabelType_30(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelSkipStart_31(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelSkipInterval_32(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelRotation_33(),
	WMG_Axis_t1943926323::get_offset_of__SetLabelsUsingMaxMin_34(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelSize_35(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelColor_36(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelFontStyle_37(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelFont_38(),
	WMG_Axis_t1943926323::get_offset_of__numDecimalsAxisLabels_39(),
	WMG_Axis_t1943926323::get_offset_of__hideLabels_40(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelSpaceOffset_41(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelSpacing_42(),
	WMG_Axis_t1943926323::get_offset_of__AxisLabelDistBetween_43(),
	WMG_Axis_t1943926323::get_offset_of__hideGrid_44(),
	WMG_Axis_t1943926323::get_offset_of__hideTicks_45(),
	WMG_Axis_t1943926323::get_offset_of__hideAxisLine_46(),
	WMG_Axis_t1943926323::get_offset_of__AxisTitleString_47(),
	WMG_Axis_t1943926323::get_offset_of__AxisTitleOffset_48(),
	WMG_Axis_t1943926323::get_offset_of__AxisTitleFontSize_49(),
	WMG_Axis_t1943926323::get_offset_of_GridLineLength_50(),
	WMG_Axis_t1943926323::get_offset_of_AxisLinePaddingTot_51(),
	WMG_Axis_t1943926323::get_offset_of_AxisPercentagePosition_52(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisLabelSizeU3Ek__BackingField_53(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisLabelSpaceOffsetU3Ek__BackingField_54(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisTitleFontSizeU3Ek__BackingField_55(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisTitleOffsetU3Ek__BackingField_56(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisLinePaddingU3Ek__BackingField_57(),
	WMG_Axis_t1943926323::get_offset_of_U3CorigAxisArrowSizeU3Ek__BackingField_58(),
	WMG_Axis_t1943926323::get_offset_of_hasInit_59(),
	WMG_Axis_t1943926323::get_offset_of_otherAxis_60(),
	WMG_Axis_t1943926323::get_offset_of_otherAxis2_61(),
	WMG_Axis_t1943926323::get_offset_of_U3CisYU3Ek__BackingField_62(),
	WMG_Axis_t1943926323::get_offset_of_U3CisSecondaryU3Ek__BackingField_63(),
	WMG_Axis_t1943926323::get_offset_of_anchorVec_64(),
	WMG_Axis_t1943926323::get_offset_of_axisLabelLabeler_65(),
	WMG_Axis_t1943926323::get_offset_of_changeObjs_66(),
	WMG_Axis_t1943926323::get_offset_of_graphC_67(),
	WMG_Axis_t1943926323::get_offset_of_seriesC_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (labelTypes_t3895720373)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2019[5] = 
{
	labelTypes_t3895720373::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (AxisLabelLabeler_t3200445994), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (WMG_Bezier_Band_t2296745749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[17] = 
{
	WMG_Bezier_Band_t2296745749::get_offset_of_bandFillSpriteGO_2(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandLineSpriteGO_3(),
	WMG_Bezier_Band_t2296745749::get_offset_of_labelParent_4(),
	WMG_Bezier_Band_t2296745749::get_offset_of_percentLabel_5(),
	WMG_Bezier_Band_t2296745749::get_offset_of_label_6(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandFillSprite_7(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandLineSprite_8(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandFillMat_9(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandLineMat_10(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandFillColors_11(),
	WMG_Bezier_Band_t2296745749::get_offset_of_bandLineColors_12(),
	WMG_Bezier_Band_t2296745749::get_offset_of_texSize_13(),
	WMG_Bezier_Band_t2296745749::get_offset_of_graph_14(),
	WMG_Bezier_Band_t2296745749::get_offset_of_cumulativePercent_15(),
	WMG_Bezier_Band_t2296745749::get_offset_of_prevCumulativePercent_16(),
	WMG_Bezier_Band_t2296745749::get_offset_of_superSamplingRate_17(),
	WMG_Bezier_Band_t2296745749::get_offset_of_xPad_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (WMG_ComputeLineGraph_Data_t2188547140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[1] = 
{
	WMG_ComputeLineGraph_Data_t2188547140::get_offset_of_pointVals_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (WMG_Compute_Shader_t1525039799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[6] = 
{
	WMG_Compute_Shader_t1525039799::get_offset_of_computeShader_2(),
	WMG_Compute_Shader_t1525039799::get_offset_of_texSize_3(),
	WMG_Compute_Shader_t1525039799::get_offset_of_kernelHandle_4(),
	WMG_Compute_Shader_t1525039799::get_offset_of_renderTexture_5(),
	WMG_Compute_Shader_t1525039799::get_offset_of_rawImg_6(),
	WMG_Compute_Shader_t1525039799::get_offset_of_hasInit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (WMG_Data_Source_t3507445670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[11] = 
{
	WMG_Data_Source_t3507445670::get_offset_of_dataSourceType_2(),
	WMG_Data_Source_t3507445670::get_offset_of_dataProviders_3(),
	WMG_Data_Source_t3507445670::get_offset_of_dataProvider_4(),
	WMG_Data_Source_t3507445670::get_offset_of_variableTypes_5(),
	WMG_Data_Source_t3507445670::get_offset_of_variableType_6(),
	WMG_Data_Source_t3507445670::get_offset_of_variableNames_7(),
	WMG_Data_Source_t3507445670::get_offset_of_variableName_8(),
	WMG_Data_Source_t3507445670::get_offset_of_varNames1_9(),
	WMG_Data_Source_t3507445670::get_offset_of_varName1_10(),
	WMG_Data_Source_t3507445670::get_offset_of_varNames2_11(),
	WMG_Data_Source_t3507445670::get_offset_of_varName2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (WMG_DataSourceTypes_t539563441)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[4] = 
{
	WMG_DataSourceTypes_t539563441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (WMG_VariableTypes_t3350654562)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[6] = 
{
	WMG_VariableTypes_t3350654562::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (WMG_Graph_Auto_Anim_t872713172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	WMG_Graph_Auto_Anim_t872713172::get_offset_of_theGraph_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (U3CSeriesAutoAnimStartedMethodU3Ec__AnonStorey0_t3569240739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	U3CSeriesAutoAnimStartedMethodU3Ec__AnonStorey0_t3569240739::get_offset_of_aSeries_0(),
	U3CSeriesAutoAnimStartedMethodU3Ec__AnonStorey0_t3569240739::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (WMG_Graph_Manager_t1488325314), -1, sizeof(WMG_Graph_Manager_t1488325314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2029[4] = 
{
	WMG_Graph_Manager_t1488325314::get_offset_of_nodesParent_15(),
	WMG_Graph_Manager_t1488325314::get_offset_of_linksParent_16(),
	WMG_Graph_Manager_t1488325314_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
	WMG_Graph_Manager_t1488325314_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (WMG_Legend_t2854767033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[38] = 
{
	WMG_Legend_t2854767033::get_offset_of_theGraph_2(),
	WMG_Legend_t2854767033::get_offset_of_background_3(),
	WMG_Legend_t2854767033::get_offset_of_entriesParent_4(),
	WMG_Legend_t2854767033::get_offset_of_emptyPrefab_5(),
	WMG_Legend_t2854767033::get_offset_of_legendEntries_6(),
	WMG_Legend_t2854767033::get_offset_of_pieGraph_7(),
	WMG_Legend_t2854767033::get_offset_of_axisGraph_8(),
	WMG_Legend_t2854767033::get_offset_of__hideLegend_9(),
	WMG_Legend_t2854767033::get_offset_of__legendType_10(),
	WMG_Legend_t2854767033::get_offset_of__labelType_11(),
	WMG_Legend_t2854767033::get_offset_of__showBackground_12(),
	WMG_Legend_t2854767033::get_offset_of__oppositeSideLegend_13(),
	WMG_Legend_t2854767033::get_offset_of__offset_14(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryWidth_15(),
	WMG_Legend_t2854767033::get_offset_of__setWidthFromLabels_16(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryHeight_17(),
	WMG_Legend_t2854767033::get_offset_of__numRowsOrColumns_18(),
	WMG_Legend_t2854767033::get_offset_of__numDecimals_19(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryLinkSpacing_20(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryFontSize_21(),
	WMG_Legend_t2854767033::get_offset_of__legendEntrySpacing_22(),
	WMG_Legend_t2854767033::get_offset_of__pieSwatchSize_23(),
	WMG_Legend_t2854767033::get_offset_of__backgroundPadding_24(),
	WMG_Legend_t2854767033::get_offset_of__autofitEnabled_25(),
	WMG_Legend_t2854767033::get_offset_of__labelColor_26(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryFontStyle_27(),
	WMG_Legend_t2854767033::get_offset_of__legendEntryFont_28(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigLegendEntryWidthU3Ek__BackingField_29(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigLegendEntryHeightU3Ek__BackingField_30(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigLegendEntryLinkSpacingU3Ek__BackingField_31(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigLegendEntryFontSizeU3Ek__BackingField_32(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigLegendEntrySpacingU3Ek__BackingField_33(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigPieSwatchSizeU3Ek__BackingField_34(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigOffsetU3Ek__BackingField_35(),
	WMG_Legend_t2854767033::get_offset_of_U3CorigBackgroundPaddingU3Ek__BackingField_36(),
	WMG_Legend_t2854767033::get_offset_of_hasInit_37(),
	WMG_Legend_t2854767033::get_offset_of_changeObjs_38(),
	WMG_Legend_t2854767033::get_offset_of_legendC_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (legendTypes_t3974389206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	legendTypes_t3974389206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (WMG_Legend_Entry_t2252178308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[7] = 
{
	WMG_Legend_Entry_t2252178308::get_offset_of_legend_2(),
	WMG_Legend_Entry_t2252178308::get_offset_of_label_3(),
	WMG_Legend_Entry_t2252178308::get_offset_of_swatchNode_4(),
	WMG_Legend_Entry_t2252178308::get_offset_of_line_5(),
	WMG_Legend_Entry_t2252178308::get_offset_of_nodeLeft_6(),
	WMG_Legend_Entry_t2252178308::get_offset_of_nodeRight_7(),
	WMG_Legend_Entry_t2252178308::get_offset_of_seriesRef_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (WMG_Link_t1117412714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[11] = 
{
	WMG_Link_t1117412714::get_offset_of_id_2(),
	WMG_Link_t1117412714::get_offset_of_fromNode_3(),
	WMG_Link_t1117412714::get_offset_of_toNode_4(),
	WMG_Link_t1117412714::get_offset_of_objectToScale_5(),
	WMG_Link_t1117412714::get_offset_of_objectToColor_6(),
	WMG_Link_t1117412714::get_offset_of_objectToLabel_7(),
	WMG_Link_t1117412714::get_offset_of_weightIsLength_8(),
	WMG_Link_t1117412714::get_offset_of_updateLabelWithLength_9(),
	WMG_Link_t1117412714::get_offset_of_isSelected_10(),
	WMG_Link_t1117412714::get_offset_of_wasSelected_11(),
	WMG_Link_t1117412714::get_offset_of_weight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (WMG_Node_t2125495066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[15] = 
{
	WMG_Node_t2125495066::get_offset_of_id_2(),
	WMG_Node_t2125495066::get_offset_of_radius_3(),
	WMG_Node_t2125495066::get_offset_of_isSquare_4(),
	WMG_Node_t2125495066::get_offset_of_numLinks_5(),
	WMG_Node_t2125495066::get_offset_of_links_6(),
	WMG_Node_t2125495066::get_offset_of_linkAngles_7(),
	WMG_Node_t2125495066::get_offset_of_objectToScale_8(),
	WMG_Node_t2125495066::get_offset_of_objectToColor_9(),
	WMG_Node_t2125495066::get_offset_of_objectToLabel_10(),
	WMG_Node_t2125495066::get_offset_of_isSelected_11(),
	WMG_Node_t2125495066::get_offset_of_wasSelected_12(),
	WMG_Node_t2125495066::get_offset_of_BFS_mark_13(),
	WMG_Node_t2125495066::get_offset_of_BFS_depth_14(),
	WMG_Node_t2125495066::get_offset_of_Dijkstra_depth_15(),
	WMG_Node_t2125495066::get_offset_of_seriesRef_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (WMG_Pie_Graph_Slice_t957532058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	WMG_Pie_Graph_Slice_t957532058::get_offset_of_objectToMask_17(),
	WMG_Pie_Graph_Slice_t957532058::get_offset_of_slicePercent_18(),
	WMG_Pie_Graph_Slice_t957532058::get_offset_of_slicePercentPosition_19(),
	WMG_Pie_Graph_Slice_t957532058::get_offset_of_pieRef_20(),
	WMG_Pie_Graph_Slice_t957532058::get_offset_of_sliceIndex_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (WMG_Ring_t3086780720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[17] = 
{
	WMG_Ring_t3086780720::get_offset_of_ring_2(),
	WMG_Ring_t3086780720::get_offset_of_band_3(),
	WMG_Ring_t3086780720::get_offset_of_label_4(),
	WMG_Ring_t3086780720::get_offset_of_textLine_5(),
	WMG_Ring_t3086780720::get_offset_of_labelText_6(),
	WMG_Ring_t3086780720::get_offset_of_lowerLabelText_7(),
	WMG_Ring_t3086780720::get_offset_of_labelPoint_8(),
	WMG_Ring_t3086780720::get_offset_of_labelBackground_9(),
	WMG_Ring_t3086780720::get_offset_of_lowerLabelBackground_10(),
	WMG_Ring_t3086780720::get_offset_of_line_11(),
	WMG_Ring_t3086780720::get_offset_of_interactibleObj_12(),
	WMG_Ring_t3086780720::get_offset_of_U3CgraphU3Ek__BackingField_13(),
	WMG_Ring_t3086780720::get_offset_of_U3CringIndexU3Ek__BackingField_14(),
	WMG_Ring_t3086780720::get_offset_of_U3CringSpriteU3Ek__BackingField_15(),
	WMG_Ring_t3086780720::get_offset_of_U3CbandSpriteU3Ek__BackingField_16(),
	WMG_Ring_t3086780720::get_offset_of_texSize_17(),
	WMG_Ring_t3086780720::get_offset_of_animTimeline_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (U3CanimBandFillU3Ec__AnonStorey0_t667296401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[3] = 
{
	U3CanimBandFillU3Ec__AnonStorey0_t667296401::get_offset_of_ringNum_0(),
	U3CanimBandFillU3Ec__AnonStorey0_t667296401::get_offset_of_endFill_1(),
	U3CanimBandFillU3Ec__AnonStorey0_t667296401::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (WMG_Series_t1402980291), -1, sizeof(WMG_Series_t1402980291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2038[101] = 
{
	WMG_Series_t1402980291::get_offset_of__pointValues_2(),
	WMG_Series_t1402980291::get_offset_of_pointValues_3(),
	WMG_Series_t1402980291::get_offset_of__pointColors_4(),
	WMG_Series_t1402980291::get_offset_of_pointColors_5(),
	WMG_Series_t1402980291::get_offset_of_dataLabelPrefab_6(),
	WMG_Series_t1402980291::get_offset_of_dataLabelsParent_7(),
	WMG_Series_t1402980291::get_offset_of_areaShadingMatSolid_8(),
	WMG_Series_t1402980291::get_offset_of_areaShadingMatGradient_9(),
	WMG_Series_t1402980291::get_offset_of_areaShadingParent_10(),
	WMG_Series_t1402980291::get_offset_of_areaShadingPrefab_11(),
	WMG_Series_t1402980291::get_offset_of_areaShadingCSPrefab_12(),
	WMG_Series_t1402980291::get_offset_of_theGraph_13(),
	WMG_Series_t1402980291::get_offset_of_realTimeDataSource_14(),
	WMG_Series_t1402980291::get_offset_of_pointValuesDataSource_15(),
	WMG_Series_t1402980291::get_offset_of_legendEntryPrefab_16(),
	WMG_Series_t1402980291::get_offset_of_linkParent_17(),
	WMG_Series_t1402980291::get_offset_of_nodeParent_18(),
	WMG_Series_t1402980291::get_offset_of_legendEntry_19(),
	WMG_Series_t1402980291::get_offset_of_areaShadingTextureResolution_20(),
	WMG_Series_t1402980291::get_offset_of__comboType_21(),
	WMG_Series_t1402980291::get_offset_of__useSecondYaxis_22(),
	WMG_Series_t1402980291::get_offset_of__seriesName_23(),
	WMG_Series_t1402980291::get_offset_of__pointWidthHeight_24(),
	WMG_Series_t1402980291::get_offset_of__lineScale_25(),
	WMG_Series_t1402980291::get_offset_of__pointColor_26(),
	WMG_Series_t1402980291::get_offset_of__usePointColors_27(),
	WMG_Series_t1402980291::get_offset_of__lineColor_28(),
	WMG_Series_t1402980291::get_offset_of__UseXDistBetweenToSpace_29(),
	WMG_Series_t1402980291::get_offset_of__ManuallySetXDistBetween_30(),
	WMG_Series_t1402980291::get_offset_of__xDistBetweenPoints_31(),
	WMG_Series_t1402980291::get_offset_of__ManuallySetExtraXSpace_32(),
	WMG_Series_t1402980291::get_offset_of__extraXSpace_33(),
	WMG_Series_t1402980291::get_offset_of__hidePoints_34(),
	WMG_Series_t1402980291::get_offset_of__hideLines_35(),
	WMG_Series_t1402980291::get_offset_of__connectFirstToLast_36(),
	WMG_Series_t1402980291::get_offset_of__linePadding_37(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsEnabled_38(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsNumDecimals_39(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsFontSize_40(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsColor_41(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsFontStyle_42(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsFont_43(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsAnchoredLeftBot_44(),
	WMG_Series_t1402980291::get_offset_of__dataLabelsOffset_45(),
	WMG_Series_t1402980291::get_offset_of__areaShadingType_46(),
	WMG_Series_t1402980291::get_offset_of__areaShadingUsesComputeShader_47(),
	WMG_Series_t1402980291::get_offset_of__areaShadingColor_48(),
	WMG_Series_t1402980291::get_offset_of__areaShadingAxisValue_49(),
	WMG_Series_t1402980291::get_offset_of__pointPrefab_50(),
	WMG_Series_t1402980291::get_offset_of__linkPrefab_51(),
	WMG_Series_t1402980291::get_offset_of_nodePrefab_52(),
	WMG_Series_t1402980291::get_offset_of_points_53(),
	WMG_Series_t1402980291::get_offset_of_lines_54(),
	WMG_Series_t1402980291::get_offset_of_areaShadingRects_55(),
	WMG_Series_t1402980291::get_offset_of_dataLabels_56(),
	WMG_Series_t1402980291::get_offset_of_barIsNegative_57(),
	WMG_Series_t1402980291::get_offset_of_changedValIndices_58(),
	WMG_Series_t1402980291::get_offset_of_U3CorigPointWidthHeightU3Ek__BackingField_59(),
	WMG_Series_t1402980291::get_offset_of_U3CorigLineScaleU3Ek__BackingField_60(),
	WMG_Series_t1402980291::get_offset_of_U3CorigDataLabelsFontSizeU3Ek__BackingField_61(),
	WMG_Series_t1402980291::get_offset_of_U3CorigDataLabelOffsetU3Ek__BackingField_62(),
	WMG_Series_t1402980291::get_offset_of_cachedSeriesType_63(),
	WMG_Series_t1402980291::get_offset_of_realTimeRunning_64(),
	WMG_Series_t1402980291::get_offset_of_realTimeLoopVar_65(),
	WMG_Series_t1402980291::get_offset_of_realTimeOrigMax_66(),
	WMG_Series_t1402980291::get_offset_of_beginningToAutoAnimate_67(),
	WMG_Series_t1402980291::get_offset_of_U3CcurrentlyAnimatingU3Ek__BackingField_68(),
	WMG_Series_t1402980291::get_offset_of_U3CautoAnimationTimelineU3Ek__BackingField_69(),
	WMG_Series_t1402980291::get_offset_of_afterPositions_70(),
	WMG_Series_t1402980291::get_offset_of_afterWidths_71(),
	WMG_Series_t1402980291::get_offset_of_afterHeights_72(),
	WMG_Series_t1402980291::get_offset_of_origPositions_73(),
	WMG_Series_t1402980291::get_offset_of_origWidths_74(),
	WMG_Series_t1402980291::get_offset_of_origHeights_75(),
	WMG_Series_t1402980291::get_offset_of_changeObjs_76(),
	WMG_Series_t1402980291::get_offset_of_pointValuesC_77(),
	WMG_Series_t1402980291::get_offset_of_pointValuesCountC_78(),
	WMG_Series_t1402980291::get_offset_of_pointValuesValC_79(),
	WMG_Series_t1402980291::get_offset_of_lineScaleC_80(),
	WMG_Series_t1402980291::get_offset_of_pointWidthHeightC_81(),
	WMG_Series_t1402980291::get_offset_of_dataLabelsC_82(),
	WMG_Series_t1402980291::get_offset_of_lineColorC_83(),
	WMG_Series_t1402980291::get_offset_of_pointColorC_84(),
	WMG_Series_t1402980291::get_offset_of_hideLineC_85(),
	WMG_Series_t1402980291::get_offset_of_hidePointC_86(),
	WMG_Series_t1402980291::get_offset_of_seriesNameC_87(),
	WMG_Series_t1402980291::get_offset_of_linePaddingC_88(),
	WMG_Series_t1402980291::get_offset_of_areaShadingTypeC_89(),
	WMG_Series_t1402980291::get_offset_of_areaShadingC_90(),
	WMG_Series_t1402980291::get_offset_of_prefabC_91(),
	WMG_Series_t1402980291::get_offset_of_connectFirstToLastC_92(),
	WMG_Series_t1402980291::get_offset_of_hasInit_93(),
	WMG_Series_t1402980291::get_offset_of_seriesDataLabeler_94(),
	WMG_Series_t1402980291::get_offset_of_tooltipPointAnimator_95(),
	WMG_Series_t1402980291::get_offset_of_SeriesAutoAnimStarted_96(),
	WMG_Series_t1402980291::get_offset_of_PointCreated_97(),
	WMG_Series_t1402980291::get_offset_of_PointSpriteUpdated_98(),
	WMG_Series_t1402980291::get_offset_of_PointShadingSpriteUpdated_99(),
	WMG_Series_t1402980291::get_offset_of_AutoUpdateXDistBetween_100(),
	WMG_Series_t1402980291_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_101(),
	WMG_Series_t1402980291_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_102(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (comboTypes_t283680567)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[3] = 
{
	comboTypes_t283680567::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (areaShadingTypes_t4207240862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[4] = 
{
	areaShadingTypes_t4207240862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (SeriesDataLabeler_t652605318), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (TooltipPointAnimator_t1694657494), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (SeriesAutoAnimStartedHandler_t3393216812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (PointCreatedHandler_t115080874), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (PointSpriteUpdatedHandler_t1824833182), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (PointShadingSpriteUpdatedHandler_t653935292), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CSetDelayedAreaShadingChangedU3Ec__Iterator0_t2852142050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[4] = 
{
	U3CSetDelayedAreaShadingChangedU3Ec__Iterator0_t2852142050::get_offset_of_U24this_0(),
	U3CSetDelayedAreaShadingChangedU3Ec__Iterator0_t2852142050::get_offset_of_U24current_1(),
	U3CSetDelayedAreaShadingChangedU3Ec__Iterator0_t2852142050::get_offset_of_U24disposing_2(),
	U3CSetDelayedAreaShadingChangedU3Ec__Iterator0_t2852142050::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (AnimationSounds_t3025961406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[4] = 
{
	AnimationSounds_t3025961406::get_offset_of_swish_2(),
	AnimationSounds_t3025961406::get_offset_of_coin_3(),
	AnimationSounds_t3025961406::get_offset_of_source_4(),
	AnimationSounds_t3025961406::get_offset_of_volumeUp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (FadeOut_t1804734986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[4] = 
{
	FadeOut_t1804734986::get_offset_of_background_2(),
	FadeOut_t1804734986::get_offset_of_logo_3(),
	FadeOut_t1804734986::get_offset_of_fade_4(),
	FadeOut_t1804734986::get_offset_of_isFading_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (FourCodeRefactor_t1853282147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[34] = 
{
	FourCodeRefactor_t1853282147::get_offset_of_positionBorders_2(),
	FourCodeRefactor_t1853282147::get_offset_of_correctSprites_3(),
	FourCodeRefactor_t1853282147::get_offset_of_incorrectSprites_4(),
	FourCodeRefactor_t1853282147::get_offset_of_emptySprite_5(),
	FourCodeRefactor_t1853282147::get_offset_of_positionSprites_6(),
	FourCodeRefactor_t1853282147::get_offset_of_colorButtonAudioSource_7(),
	FourCodeRefactor_t1853282147::get_offset_of_gameOverAudioSource_8(),
	FourCodeRefactor_t1853282147::get_offset_of_completedCodeAudioCource_9(),
	FourCodeRefactor_t1853282147::get_offset_of_correctSound_10(),
	FourCodeRefactor_t1853282147::get_offset_of_incorrectSound_11(),
	FourCodeRefactor_t1853282147::get_offset_of_highscoreSound_12(),
	FourCodeRefactor_t1853282147::get_offset_of_endGameSound_13(),
	FourCodeRefactor_t1853282147::get_offset_of_codeCorrectSound_14(),
	FourCodeRefactor_t1853282147::get_offset_of_timerText_15(),
	FourCodeRefactor_t1853282147::get_offset_of_scoreText_16(),
	FourCodeRefactor_t1853282147::get_offset_of_finalScore_17(),
	FourCodeRefactor_t1853282147::get_offset_of_highScoreText_18(),
	FourCodeRefactor_t1853282147::get_offset_of_confetti1_19(),
	FourCodeRefactor_t1853282147::get_offset_of_confetti2_20(),
	FourCodeRefactor_t1853282147::get_offset_of_restartButton_21(),
	FourCodeRefactor_t1853282147::get_offset_of_menuButton_22(),
	FourCodeRefactor_t1853282147::get_offset_of_startingTime_23(),
	FourCodeRefactor_t1853282147::get_offset_of_timeRemaining_24(),
	FourCodeRefactor_t1853282147::get_offset_of_addedTime_25(),
	FourCodeRefactor_t1853282147::get_offset_of_numberOfColors_26(),
	FourCodeRefactor_t1853282147::get_offset_of_colorsSet_27(),
	FourCodeRefactor_t1853282147::get_offset_of_positionsSet_28(),
	FourCodeRefactor_t1853282147::get_offset_of_positionColor_29(),
	FourCodeRefactor_t1853282147::get_offset_of_codeColorID_30(),
	FourCodeRefactor_t1853282147::get_offset_of_playerScore_31(),
	FourCodeRefactor_t1853282147::get_offset_of_highscore_32(),
	FourCodeRefactor_t1853282147::get_offset_of_gameIsPlaying_33(),
	FourCodeRefactor_t1853282147::get_offset_of_menuController_34(),
	FourCodeRefactor_t1853282147::get_offset_of_scoreTracker_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (U3CResizeU3Ec__Iterator0_t2158521237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[7] = 
{
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_position_0(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U3CnormalsizeU3E__0_1(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U3CexpandNumberU3E__0_2(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U3CexpandedSizeU3E__0_3(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U24current_4(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U24disposing_5(),
	U3CResizeU3Ec__Iterator0_t2158521237::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (U3CResetConfettiU3Ec__Iterator1_t2785433295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[4] = 
{
	U3CResetConfettiU3Ec__Iterator1_t2785433295::get_offset_of_U24this_0(),
	U3CResetConfettiU3Ec__Iterator1_t2785433295::get_offset_of_U24current_1(),
	U3CResetConfettiU3Ec__Iterator1_t2785433295::get_offset_of_U24disposing_2(),
	U3CResetConfettiU3Ec__Iterator1_t2785433295::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (U3CEnableGameOverButtonsU3Ec__Iterator2_t4246290121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[4] = 
{
	U3CEnableGameOverButtonsU3Ec__Iterator2_t4246290121::get_offset_of_U24this_0(),
	U3CEnableGameOverButtonsU3Ec__Iterator2_t4246290121::get_offset_of_U24current_1(),
	U3CEnableGameOverButtonsU3Ec__Iterator2_t4246290121::get_offset_of_U24disposing_2(),
	U3CEnableGameOverButtonsU3Ec__Iterator2_t4246290121::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Grapher_t1242508077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[10] = 
{
	Grapher_t1242508077::get_offset_of_noScoresText_2(),
	Grapher_t1242508077::get_offset_of_LevelIndicator_3(),
	Grapher_t1242508077::get_offset_of_graph_4(),
	Grapher_t1242508077::get_offset_of_level_5(),
	Grapher_t1242508077::get_offset_of_series1Data_6(),
	Grapher_t1242508077::get_offset_of_series2Data_7(),
	Grapher_t1242508077::get_offset_of_series3Data_8(),
	Grapher_t1242508077::get_offset_of_series1_9(),
	Grapher_t1242508077::get_offset_of_series2_10(),
	Grapher_t1242508077::get_offset_of_series3_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (MenuRefactor_t148745019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[30] = 
{
	MenuRefactor_t148745019::get_offset_of_gameMenu_2(),
	MenuRefactor_t148745019::get_offset_of_gameItems_3(),
	MenuRefactor_t148745019::get_offset_of_gameOver_4(),
	MenuRefactor_t148745019::get_offset_of_optionsMenu_5(),
	MenuRefactor_t148745019::get_offset_of_creditsMenu_6(),
	MenuRefactor_t148745019::get_offset_of_yesNoMenu_7(),
	MenuRefactor_t148745019::get_offset_of_levelMenu_8(),
	MenuRefactor_t148745019::get_offset_of_tutorial_9(),
	MenuRefactor_t148745019::get_offset_of_stats_10(),
	MenuRefactor_t148745019::get_offset_of_color1Button_11(),
	MenuRefactor_t148745019::get_offset_of_color2Button_12(),
	MenuRefactor_t148745019::get_offset_of_color3Button_13(),
	MenuRefactor_t148745019::get_offset_of_color4Button_14(),
	MenuRefactor_t148745019::get_offset_of_color5Button_15(),
	MenuRefactor_t148745019::get_offset_of_color6Button_16(),
	MenuRefactor_t148745019::get_offset_of_layoutGroup_17(),
	MenuRefactor_t148745019::get_offset_of_pos1_18(),
	MenuRefactor_t148745019::get_offset_of_pos2_19(),
	MenuRefactor_t148745019::get_offset_of_pos3_20(),
	MenuRefactor_t148745019::get_offset_of_pos4_21(),
	MenuRefactor_t148745019::get_offset_of_pos5_22(),
	MenuRefactor_t148745019::get_offset_of_pos6_23(),
	MenuRefactor_t148745019::get_offset_of_buttonSound_24(),
	MenuRefactor_t148745019::get_offset_of_musicSource_25(),
	MenuRefactor_t148745019::get_offset_of_mixer_26(),
	MenuRefactor_t148745019::get_offset_of_soundFX_27(),
	MenuRefactor_t148745019::get_offset_of_music_28(),
	MenuRefactor_t148745019::get_offset_of_controller_29(),
	MenuRefactor_t148745019::get_offset_of_tutorialScript_30(),
	MenuRefactor_t148745019::get_offset_of_graphScript_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (WeeklyScore_t1892813563)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[4] = 
{
	WeeklyScore_t1892813563::get_offset_of_highScore_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WeeklyScore_t1892813563::get_offset_of_averageScore_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WeeklyScore_t1892813563::get_offset_of_lowScore_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WeeklyScore_t1892813563::get_offset_of_date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (DailyScore_t1940687647)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[2] = 
{
	DailyScore_t1940687647::get_offset_of_date_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DailyScore_t1940687647::get_offset_of_scores_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (ScoreTracker_t1973445098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ScrollTexture_t3348695036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[5] = 
{
	ScrollTexture_t3348695036::get_offset_of_xSpeed_2(),
	ScrollTexture_t3348695036::get_offset_of_ySpeed_3(),
	ScrollTexture_t3348695036::get_offset_of_uvOffset_4(),
	ScrollTexture_t3348695036::get_offset_of_rend_5(),
	ScrollTexture_t3348695036::get_offset_of_mat_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (SplashScreenScript_t1054838360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	SplashScreenScript_t1054838360::get_offset_of_sharkAnimation_2(),
	SplashScreenScript_t1054838360::get_offset_of_splashStarted_3(),
	SplashScreenScript_t1054838360::get_offset_of_isFading_4(),
	SplashScreenScript_t1054838360::get_offset_of_fade_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (U3CWaitStartLogoAnimationU3Ec__Iterator0_t407139158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	U3CWaitStartLogoAnimationU3Ec__Iterator0_t407139158::get_offset_of_U24this_0(),
	U3CWaitStartLogoAnimationU3Ec__Iterator0_t407139158::get_offset_of_U24current_1(),
	U3CWaitStartLogoAnimationU3Ec__Iterator0_t407139158::get_offset_of_U24disposing_2(),
	U3CWaitStartLogoAnimationU3Ec__Iterator0_t407139158::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (U3CLoadNextSceneU3Ec__Iterator1_t4270145904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[3] = 
{
	U3CLoadNextSceneU3Ec__Iterator1_t4270145904::get_offset_of_U24current_0(),
	U3CLoadNextSceneU3Ec__Iterator1_t4270145904::get_offset_of_U24disposing_1(),
	U3CLoadNextSceneU3Ec__Iterator1_t4270145904::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (TutorialScript_t3587597509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[12] = 
{
	TutorialScript_t3587597509::get_offset_of_positionBorders_2(),
	TutorialScript_t3587597509::get_offset_of_score_3(),
	TutorialScript_t3587597509::get_offset_of_correctSprites_4(),
	TutorialScript_t3587597509::get_offset_of_incorrectSprites_5(),
	TutorialScript_t3587597509::get_offset_of_emptySprite_6(),
	TutorialScript_t3587597509::get_offset_of_positionSprites_7(),
	TutorialScript_t3587597509::get_offset_of_tutorialMessages_8(),
	TutorialScript_t3587597509::get_offset_of_tutorialStep_9(),
	TutorialScript_t3587597509::get_offset_of_colorsSet_10(),
	TutorialScript_t3587597509::get_offset_of_positionsSet_11(),
	TutorialScript_t3587597509::get_offset_of_mainGameScript_12(),
	TutorialScript_t3587597509::get_offset_of_menuController_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (AxisTouchButton_t3842535002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[6] = 
{
	AxisTouchButton_t3842535002::get_offset_of_axisName_2(),
	AxisTouchButton_t3842535002::get_offset_of_axisValue_3(),
	AxisTouchButton_t3842535002::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3842535002::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3842535002::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3842535002::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (ButtonHandler_t1749519406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[1] = 
{
	ButtonHandler_t1749519406::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (CrossPlatformInputManager_t1746754562), -1, sizeof(CrossPlatformInputManager_t1746754562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2067[3] = 
{
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (ActiveInputMethod_t900829694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	ActiveInputMethod_t900829694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (VirtualAxis_t2691167515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[3] = 
{
	VirtualAxis_t2691167515::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t2691167515::get_offset_of_m_Value_1(),
	VirtualAxis_t2691167515::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (VirtualButton_t2157404822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[5] = 
{
	VirtualButton_t2157404822::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2157404822::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2157404822::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2157404822::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2157404822::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (InputAxisScrollbar_t3369627127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	InputAxisScrollbar_t3369627127::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Joystick_t2144252492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[9] = 
{
	Joystick_t2144252492::get_offset_of_MovementRange_2(),
	Joystick_t2144252492::get_offset_of_axesToUse_3(),
	Joystick_t2144252492::get_offset_of_horizontalAxisName_4(),
	Joystick_t2144252492::get_offset_of_verticalAxisName_5(),
	Joystick_t2144252492::get_offset_of_m_StartPos_6(),
	Joystick_t2144252492::get_offset_of_m_UseX_7(),
	Joystick_t2144252492::get_offset_of_m_UseY_8(),
	Joystick_t2144252492::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2144252492::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (AxisOption_t3675451859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2073[4] = 
{
	AxisOption_t3675451859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (MobileControlRig_t3634411257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (MobileInput_t2607665220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (StandaloneInput_t3273007553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (TiltInput_t3398611001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	TiltInput_t3398611001::get_offset_of_mapping_2(),
	TiltInput_t3398611001::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t3398611001::get_offset_of_fullTiltAngle_4(),
	TiltInput_t3398611001::get_offset_of_centreAngleOffset_5(),
	TiltInput_t3398611001::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (AxisOptions_t1039424009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2078[3] = 
{
	AxisOptions_t1039424009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (AxisMapping_t69389957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[2] = 
{
	AxisMapping_t69389957::get_offset_of_type_0(),
	AxisMapping_t69389957::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (MappingType_t4103805620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[5] = 
{
	MappingType_t4103805620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (TouchPad_t1952940174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[18] = 
{
	TouchPad_t1952940174::get_offset_of_axesToUse_2(),
	TouchPad_t1952940174::get_offset_of_controlStyle_3(),
	TouchPad_t1952940174::get_offset_of_horizontalAxisName_4(),
	TouchPad_t1952940174::get_offset_of_verticalAxisName_5(),
	TouchPad_t1952940174::get_offset_of_Xsensitivity_6(),
	TouchPad_t1952940174::get_offset_of_Ysensitivity_7(),
	TouchPad_t1952940174::get_offset_of_m_StartPos_8(),
	TouchPad_t1952940174::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t1952940174::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t1952940174::get_offset_of_m_UseX_11(),
	TouchPad_t1952940174::get_offset_of_m_UseY_12(),
	TouchPad_t1952940174::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t1952940174::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t1952940174::get_offset_of_m_Dragging_15(),
	TouchPad_t1952940174::get_offset_of_m_Id_16(),
	TouchPad_t1952940174::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t1952940174::get_offset_of_m_Center_18(),
	TouchPad_t1952940174::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (AxisOption_t3887193949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[4] = 
{
	AxisOption_t3887193949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (ControlStyle_t113868641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[4] = 
{
	ControlStyle_t113868641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (VirtualInput_t3347016329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[4] = 
{
	VirtualInput_t3347016329::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t3347016329::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (AfterburnerPhysicsForce_t706380940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[6] = 
{
	AfterburnerPhysicsForce_t706380940::get_offset_of_effectAngle_2(),
	AfterburnerPhysicsForce_t706380940::get_offset_of_effectWidth_3(),
	AfterburnerPhysicsForce_t706380940::get_offset_of_effectDistance_4(),
	AfterburnerPhysicsForce_t706380940::get_offset_of_force_5(),
	AfterburnerPhysicsForce_t706380940::get_offset_of_m_Cols_6(),
	AfterburnerPhysicsForce_t706380940::get_offset_of_m_Sphere_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (ExplosionFireAndDebris_t1174895067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[4] = 
{
	ExplosionFireAndDebris_t1174895067::get_offset_of_debrisPrefabs_2(),
	ExplosionFireAndDebris_t1174895067::get_offset_of_firePrefab_3(),
	ExplosionFireAndDebris_t1174895067::get_offset_of_numDebrisPieces_4(),
	ExplosionFireAndDebris_t1174895067::get_offset_of_numFires_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (U3CStartU3Ec__Iterator0_t2348924364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[10] = 
{
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U3CmultiplierU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U3CrU3E__0_1(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U3CcolsU3E__0_2(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24locvar0_3(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24locvar1_4(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U3CtestRU3E__0_5(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t2348924364::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (ExplosionPhysicsForce_t3442124285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[1] = 
{
	ExplosionPhysicsForce_t3442124285::get_offset_of_explosionForce_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (U3CStartU3Ec__Iterator0_t1741822950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[11] = 
{
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U3CmultiplierU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U3CrU3E__0_1(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U3CcolsU3E__0_2(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U3CrigidbodiesU3E__0_3(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24locvar0_4(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24locvar1_5(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24locvar2_6(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24this_7(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24current_8(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24disposing_9(),
	U3CStartU3Ec__Iterator0_t1741822950::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (Explosive_t641737877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[7] = 
{
	Explosive_t641737877::get_offset_of_explosionPrefab_2(),
	Explosive_t641737877::get_offset_of_detonationImpactVelocity_3(),
	Explosive_t641737877::get_offset_of_sizeMultiplier_4(),
	Explosive_t641737877::get_offset_of_reset_5(),
	Explosive_t641737877::get_offset_of_resetTimeDelay_6(),
	Explosive_t641737877::get_offset_of_m_Exploded_7(),
	Explosive_t641737877::get_offset_of_m_ObjectResetter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (U3COnCollisionEnterU3Ec__Iterator0_t455343489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[5] = 
{
	U3COnCollisionEnterU3Ec__Iterator0_t455343489::get_offset_of_col_0(),
	U3COnCollisionEnterU3Ec__Iterator0_t455343489::get_offset_of_U24this_1(),
	U3COnCollisionEnterU3Ec__Iterator0_t455343489::get_offset_of_U24current_2(),
	U3COnCollisionEnterU3Ec__Iterator0_t455343489::get_offset_of_U24disposing_3(),
	U3COnCollisionEnterU3Ec__Iterator0_t455343489::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (ExtinguishableParticleSystem_t2151802939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[2] = 
{
	ExtinguishableParticleSystem_t2151802939::get_offset_of_multiplier_2(),
	ExtinguishableParticleSystem_t2151802939::get_offset_of_m_Systems_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (FireLight_t2590751060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	FireLight_t2590751060::get_offset_of_m_Rnd_2(),
	FireLight_t2590751060::get_offset_of_m_Burning_3(),
	FireLight_t2590751060::get_offset_of_m_Light_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (Hose_t4212470145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[6] = 
{
	Hose_t4212470145::get_offset_of_maxPower_2(),
	Hose_t4212470145::get_offset_of_minPower_3(),
	Hose_t4212470145::get_offset_of_changeSpeed_4(),
	Hose_t4212470145::get_offset_of_hoseWaterSystems_5(),
	Hose_t4212470145::get_offset_of_systemRenderer_6(),
	Hose_t4212470145::get_offset_of_m_Power_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (ParticleSystemMultiplier_t3377687064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	ParticleSystemMultiplier_t3377687064::get_offset_of_multiplier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (SmokeParticles_t1461062090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[1] = 
{
	SmokeParticles_t1461062090::get_offset_of_extinguishSounds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (WaterHoseParticles_t2638123957), -1, sizeof(WaterHoseParticles_t2638123957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2097[4] = 
{
	WaterHoseParticles_t2638123957_StaticFields::get_offset_of_lastSoundTime_2(),
	WaterHoseParticles_t2638123957::get_offset_of_force_3(),
	WaterHoseParticles_t2638123957::get_offset_of_m_CollisionEvents_4(),
	WaterHoseParticles_t2638123957::get_offset_of_m_ParticleSystem_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ActivateTrigger_t2794485791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[5] = 
{
	ActivateTrigger_t2794485791::get_offset_of_action_2(),
	ActivateTrigger_t2794485791::get_offset_of_target_3(),
	ActivateTrigger_t2794485791::get_offset_of_source_4(),
	ActivateTrigger_t2794485791::get_offset_of_triggerCount_5(),
	ActivateTrigger_t2794485791::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Mode_t639433180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[7] = 
{
	Mode_t639433180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
