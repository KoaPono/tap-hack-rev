﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// FourCodeRefactor
struct FourCodeRefactor_t1853282147;
// MenuRefactor
struct MenuRefactor_t148745019;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialScript
struct  TutorialScript_t3587597509  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] TutorialScript::positionBorders
	GameObjectU5BU5D_t3057952154* ___positionBorders_2;
	// UnityEngine.UI.Text TutorialScript::score
	Text_t356221433 * ___score_3;
	// UnityEngine.Sprite[] TutorialScript::correctSprites
	SpriteU5BU5D_t3359083662* ___correctSprites_4;
	// UnityEngine.Sprite[] TutorialScript::incorrectSprites
	SpriteU5BU5D_t3359083662* ___incorrectSprites_5;
	// UnityEngine.Sprite TutorialScript::emptySprite
	Sprite_t309593783 * ___emptySprite_6;
	// UnityEngine.UI.Image[] TutorialScript::positionSprites
	ImageU5BU5D_t590162004* ___positionSprites_7;
	// UnityEngine.GameObject[] TutorialScript::tutorialMessages
	GameObjectU5BU5D_t3057952154* ___tutorialMessages_8;
	// System.Int32 TutorialScript::tutorialStep
	int32_t ___tutorialStep_9;
	// System.Byte TutorialScript::colorsSet
	uint8_t ___colorsSet_10;
	// System.Byte TutorialScript::positionsSet
	uint8_t ___positionsSet_11;
	// FourCodeRefactor TutorialScript::mainGameScript
	FourCodeRefactor_t1853282147 * ___mainGameScript_12;
	// MenuRefactor TutorialScript::menuController
	MenuRefactor_t148745019 * ___menuController_13;

public:
	inline static int32_t get_offset_of_positionBorders_2() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___positionBorders_2)); }
	inline GameObjectU5BU5D_t3057952154* get_positionBorders_2() const { return ___positionBorders_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_positionBorders_2() { return &___positionBorders_2; }
	inline void set_positionBorders_2(GameObjectU5BU5D_t3057952154* value)
	{
		___positionBorders_2 = value;
		Il2CppCodeGenWriteBarrier(&___positionBorders_2, value);
	}

	inline static int32_t get_offset_of_score_3() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___score_3)); }
	inline Text_t356221433 * get_score_3() const { return ___score_3; }
	inline Text_t356221433 ** get_address_of_score_3() { return &___score_3; }
	inline void set_score_3(Text_t356221433 * value)
	{
		___score_3 = value;
		Il2CppCodeGenWriteBarrier(&___score_3, value);
	}

	inline static int32_t get_offset_of_correctSprites_4() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___correctSprites_4)); }
	inline SpriteU5BU5D_t3359083662* get_correctSprites_4() const { return ___correctSprites_4; }
	inline SpriteU5BU5D_t3359083662** get_address_of_correctSprites_4() { return &___correctSprites_4; }
	inline void set_correctSprites_4(SpriteU5BU5D_t3359083662* value)
	{
		___correctSprites_4 = value;
		Il2CppCodeGenWriteBarrier(&___correctSprites_4, value);
	}

	inline static int32_t get_offset_of_incorrectSprites_5() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___incorrectSprites_5)); }
	inline SpriteU5BU5D_t3359083662* get_incorrectSprites_5() const { return ___incorrectSprites_5; }
	inline SpriteU5BU5D_t3359083662** get_address_of_incorrectSprites_5() { return &___incorrectSprites_5; }
	inline void set_incorrectSprites_5(SpriteU5BU5D_t3359083662* value)
	{
		___incorrectSprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectSprites_5, value);
	}

	inline static int32_t get_offset_of_emptySprite_6() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___emptySprite_6)); }
	inline Sprite_t309593783 * get_emptySprite_6() const { return ___emptySprite_6; }
	inline Sprite_t309593783 ** get_address_of_emptySprite_6() { return &___emptySprite_6; }
	inline void set_emptySprite_6(Sprite_t309593783 * value)
	{
		___emptySprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___emptySprite_6, value);
	}

	inline static int32_t get_offset_of_positionSprites_7() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___positionSprites_7)); }
	inline ImageU5BU5D_t590162004* get_positionSprites_7() const { return ___positionSprites_7; }
	inline ImageU5BU5D_t590162004** get_address_of_positionSprites_7() { return &___positionSprites_7; }
	inline void set_positionSprites_7(ImageU5BU5D_t590162004* value)
	{
		___positionSprites_7 = value;
		Il2CppCodeGenWriteBarrier(&___positionSprites_7, value);
	}

	inline static int32_t get_offset_of_tutorialMessages_8() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___tutorialMessages_8)); }
	inline GameObjectU5BU5D_t3057952154* get_tutorialMessages_8() const { return ___tutorialMessages_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_tutorialMessages_8() { return &___tutorialMessages_8; }
	inline void set_tutorialMessages_8(GameObjectU5BU5D_t3057952154* value)
	{
		___tutorialMessages_8 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialMessages_8, value);
	}

	inline static int32_t get_offset_of_tutorialStep_9() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___tutorialStep_9)); }
	inline int32_t get_tutorialStep_9() const { return ___tutorialStep_9; }
	inline int32_t* get_address_of_tutorialStep_9() { return &___tutorialStep_9; }
	inline void set_tutorialStep_9(int32_t value)
	{
		___tutorialStep_9 = value;
	}

	inline static int32_t get_offset_of_colorsSet_10() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___colorsSet_10)); }
	inline uint8_t get_colorsSet_10() const { return ___colorsSet_10; }
	inline uint8_t* get_address_of_colorsSet_10() { return &___colorsSet_10; }
	inline void set_colorsSet_10(uint8_t value)
	{
		___colorsSet_10 = value;
	}

	inline static int32_t get_offset_of_positionsSet_11() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___positionsSet_11)); }
	inline uint8_t get_positionsSet_11() const { return ___positionsSet_11; }
	inline uint8_t* get_address_of_positionsSet_11() { return &___positionsSet_11; }
	inline void set_positionsSet_11(uint8_t value)
	{
		___positionsSet_11 = value;
	}

	inline static int32_t get_offset_of_mainGameScript_12() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___mainGameScript_12)); }
	inline FourCodeRefactor_t1853282147 * get_mainGameScript_12() const { return ___mainGameScript_12; }
	inline FourCodeRefactor_t1853282147 ** get_address_of_mainGameScript_12() { return &___mainGameScript_12; }
	inline void set_mainGameScript_12(FourCodeRefactor_t1853282147 * value)
	{
		___mainGameScript_12 = value;
		Il2CppCodeGenWriteBarrier(&___mainGameScript_12, value);
	}

	inline static int32_t get_offset_of_menuController_13() { return static_cast<int32_t>(offsetof(TutorialScript_t3587597509, ___menuController_13)); }
	inline MenuRefactor_t148745019 * get_menuController_13() const { return ___menuController_13; }
	inline MenuRefactor_t148745019 ** get_address_of_menuController_13() { return &___menuController_13; }
	inline void set_menuController_13(MenuRefactor_t148745019 * value)
	{
		___menuController_13 = value;
		Il2CppCodeGenWriteBarrier(&___menuController_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
