﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2875670365;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// FourCodeRefactor
struct FourCodeRefactor_t1853282147;
// TutorialScript
struct TutorialScript_t3587597509;
// Grapher
struct Grapher_t1242508077;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuRefactor
struct  MenuRefactor_t148745019  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MenuRefactor::gameMenu
	GameObject_t1756533147 * ___gameMenu_2;
	// UnityEngine.GameObject MenuRefactor::gameItems
	GameObject_t1756533147 * ___gameItems_3;
	// UnityEngine.GameObject MenuRefactor::gameOver
	GameObject_t1756533147 * ___gameOver_4;
	// UnityEngine.GameObject MenuRefactor::optionsMenu
	GameObject_t1756533147 * ___optionsMenu_5;
	// UnityEngine.GameObject MenuRefactor::creditsMenu
	GameObject_t1756533147 * ___creditsMenu_6;
	// UnityEngine.GameObject MenuRefactor::yesNoMenu
	GameObject_t1756533147 * ___yesNoMenu_7;
	// UnityEngine.GameObject MenuRefactor::levelMenu
	GameObject_t1756533147 * ___levelMenu_8;
	// UnityEngine.GameObject MenuRefactor::tutorial
	GameObject_t1756533147 * ___tutorial_9;
	// UnityEngine.GameObject MenuRefactor::stats
	GameObject_t1756533147 * ___stats_10;
	// UnityEngine.GameObject MenuRefactor::color1Button
	GameObject_t1756533147 * ___color1Button_11;
	// UnityEngine.GameObject MenuRefactor::color2Button
	GameObject_t1756533147 * ___color2Button_12;
	// UnityEngine.GameObject MenuRefactor::color3Button
	GameObject_t1756533147 * ___color3Button_13;
	// UnityEngine.GameObject MenuRefactor::color4Button
	GameObject_t1756533147 * ___color4Button_14;
	// UnityEngine.GameObject MenuRefactor::color5Button
	GameObject_t1756533147 * ___color5Button_15;
	// UnityEngine.GameObject MenuRefactor::color6Button
	GameObject_t1756533147 * ___color6Button_16;
	// UnityEngine.UI.HorizontalLayoutGroup MenuRefactor::layoutGroup
	HorizontalLayoutGroup_t2875670365 * ___layoutGroup_17;
	// UnityEngine.GameObject MenuRefactor::pos1
	GameObject_t1756533147 * ___pos1_18;
	// UnityEngine.GameObject MenuRefactor::pos2
	GameObject_t1756533147 * ___pos2_19;
	// UnityEngine.GameObject MenuRefactor::pos3
	GameObject_t1756533147 * ___pos3_20;
	// UnityEngine.GameObject MenuRefactor::pos4
	GameObject_t1756533147 * ___pos4_21;
	// UnityEngine.GameObject MenuRefactor::pos5
	GameObject_t1756533147 * ___pos5_22;
	// UnityEngine.GameObject MenuRefactor::pos6
	GameObject_t1756533147 * ___pos6_23;
	// UnityEngine.AudioSource MenuRefactor::buttonSound
	AudioSource_t1135106623 * ___buttonSound_24;
	// UnityEngine.AudioSource MenuRefactor::musicSource
	AudioSource_t1135106623 * ___musicSource_25;
	// UnityEngine.Audio.AudioMixer MenuRefactor::mixer
	AudioMixer_t3244290001 * ___mixer_26;
	// UnityEngine.UI.Toggle MenuRefactor::soundFX
	Toggle_t3976754468 * ___soundFX_27;
	// UnityEngine.UI.Toggle MenuRefactor::music
	Toggle_t3976754468 * ___music_28;
	// FourCodeRefactor MenuRefactor::controller
	FourCodeRefactor_t1853282147 * ___controller_29;
	// TutorialScript MenuRefactor::tutorialScript
	TutorialScript_t3587597509 * ___tutorialScript_30;
	// Grapher MenuRefactor::graphScript
	Grapher_t1242508077 * ___graphScript_31;

public:
	inline static int32_t get_offset_of_gameMenu_2() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___gameMenu_2)); }
	inline GameObject_t1756533147 * get_gameMenu_2() const { return ___gameMenu_2; }
	inline GameObject_t1756533147 ** get_address_of_gameMenu_2() { return &___gameMenu_2; }
	inline void set_gameMenu_2(GameObject_t1756533147 * value)
	{
		___gameMenu_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameMenu_2, value);
	}

	inline static int32_t get_offset_of_gameItems_3() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___gameItems_3)); }
	inline GameObject_t1756533147 * get_gameItems_3() const { return ___gameItems_3; }
	inline GameObject_t1756533147 ** get_address_of_gameItems_3() { return &___gameItems_3; }
	inline void set_gameItems_3(GameObject_t1756533147 * value)
	{
		___gameItems_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameItems_3, value);
	}

	inline static int32_t get_offset_of_gameOver_4() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___gameOver_4)); }
	inline GameObject_t1756533147 * get_gameOver_4() const { return ___gameOver_4; }
	inline GameObject_t1756533147 ** get_address_of_gameOver_4() { return &___gameOver_4; }
	inline void set_gameOver_4(GameObject_t1756533147 * value)
	{
		___gameOver_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameOver_4, value);
	}

	inline static int32_t get_offset_of_optionsMenu_5() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___optionsMenu_5)); }
	inline GameObject_t1756533147 * get_optionsMenu_5() const { return ___optionsMenu_5; }
	inline GameObject_t1756533147 ** get_address_of_optionsMenu_5() { return &___optionsMenu_5; }
	inline void set_optionsMenu_5(GameObject_t1756533147 * value)
	{
		___optionsMenu_5 = value;
		Il2CppCodeGenWriteBarrier(&___optionsMenu_5, value);
	}

	inline static int32_t get_offset_of_creditsMenu_6() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___creditsMenu_6)); }
	inline GameObject_t1756533147 * get_creditsMenu_6() const { return ___creditsMenu_6; }
	inline GameObject_t1756533147 ** get_address_of_creditsMenu_6() { return &___creditsMenu_6; }
	inline void set_creditsMenu_6(GameObject_t1756533147 * value)
	{
		___creditsMenu_6 = value;
		Il2CppCodeGenWriteBarrier(&___creditsMenu_6, value);
	}

	inline static int32_t get_offset_of_yesNoMenu_7() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___yesNoMenu_7)); }
	inline GameObject_t1756533147 * get_yesNoMenu_7() const { return ___yesNoMenu_7; }
	inline GameObject_t1756533147 ** get_address_of_yesNoMenu_7() { return &___yesNoMenu_7; }
	inline void set_yesNoMenu_7(GameObject_t1756533147 * value)
	{
		___yesNoMenu_7 = value;
		Il2CppCodeGenWriteBarrier(&___yesNoMenu_7, value);
	}

	inline static int32_t get_offset_of_levelMenu_8() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___levelMenu_8)); }
	inline GameObject_t1756533147 * get_levelMenu_8() const { return ___levelMenu_8; }
	inline GameObject_t1756533147 ** get_address_of_levelMenu_8() { return &___levelMenu_8; }
	inline void set_levelMenu_8(GameObject_t1756533147 * value)
	{
		___levelMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___levelMenu_8, value);
	}

	inline static int32_t get_offset_of_tutorial_9() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___tutorial_9)); }
	inline GameObject_t1756533147 * get_tutorial_9() const { return ___tutorial_9; }
	inline GameObject_t1756533147 ** get_address_of_tutorial_9() { return &___tutorial_9; }
	inline void set_tutorial_9(GameObject_t1756533147 * value)
	{
		___tutorial_9 = value;
		Il2CppCodeGenWriteBarrier(&___tutorial_9, value);
	}

	inline static int32_t get_offset_of_stats_10() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___stats_10)); }
	inline GameObject_t1756533147 * get_stats_10() const { return ___stats_10; }
	inline GameObject_t1756533147 ** get_address_of_stats_10() { return &___stats_10; }
	inline void set_stats_10(GameObject_t1756533147 * value)
	{
		___stats_10 = value;
		Il2CppCodeGenWriteBarrier(&___stats_10, value);
	}

	inline static int32_t get_offset_of_color1Button_11() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color1Button_11)); }
	inline GameObject_t1756533147 * get_color1Button_11() const { return ___color1Button_11; }
	inline GameObject_t1756533147 ** get_address_of_color1Button_11() { return &___color1Button_11; }
	inline void set_color1Button_11(GameObject_t1756533147 * value)
	{
		___color1Button_11 = value;
		Il2CppCodeGenWriteBarrier(&___color1Button_11, value);
	}

	inline static int32_t get_offset_of_color2Button_12() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color2Button_12)); }
	inline GameObject_t1756533147 * get_color2Button_12() const { return ___color2Button_12; }
	inline GameObject_t1756533147 ** get_address_of_color2Button_12() { return &___color2Button_12; }
	inline void set_color2Button_12(GameObject_t1756533147 * value)
	{
		___color2Button_12 = value;
		Il2CppCodeGenWriteBarrier(&___color2Button_12, value);
	}

	inline static int32_t get_offset_of_color3Button_13() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color3Button_13)); }
	inline GameObject_t1756533147 * get_color3Button_13() const { return ___color3Button_13; }
	inline GameObject_t1756533147 ** get_address_of_color3Button_13() { return &___color3Button_13; }
	inline void set_color3Button_13(GameObject_t1756533147 * value)
	{
		___color3Button_13 = value;
		Il2CppCodeGenWriteBarrier(&___color3Button_13, value);
	}

	inline static int32_t get_offset_of_color4Button_14() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color4Button_14)); }
	inline GameObject_t1756533147 * get_color4Button_14() const { return ___color4Button_14; }
	inline GameObject_t1756533147 ** get_address_of_color4Button_14() { return &___color4Button_14; }
	inline void set_color4Button_14(GameObject_t1756533147 * value)
	{
		___color4Button_14 = value;
		Il2CppCodeGenWriteBarrier(&___color4Button_14, value);
	}

	inline static int32_t get_offset_of_color5Button_15() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color5Button_15)); }
	inline GameObject_t1756533147 * get_color5Button_15() const { return ___color5Button_15; }
	inline GameObject_t1756533147 ** get_address_of_color5Button_15() { return &___color5Button_15; }
	inline void set_color5Button_15(GameObject_t1756533147 * value)
	{
		___color5Button_15 = value;
		Il2CppCodeGenWriteBarrier(&___color5Button_15, value);
	}

	inline static int32_t get_offset_of_color6Button_16() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___color6Button_16)); }
	inline GameObject_t1756533147 * get_color6Button_16() const { return ___color6Button_16; }
	inline GameObject_t1756533147 ** get_address_of_color6Button_16() { return &___color6Button_16; }
	inline void set_color6Button_16(GameObject_t1756533147 * value)
	{
		___color6Button_16 = value;
		Il2CppCodeGenWriteBarrier(&___color6Button_16, value);
	}

	inline static int32_t get_offset_of_layoutGroup_17() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___layoutGroup_17)); }
	inline HorizontalLayoutGroup_t2875670365 * get_layoutGroup_17() const { return ___layoutGroup_17; }
	inline HorizontalLayoutGroup_t2875670365 ** get_address_of_layoutGroup_17() { return &___layoutGroup_17; }
	inline void set_layoutGroup_17(HorizontalLayoutGroup_t2875670365 * value)
	{
		___layoutGroup_17 = value;
		Il2CppCodeGenWriteBarrier(&___layoutGroup_17, value);
	}

	inline static int32_t get_offset_of_pos1_18() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos1_18)); }
	inline GameObject_t1756533147 * get_pos1_18() const { return ___pos1_18; }
	inline GameObject_t1756533147 ** get_address_of_pos1_18() { return &___pos1_18; }
	inline void set_pos1_18(GameObject_t1756533147 * value)
	{
		___pos1_18 = value;
		Il2CppCodeGenWriteBarrier(&___pos1_18, value);
	}

	inline static int32_t get_offset_of_pos2_19() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos2_19)); }
	inline GameObject_t1756533147 * get_pos2_19() const { return ___pos2_19; }
	inline GameObject_t1756533147 ** get_address_of_pos2_19() { return &___pos2_19; }
	inline void set_pos2_19(GameObject_t1756533147 * value)
	{
		___pos2_19 = value;
		Il2CppCodeGenWriteBarrier(&___pos2_19, value);
	}

	inline static int32_t get_offset_of_pos3_20() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos3_20)); }
	inline GameObject_t1756533147 * get_pos3_20() const { return ___pos3_20; }
	inline GameObject_t1756533147 ** get_address_of_pos3_20() { return &___pos3_20; }
	inline void set_pos3_20(GameObject_t1756533147 * value)
	{
		___pos3_20 = value;
		Il2CppCodeGenWriteBarrier(&___pos3_20, value);
	}

	inline static int32_t get_offset_of_pos4_21() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos4_21)); }
	inline GameObject_t1756533147 * get_pos4_21() const { return ___pos4_21; }
	inline GameObject_t1756533147 ** get_address_of_pos4_21() { return &___pos4_21; }
	inline void set_pos4_21(GameObject_t1756533147 * value)
	{
		___pos4_21 = value;
		Il2CppCodeGenWriteBarrier(&___pos4_21, value);
	}

	inline static int32_t get_offset_of_pos5_22() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos5_22)); }
	inline GameObject_t1756533147 * get_pos5_22() const { return ___pos5_22; }
	inline GameObject_t1756533147 ** get_address_of_pos5_22() { return &___pos5_22; }
	inline void set_pos5_22(GameObject_t1756533147 * value)
	{
		___pos5_22 = value;
		Il2CppCodeGenWriteBarrier(&___pos5_22, value);
	}

	inline static int32_t get_offset_of_pos6_23() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___pos6_23)); }
	inline GameObject_t1756533147 * get_pos6_23() const { return ___pos6_23; }
	inline GameObject_t1756533147 ** get_address_of_pos6_23() { return &___pos6_23; }
	inline void set_pos6_23(GameObject_t1756533147 * value)
	{
		___pos6_23 = value;
		Il2CppCodeGenWriteBarrier(&___pos6_23, value);
	}

	inline static int32_t get_offset_of_buttonSound_24() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___buttonSound_24)); }
	inline AudioSource_t1135106623 * get_buttonSound_24() const { return ___buttonSound_24; }
	inline AudioSource_t1135106623 ** get_address_of_buttonSound_24() { return &___buttonSound_24; }
	inline void set_buttonSound_24(AudioSource_t1135106623 * value)
	{
		___buttonSound_24 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSound_24, value);
	}

	inline static int32_t get_offset_of_musicSource_25() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___musicSource_25)); }
	inline AudioSource_t1135106623 * get_musicSource_25() const { return ___musicSource_25; }
	inline AudioSource_t1135106623 ** get_address_of_musicSource_25() { return &___musicSource_25; }
	inline void set_musicSource_25(AudioSource_t1135106623 * value)
	{
		___musicSource_25 = value;
		Il2CppCodeGenWriteBarrier(&___musicSource_25, value);
	}

	inline static int32_t get_offset_of_mixer_26() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___mixer_26)); }
	inline AudioMixer_t3244290001 * get_mixer_26() const { return ___mixer_26; }
	inline AudioMixer_t3244290001 ** get_address_of_mixer_26() { return &___mixer_26; }
	inline void set_mixer_26(AudioMixer_t3244290001 * value)
	{
		___mixer_26 = value;
		Il2CppCodeGenWriteBarrier(&___mixer_26, value);
	}

	inline static int32_t get_offset_of_soundFX_27() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___soundFX_27)); }
	inline Toggle_t3976754468 * get_soundFX_27() const { return ___soundFX_27; }
	inline Toggle_t3976754468 ** get_address_of_soundFX_27() { return &___soundFX_27; }
	inline void set_soundFX_27(Toggle_t3976754468 * value)
	{
		___soundFX_27 = value;
		Il2CppCodeGenWriteBarrier(&___soundFX_27, value);
	}

	inline static int32_t get_offset_of_music_28() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___music_28)); }
	inline Toggle_t3976754468 * get_music_28() const { return ___music_28; }
	inline Toggle_t3976754468 ** get_address_of_music_28() { return &___music_28; }
	inline void set_music_28(Toggle_t3976754468 * value)
	{
		___music_28 = value;
		Il2CppCodeGenWriteBarrier(&___music_28, value);
	}

	inline static int32_t get_offset_of_controller_29() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___controller_29)); }
	inline FourCodeRefactor_t1853282147 * get_controller_29() const { return ___controller_29; }
	inline FourCodeRefactor_t1853282147 ** get_address_of_controller_29() { return &___controller_29; }
	inline void set_controller_29(FourCodeRefactor_t1853282147 * value)
	{
		___controller_29 = value;
		Il2CppCodeGenWriteBarrier(&___controller_29, value);
	}

	inline static int32_t get_offset_of_tutorialScript_30() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___tutorialScript_30)); }
	inline TutorialScript_t3587597509 * get_tutorialScript_30() const { return ___tutorialScript_30; }
	inline TutorialScript_t3587597509 ** get_address_of_tutorialScript_30() { return &___tutorialScript_30; }
	inline void set_tutorialScript_30(TutorialScript_t3587597509 * value)
	{
		___tutorialScript_30 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialScript_30, value);
	}

	inline static int32_t get_offset_of_graphScript_31() { return static_cast<int32_t>(offsetof(MenuRefactor_t148745019, ___graphScript_31)); }
	inline Grapher_t1242508077 * get_graphScript_31() const { return ___graphScript_31; }
	inline Grapher_t1242508077 ** get_address_of_graphScript_31() { return &___graphScript_31; }
	inline void set_graphScript_31(Grapher_t1242508077 * value)
	{
		___graphScript_31 = value;
		Il2CppCodeGenWriteBarrier(&___graphScript_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
