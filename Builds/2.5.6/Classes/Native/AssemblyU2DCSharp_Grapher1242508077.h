﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// WMG_Axis_Graph
struct WMG_Axis_Graph_t1917651748;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// WMG_Series
struct WMG_Series_t1402980291;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grapher
struct  Grapher_t1242508077  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Grapher::noScoresText
	GameObject_t1756533147 * ___noScoresText_2;
	// UnityEngine.UI.Text Grapher::LevelIndicator
	Text_t356221433 * ___LevelIndicator_3;
	// WMG_Axis_Graph Grapher::graph
	WMG_Axis_Graph_t1917651748 * ___graph_4;
	// System.Int32 Grapher::level
	int32_t ___level_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Grapher::series1Data
	List_1_t1612828711 * ___series1Data_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Grapher::series2Data
	List_1_t1612828711 * ___series2Data_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Grapher::series3Data
	List_1_t1612828711 * ___series3Data_8;
	// WMG_Series Grapher::series1
	WMG_Series_t1402980291 * ___series1_9;
	// WMG_Series Grapher::series2
	WMG_Series_t1402980291 * ___series2_10;
	// WMG_Series Grapher::series3
	WMG_Series_t1402980291 * ___series3_11;

public:
	inline static int32_t get_offset_of_noScoresText_2() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___noScoresText_2)); }
	inline GameObject_t1756533147 * get_noScoresText_2() const { return ___noScoresText_2; }
	inline GameObject_t1756533147 ** get_address_of_noScoresText_2() { return &___noScoresText_2; }
	inline void set_noScoresText_2(GameObject_t1756533147 * value)
	{
		___noScoresText_2 = value;
		Il2CppCodeGenWriteBarrier(&___noScoresText_2, value);
	}

	inline static int32_t get_offset_of_LevelIndicator_3() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___LevelIndicator_3)); }
	inline Text_t356221433 * get_LevelIndicator_3() const { return ___LevelIndicator_3; }
	inline Text_t356221433 ** get_address_of_LevelIndicator_3() { return &___LevelIndicator_3; }
	inline void set_LevelIndicator_3(Text_t356221433 * value)
	{
		___LevelIndicator_3 = value;
		Il2CppCodeGenWriteBarrier(&___LevelIndicator_3, value);
	}

	inline static int32_t get_offset_of_graph_4() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___graph_4)); }
	inline WMG_Axis_Graph_t1917651748 * get_graph_4() const { return ___graph_4; }
	inline WMG_Axis_Graph_t1917651748 ** get_address_of_graph_4() { return &___graph_4; }
	inline void set_graph_4(WMG_Axis_Graph_t1917651748 * value)
	{
		___graph_4 = value;
		Il2CppCodeGenWriteBarrier(&___graph_4, value);
	}

	inline static int32_t get_offset_of_level_5() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___level_5)); }
	inline int32_t get_level_5() const { return ___level_5; }
	inline int32_t* get_address_of_level_5() { return &___level_5; }
	inline void set_level_5(int32_t value)
	{
		___level_5 = value;
	}

	inline static int32_t get_offset_of_series1Data_6() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series1Data_6)); }
	inline List_1_t1612828711 * get_series1Data_6() const { return ___series1Data_6; }
	inline List_1_t1612828711 ** get_address_of_series1Data_6() { return &___series1Data_6; }
	inline void set_series1Data_6(List_1_t1612828711 * value)
	{
		___series1Data_6 = value;
		Il2CppCodeGenWriteBarrier(&___series1Data_6, value);
	}

	inline static int32_t get_offset_of_series2Data_7() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series2Data_7)); }
	inline List_1_t1612828711 * get_series2Data_7() const { return ___series2Data_7; }
	inline List_1_t1612828711 ** get_address_of_series2Data_7() { return &___series2Data_7; }
	inline void set_series2Data_7(List_1_t1612828711 * value)
	{
		___series2Data_7 = value;
		Il2CppCodeGenWriteBarrier(&___series2Data_7, value);
	}

	inline static int32_t get_offset_of_series3Data_8() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series3Data_8)); }
	inline List_1_t1612828711 * get_series3Data_8() const { return ___series3Data_8; }
	inline List_1_t1612828711 ** get_address_of_series3Data_8() { return &___series3Data_8; }
	inline void set_series3Data_8(List_1_t1612828711 * value)
	{
		___series3Data_8 = value;
		Il2CppCodeGenWriteBarrier(&___series3Data_8, value);
	}

	inline static int32_t get_offset_of_series1_9() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series1_9)); }
	inline WMG_Series_t1402980291 * get_series1_9() const { return ___series1_9; }
	inline WMG_Series_t1402980291 ** get_address_of_series1_9() { return &___series1_9; }
	inline void set_series1_9(WMG_Series_t1402980291 * value)
	{
		___series1_9 = value;
		Il2CppCodeGenWriteBarrier(&___series1_9, value);
	}

	inline static int32_t get_offset_of_series2_10() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series2_10)); }
	inline WMG_Series_t1402980291 * get_series2_10() const { return ___series2_10; }
	inline WMG_Series_t1402980291 ** get_address_of_series2_10() { return &___series2_10; }
	inline void set_series2_10(WMG_Series_t1402980291 * value)
	{
		___series2_10 = value;
		Il2CppCodeGenWriteBarrier(&___series2_10, value);
	}

	inline static int32_t get_offset_of_series3_11() { return static_cast<int32_t>(offsetof(Grapher_t1242508077, ___series3_11)); }
	inline WMG_Series_t1402980291 * get_series3_11() const { return ___series3_11; }
	inline WMG_Series_t1402980291 ** get_address_of_series3_11() { return &___series3_11; }
	inline void set_series3_11(WMG_Series_t1402980291 * value)
	{
		___series3_11 = value;
		Il2CppCodeGenWriteBarrier(&___series3_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
