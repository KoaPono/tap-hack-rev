﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_WMG_X_Data_Provider2067837921.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic2558512.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CstartTestsU3Ec_3289998143.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CautofitTestsU3Ec368292594.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CgroupingTestsU33238827950.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CseriesTestsU3Ec_253186807.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CseriesTestsU3Ec1801059355.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CautoAnimationTe1187552896.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CanimationFuncti3046996838.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CdynamicDataPopul429000384.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CrealTimeTestsU3E715726851.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CaxisAutoGrowShr3521181599.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3ChideShowTestsU33071683165.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CgridsTicksTests3931833123.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CsizeTestsU3Ec__I698533734.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3ClegendTestsU3Ec_431339985.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CdataLabelsTests3613977306.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CgraphTypeAndOrie227549071.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CaxesTestsU3Ec__1651822582.h"
#include "AssemblyU2DCSharp_WMG_X_Dynamic_U3CaddDeleteTestsU2302646472.h"
#include "AssemblyU2DCSharp_WMG_X_Equation_Plotter3633801868.h"
#include "AssemblyU2DCSharp_WMG_X_Interactive_Pie2251582480.h"
#include "AssemblyU2DCSharp_WMG_X_Pie_Ring_Graph3884569785.h"
#include "AssemblyU2DCSharp_WMG_X_Plot_Overtime2667371262.h"
#include "AssemblyU2DCSharp_WMG_X_Plot_Overtime_U3CplotDataU4222832535.h"
#include "AssemblyU2DCSharp_WMG_X_Plot_Overtime_U3CanimateAd1670877990.h"
#include "AssemblyU2DCSharp_WMG_X_Plot_Overtime_U3CanimateAd1267593463.h"
#include "AssemblyU2DCSharp_WMG_X_Ring_Graph4116428606.h"
#include "AssemblyU2DCSharp_WMG_X_Simple_Pie139636216.h"
#include "AssemblyU2DCSharp_WMG_X_Tutorial_180871881.h"
#include "AssemblyU2DCSharp_WMG_X_WorldSpace3014178741.h"
#include "AssemblyU2DCSharp_WMG_Anim3826093003.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimFillU3Ec__AnonSt2100498401.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimColorU3Ec__AnonSt421965738.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimPositionCallback3849653507.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimPositionU3Ec__An3439622110.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimSizeU3Ec__AnonSt3524999375.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimPositionCallback3104094060.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimScaleU3Ec__AnonS2377062316.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimScaleCallbackCU32965086279.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimScaleSeqInsertU34051391738.h"
#include "AssemblyU2DCSharp_WMG_Anim_U3CanimScaleSeqAppendU32884928646.h"
#include "AssemblyU2DCSharp_WMG_Events2095434391.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Click_H2038945967.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Link_Click_H3723536134.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Click_Leg_H2868657926.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Link_Click_Leg_H1368770573.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Pie_Slice_Click_H803247839.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Pie_Legend_Entry_1481709577.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Ring_Click_H1255376928.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_MouseEnter_H3761598830.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Link_MouseEnter_H730029855.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_MouseEnter_Leg_H2678812709.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Link_MouseEnter_L3994755864.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Pie_Slice_MouseEn2963085410.h"
#include "AssemblyU2DCSharp_WMG_Events_WMG_Ring_MouseEnter_H1605691837.h"
#include "AssemblyU2DCSharp_WMG_Events_U3CAddEventTriggerU3E4115898400.h"
#include "AssemblyU2DCSharp_WMG_Events_U3CAddEventTriggerU3E1387015045.h"
#include "AssemblyU2DCSharp_WMG_GUI_Functions3530287521.h"
#include "AssemblyU2DCSharp_WMG_Graph_Tooltip492427502.h"
#include "AssemblyU2DCSharp_WMG_Graph_Tooltip_TooltipLabeler4259093069.h"
#include "AssemblyU2DCSharp_WMG_Image_Custom_Mat148235638.h"
#include "AssemblyU2DCSharp_WMG_Raycaster3137594770.h"
#include "AssemblyU2DCSharp_WMG_Raycatcher3275114186.h"
#include "AssemblyU2DCSharp_WMG_Text_Functions2987185919.h"
#include "AssemblyU2DCSharp_WMG_Text_Functions_WMGpivotTypes3493693960.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph1917651748.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_graphTypes1920111748.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_orientationTypes133738122.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_axesTypes996308199.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_ResizeProperties554172928.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_AutoPaddingProper4059460018.h"
#include "AssemblyU2DCSharp_WMG_Axis_Graph_GraphBackgroundCh2639770971.h"
#include "AssemblyU2DCSharp_WMG_Bezier_Band_Graph3566287070.h"
#include "AssemblyU2DCSharp_WMG_Grid2568926986.h"
#include "AssemblyU2DCSharp_WMG_Grid_gridTypes1177259200.h"
#include "AssemblyU2DCSharp_WMG_Hierarchical_Tree2894465612.h"
#include "AssemblyU2DCSharp_WMG_Hierarchical_Tree_ResizeProp3067266120.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph252836487.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_sortMethod345387417.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_ResizeProperties2442552467.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_U3CUpdateVisualsU3E894148875.h"
#include "AssemblyU2DCSharp_WMG_Pie_Graph_U3CshrinkSlicesU3E4228999192.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1914[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (WMG_X_Data_Provider_t2067837921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[9] = 
{
	WMG_X_Data_Provider_t2067837921::get_offset_of_dataField_2(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_labelField_3(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_idField_4(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_vec1_5(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_vec2_6(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_vec3_7(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_U3CTestPropertyU3Ek__BackingField_8(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_U3CTestPropertyWithFieldsU3Ek__BackingField_9(),
	WMG_X_Data_Provider_t2067837921::get_offset_of_vector2List_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (WMG_X_Dynamic_t2558512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[11] = 
{
	WMG_X_Dynamic_t2558512::get_offset_of_graphPrefab_2(),
	WMG_X_Dynamic_t2558512::get_offset_of_graph_3(),
	WMG_X_Dynamic_t2558512::get_offset_of_performTests_4(),
	WMG_X_Dynamic_t2558512::get_offset_of_noTestDelay_5(),
	WMG_X_Dynamic_t2558512::get_offset_of_testInterval_6(),
	WMG_X_Dynamic_t2558512::get_offset_of_testGroupInterval_7(),
	WMG_X_Dynamic_t2558512::get_offset_of_easeType_8(),
	WMG_X_Dynamic_t2558512::get_offset_of_realTimePrefab_9(),
	WMG_X_Dynamic_t2558512::get_offset_of_realTimeObj_10(),
	WMG_X_Dynamic_t2558512::get_offset_of_animDuration_11(),
	WMG_X_Dynamic_t2558512::get_offset_of_waitTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (U3CstartTestsU3Ec__Iterator0_t3289998143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[4] = 
{
	U3CstartTestsU3Ec__Iterator0_t3289998143::get_offset_of_U24this_0(),
	U3CstartTestsU3Ec__Iterator0_t3289998143::get_offset_of_U24current_1(),
	U3CstartTestsU3Ec__Iterator0_t3289998143::get_offset_of_U24disposing_2(),
	U3CstartTestsU3Ec__Iterator0_t3289998143::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (U3CautofitTestsU3Ec__Iterator1_t368292594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[7] = 
{
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U3Cs1U3E__0_0(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U3Cs2U3E__0_1(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U3Cs3U3E__0_2(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U24this_3(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U24current_4(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U24disposing_5(),
	U3CautofitTestsU3Ec__Iterator1_t368292594::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (U3CgroupingTestsU3Ec__Iterator2_t3238827950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[9] = 
{
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U3CxLabelsU3E__0_0(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U3Cs1U3E__0_1(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U3Cp1U3E__0_2(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U3Cp2U3E__0_3(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U3Cp3U3E__0_4(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U24this_5(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U24current_6(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U24disposing_7(),
	U3CgroupingTestsU3Ec__Iterator2_t3238827950::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (U3CseriesTestsU3Ec__Iterator3_t253186807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[12] = 
{
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3Cs2U3E__0_0(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3Cs1DataU3E__0_1(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3Cs2DataU3E__0_2(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3Cs1PointColorU3E__0_3(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3Cs2PointColorU3E__0_4(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3CorigSizeU3E__0_5(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U3CpointColorsU3E__0_6(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U24this_7(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U24current_8(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U24disposing_9(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U24PC_10(),
	U3CseriesTestsU3Ec__Iterator3_t253186807::get_offset_of_U24locvar0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (U3CseriesTestsU3Ec__AnonStorey11_t1801059355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[2] = 
{
	U3CseriesTestsU3Ec__AnonStorey11_t1801059355::get_offset_of_s1_0(),
	U3CseriesTestsU3Ec__AnonStorey11_t1801059355::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (U3CautoAnimationTestsU3Ec__Iterator4_t1187552896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[9] = 
{
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U3Cs1U3E__0_0(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U3Cs2U3E__0_1(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U3Cs1DataU3E__0_2(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U3Cs2DataU3E__0_3(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U3Cs1Data2U3E__0_4(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U24this_5(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U24current_6(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U24disposing_7(),
	U3CautoAnimationTestsU3Ec__Iterator4_t1187552896::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[9] = 
{
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U3CbeforeScaleLineU3E__0_0(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U3CafterScaleLineU3E__0_1(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U3CbeforeScalePointU3E__0_2(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U3CafterScalePointU3E__0_3(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U3CbeforeScaleBarU3E__0_4(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U24this_5(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U24current_6(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U24disposing_7(),
	U3CanimationFunctionTestsU3Ec__Iterator5_t3046996838::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[9] = 
{
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U3CdsU3E__0_0(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U3CrandomDataU3E__0_1(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U3CdataProvidersU3E__0_2(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U3Cs1U3E__0_3(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U3Cs1DataU3E__0_4(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U24this_5(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U24current_6(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U24disposing_7(),
	U3CdynamicDataPopulationViaReflectionTestsU3Ec__Iterator6_t429000384::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (U3CrealTimeTestsU3Ec__Iterator7_t715726851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[8] = 
{
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U3Cds1U3E__0_0(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U3Cds2U3E__0_1(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U3Cs1U3E__0_2(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U3Cs2U3E__0_3(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U24this_4(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U24current_5(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U24disposing_6(),
	U3CrealTimeTestsU3Ec__Iterator7_t715726851::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[6] = 
{
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U3Cs1U3E__0_0(),
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U3Cs2U3E__0_1(),
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U24this_2(),
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U24current_3(),
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U24disposing_4(),
	U3CaxisAutoGrowShrinkTestsU3Ec__Iterator8_t3521181599::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (U3ChideShowTestsU3Ec__Iterator9_t3071683165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[6] = 
{
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U3Cs1U3E__0_0(),
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U3Cs2U3E__0_1(),
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U24this_2(),
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U24current_3(),
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U24disposing_4(),
	U3ChideShowTestsU3Ec__Iterator9_t3071683165::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (U3CgridsTicksTestsU3Ec__IteratorA_t3931833123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[5] = 
{
	U3CgridsTicksTestsU3Ec__IteratorA_t3931833123::get_offset_of_U3CxLabelsU3E__0_0(),
	U3CgridsTicksTestsU3Ec__IteratorA_t3931833123::get_offset_of_U24this_1(),
	U3CgridsTicksTestsU3Ec__IteratorA_t3931833123::get_offset_of_U24current_2(),
	U3CgridsTicksTestsU3Ec__IteratorA_t3931833123::get_offset_of_U24disposing_3(),
	U3CgridsTicksTestsU3Ec__IteratorA_t3931833123::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (U3CsizeTestsU3Ec__IteratorB_t698533734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[5] = 
{
	U3CsizeTestsU3Ec__IteratorB_t698533734::get_offset_of_U3CorigSizeU3E__0_0(),
	U3CsizeTestsU3Ec__IteratorB_t698533734::get_offset_of_U24this_1(),
	U3CsizeTestsU3Ec__IteratorB_t698533734::get_offset_of_U24current_2(),
	U3CsizeTestsU3Ec__IteratorB_t698533734::get_offset_of_U24disposing_3(),
	U3CsizeTestsU3Ec__IteratorB_t698533734::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (U3ClegendTestsU3Ec__IteratorC_t431339985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[4] = 
{
	U3ClegendTestsU3Ec__IteratorC_t431339985::get_offset_of_U24this_0(),
	U3ClegendTestsU3Ec__IteratorC_t431339985::get_offset_of_U24current_1(),
	U3ClegendTestsU3Ec__IteratorC_t431339985::get_offset_of_U24disposing_2(),
	U3ClegendTestsU3Ec__IteratorC_t431339985::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (U3CdataLabelsTestsU3Ec__IteratorD_t3613977306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[6] = 
{
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U3Cs1U3E__0_0(),
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U3Cs2U3E__0_1(),
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U24this_2(),
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U24current_3(),
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U24disposing_4(),
	U3CdataLabelsTestsU3Ec__IteratorD_t3613977306::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (U3CgraphTypeAndOrientationTestsU3Ec__IteratorE_t227549071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[4] = 
{
	U3CgraphTypeAndOrientationTestsU3Ec__IteratorE_t227549071::get_offset_of_U24this_0(),
	U3CgraphTypeAndOrientationTestsU3Ec__IteratorE_t227549071::get_offset_of_U24current_1(),
	U3CgraphTypeAndOrientationTestsU3Ec__IteratorE_t227549071::get_offset_of_U24disposing_2(),
	U3CgraphTypeAndOrientationTestsU3Ec__IteratorE_t227549071::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (U3CaxesTestsU3Ec__IteratorF_t1651822582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[4] = 
{
	U3CaxesTestsU3Ec__IteratorF_t1651822582::get_offset_of_U24this_0(),
	U3CaxesTestsU3Ec__IteratorF_t1651822582::get_offset_of_U24current_1(),
	U3CaxesTestsU3Ec__IteratorF_t1651822582::get_offset_of_U24disposing_2(),
	U3CaxesTestsU3Ec__IteratorF_t1651822582::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (U3CaddDeleteTestsU3Ec__Iterator10_t2302646472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[11] = 
{
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs1U3E__0_0(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs2U3E__0_1(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs1DataU3E__0_2(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs2DataU3E__0_3(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs1PointColorU3E__0_4(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3Cs2PointColorU3E__0_5(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U3CbarWidthU3E__0_6(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U24this_7(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U24current_8(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U24disposing_9(),
	U3CaddDeleteTestsU3Ec__Iterator10_t2302646472::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (WMG_X_Equation_Plotter_t3633801868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[10] = 
{
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_emptyGraphPrefab_2(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_equationStr_3(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_minX_4(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_maxX_5(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_intervalX_6(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_numDecimalsToRound_7(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_graph_8(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_series_9(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_decimalsMultiplier_10(),
	WMG_X_Equation_Plotter_t3633801868::get_offset_of_operatorAndParenthesesRegex_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (WMG_X_Interactive_Pie_t2251582480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[2] = 
{
	WMG_X_Interactive_Pie_t2251582480::get_offset_of_pieGraphPrefab_2(),
	WMG_X_Interactive_Pie_t2251582480::get_offset_of_graph_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (WMG_X_Pie_Ring_Graph_t3884569785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[1] = 
{
	WMG_X_Pie_Ring_Graph_t3884569785::get_offset_of_ringGraphPrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (WMG_X_Plot_Overtime_t2667371262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[27] = 
{
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_emptyGraphPrefab_2(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_plotOnStart_3(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of__plottingData_4(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_plotIntervalSeconds_5(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_plotAnimationSeconds_6(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_plotEaseType_7(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_xInterval_8(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_useAreaShading_9(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_useComputeShader_10(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_blinkCurrentPoint_11(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_blinkAnimDuration_12(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_blinkScale_13(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_moveXaxisMinimum_14(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_indicatorPrefab_15(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_indicatorNumDecimals_16(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_changeObjs_17(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_plottingDataC_18(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_graph_19(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_series1_20(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_graphOverlay_21(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_indicatorGO_22(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_tooltipNumberFormatInfo_23(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_yAxisNumberFormatInfo_24(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_seriesDataLabelsNumberFormatInfo_25(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_indicatorLabelNumberFormatInfo_26(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_addPointAnimTimeline_27(),
	WMG_X_Plot_Overtime_t2667371262::get_offset_of_blinkingTween_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (U3CplotDataU3Ec__Iterator0_t4222832535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[4] = 
{
	U3CplotDataU3Ec__Iterator0_t4222832535::get_offset_of_U24this_0(),
	U3CplotDataU3Ec__Iterator0_t4222832535::get_offset_of_U24current_1(),
	U3CplotDataU3Ec__Iterator0_t4222832535::get_offset_of_U24disposing_2(),
	U3CplotDataU3Ec__Iterator0_t4222832535::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (U3CanimateAddPointFromEndU3Ec__AnonStorey1_t1670877990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[2] = 
{
	U3CanimateAddPointFromEndU3Ec__AnonStorey1_t1670877990::get_offset_of_pointVec_0(),
	U3CanimateAddPointFromEndU3Ec__AnonStorey1_t1670877990::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (U3CanimateAddPointFromEndU3Ec__AnonStorey2_t1267593463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[4] = 
{
	U3CanimateAddPointFromEndU3Ec__AnonStorey2_t1267593463::get_offset_of_oldEnd_0(),
	U3CanimateAddPointFromEndU3Ec__AnonStorey2_t1267593463::get_offset_of_newStart_1(),
	U3CanimateAddPointFromEndU3Ec__AnonStorey2_t1267593463::get_offset_of_oldStart_2(),
	U3CanimateAddPointFromEndU3Ec__AnonStorey2_t1267593463::get_offset_of_U3CU3Ef__refU241_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (WMG_X_Ring_Graph_t4116428606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[3] = 
{
	WMG_X_Ring_Graph_t4116428606::get_offset_of_ringGraphPrefab_2(),
	WMG_X_Ring_Graph_t4116428606::get_offset_of_onlyRandomizeData_3(),
	WMG_X_Ring_Graph_t4116428606::get_offset_of_ringGraphs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (WMG_X_Simple_Pie_t139636216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[4] = 
{
	WMG_X_Simple_Pie_t139636216::get_offset_of_emptyPiePrefab_2(),
	WMG_X_Simple_Pie_t139636216::get_offset_of_pieGraph_3(),
	WMG_X_Simple_Pie_t139636216::get_offset_of_testData_4(),
	WMG_X_Simple_Pie_t139636216::get_offset_of_testStrings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (WMG_X_Tutorial_1_t80871881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[6] = 
{
	WMG_X_Tutorial_1_t80871881::get_offset_of_emptyGraphPrefab_2(),
	WMG_X_Tutorial_1_t80871881::get_offset_of_graph_3(),
	WMG_X_Tutorial_1_t80871881::get_offset_of_series1_4(),
	WMG_X_Tutorial_1_t80871881::get_offset_of_series1Data_5(),
	WMG_X_Tutorial_1_t80871881::get_offset_of_useData2_6(),
	WMG_X_Tutorial_1_t80871881::get_offset_of_series1Data2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (WMG_X_WorldSpace_t3014178741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[3] = 
{
	WMG_X_WorldSpace_t3014178741::get_offset_of_graph_2(),
	WMG_X_WorldSpace_t3014178741::get_offset_of_toolTipPanel_3(),
	WMG_X_WorldSpace_t3014178741::get_offset_of_toolTipLabel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (WMG_Anim_t3826093003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (U3CanimFillU3Ec__AnonStorey0_t2100498401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[1] = 
{
	U3CanimFillU3Ec__AnonStorey0_t2100498401::get_offset_of_comp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (U3CanimColorU3Ec__AnonStorey1_t421965738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[1] = 
{
	U3CanimColorU3Ec__AnonStorey1_t421965738::get_offset_of_comp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (U3CanimPositionCallbackCU3Ec__AnonStorey2_t3849653507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	U3CanimPositionCallbackCU3Ec__AnonStorey2_t3849653507::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (U3CanimPositionU3Ec__AnonStorey3_t3439622110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[1] = 
{
	U3CanimPositionU3Ec__AnonStorey3_t3439622110::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (U3CanimSizeU3Ec__AnonStorey4_t3524999375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[1] = 
{
	U3CanimSizeU3Ec__AnonStorey4_t3524999375::get_offset_of_comp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (U3CanimPositionCallbacksU3Ec__AnonStorey5_t3104094060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[1] = 
{
	U3CanimPositionCallbacksU3Ec__AnonStorey5_t3104094060::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (U3CanimScaleU3Ec__AnonStorey6_t2377062316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[1] = 
{
	U3CanimScaleU3Ec__AnonStorey6_t2377062316::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (U3CanimScaleCallbackCU3Ec__AnonStorey7_t2965086279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[1] = 
{
	U3CanimScaleCallbackCU3Ec__AnonStorey7_t2965086279::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (U3CanimScaleSeqInsertU3Ec__AnonStorey8_t4051391738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[1] = 
{
	U3CanimScaleSeqInsertU3Ec__AnonStorey8_t4051391738::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (U3CanimScaleSeqAppendU3Ec__AnonStorey9_t2884928646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[1] = 
{
	U3CanimScaleSeqAppendU3Ec__AnonStorey9_t2884928646::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (WMG_Events_t2095434391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[13] = 
{
	WMG_Events_t2095434391::get_offset_of_WMG_Click_2(),
	WMG_Events_t2095434391::get_offset_of_WMG_Link_Click_3(),
	WMG_Events_t2095434391::get_offset_of_WMG_Click_Leg_4(),
	WMG_Events_t2095434391::get_offset_of_WMG_Link_Click_Leg_5(),
	WMG_Events_t2095434391::get_offset_of_WMG_Pie_Slice_Click_6(),
	WMG_Events_t2095434391::get_offset_of_WMG_Pie_Legend_Entry_Click_7(),
	WMG_Events_t2095434391::get_offset_of_WMG_Ring_Click_8(),
	WMG_Events_t2095434391::get_offset_of_WMG_MouseEnter_9(),
	WMG_Events_t2095434391::get_offset_of_WMG_Link_MouseEnter_10(),
	WMG_Events_t2095434391::get_offset_of_WMG_MouseEnter_Leg_11(),
	WMG_Events_t2095434391::get_offset_of_WMG_Link_MouseEnter_Leg_12(),
	WMG_Events_t2095434391::get_offset_of_WMG_Pie_Slice_MouseEnter_13(),
	WMG_Events_t2095434391::get_offset_of_WMG_Ring_MouseEnter_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (WMG_Click_H_t2038945967), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (WMG_Link_Click_H_t3723536134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (WMG_Click_Leg_H_t2868657926), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (WMG_Link_Click_Leg_H_t1368770573), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (WMG_Pie_Slice_Click_H_t803247839), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (WMG_Pie_Legend_Entry_Click_H_t1481709577), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (WMG_Ring_Click_H_t1255376928), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (WMG_MouseEnter_H_t3761598830), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (WMG_Link_MouseEnter_H_t730029855), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (WMG_MouseEnter_Leg_H_t2678812709), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (WMG_Link_MouseEnter_Leg_H_t3994755864), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (WMG_Pie_Slice_MouseEnter_H_t2963085410), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (WMG_Ring_MouseEnter_H_t1605691837), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (U3CAddEventTriggerU3Ec__AnonStorey0_t4115898400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[2] = 
{
	U3CAddEventTriggerU3Ec__AnonStorey0_t4115898400::get_offset_of_action_0(),
	U3CAddEventTriggerU3Ec__AnonStorey0_t4115898400::get_offset_of_go_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U3CAddEventTriggerU3Ec__AnonStorey1_t1387015045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[3] = 
{
	U3CAddEventTriggerU3Ec__AnonStorey1_t1387015045::get_offset_of_action_0(),
	U3CAddEventTriggerU3Ec__AnonStorey1_t1387015045::get_offset_of_go_1(),
	U3CAddEventTriggerU3Ec__AnonStorey1_t1387015045::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (WMG_GUI_Functions_t3530287521), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (WMG_Graph_Tooltip_t492427502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[5] = 
{
	WMG_Graph_Tooltip_t492427502::get_offset_of_tooltipLabeler_2(),
	WMG_Graph_Tooltip_t492427502::get_offset_of_theGraph_3(),
	WMG_Graph_Tooltip_t492427502::get_offset_of__canvas_4(),
	WMG_Graph_Tooltip_t492427502::get_offset_of_currentObj_5(),
	WMG_Graph_Tooltip_t492427502::get_offset_of__cg_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (TooltipLabeler_t4259093069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (WMG_Image_Custom_Mat_t148235638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (WMG_Raycaster_t3137594770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[2] = 
{
	WMG_Raycaster_t3137594770::get_offset_of_AlphaThreshold_10(),
	WMG_Raycaster_t3137594770::get_offset_of_exclusions_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (WMG_Raycatcher_t3275114186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[1] = 
{
	WMG_Raycatcher_t3275114186::get_offset_of_IncludeMaterialAlpha_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (WMG_Text_Functions_t2987185919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (WMGpivotTypes_t3493693960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1982[10] = 
{
	WMGpivotTypes_t3493693960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (WMG_Axis_Graph_t1917651748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[86] = 
{
	WMG_Axis_Graph_t1917651748::get_offset_of_yAxis_19(),
	WMG_Axis_Graph_t1917651748::get_offset_of_xAxis_20(),
	WMG_Axis_Graph_t1917651748::get_offset_of_yAxis2_21(),
	WMG_Axis_Graph_t1917651748::get_offset_of__groups_22(),
	WMG_Axis_Graph_t1917651748::get_offset_of_groups_23(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipOffset_24(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipNumberDecimals_25(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipDisplaySeriesName_26(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipAnimationsEnabled_27(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipAnimationsEasetype_28(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipAnimationsDuration_29(),
	WMG_Axis_Graph_t1917651748::get_offset_of_autoAnimationsEasetype_30(),
	WMG_Axis_Graph_t1917651748::get_offset_of_autoAnimationsDuration_31(),
	WMG_Axis_Graph_t1917651748::get_offset_of_lineSeries_32(),
	WMG_Axis_Graph_t1917651748::get_offset_of_pointPrefabs_33(),
	WMG_Axis_Graph_t1917651748::get_offset_of_linkPrefabs_34(),
	WMG_Axis_Graph_t1917651748::get_offset_of_barPrefab_35(),
	WMG_Axis_Graph_t1917651748::get_offset_of_seriesPrefab_36(),
	WMG_Axis_Graph_t1917651748::get_offset_of_legend_37(),
	WMG_Axis_Graph_t1917651748::get_offset_of_graphTitle_38(),
	WMG_Axis_Graph_t1917651748::get_offset_of_graphBackground_39(),
	WMG_Axis_Graph_t1917651748::get_offset_of_anchoredParent_40(),
	WMG_Axis_Graph_t1917651748::get_offset_of_graphAreaBoundsParent_41(),
	WMG_Axis_Graph_t1917651748::get_offset_of_seriesParent_42(),
	WMG_Axis_Graph_t1917651748::get_offset_of_toolTipPanel_43(),
	WMG_Axis_Graph_t1917651748::get_offset_of_toolTipLabel_44(),
	WMG_Axis_Graph_t1917651748::get_offset_of_theTooltip_45(),
	WMG_Axis_Graph_t1917651748::get_offset_of_resizingChangesFontScaleInsteadOfFontSize_46(),
	WMG_Axis_Graph_t1917651748::get_offset_of__graphType_47(),
	WMG_Axis_Graph_t1917651748::get_offset_of__orientationType_48(),
	WMG_Axis_Graph_t1917651748::get_offset_of__axesType_49(),
	WMG_Axis_Graph_t1917651748::get_offset_of__resizeEnabled_50(),
	WMG_Axis_Graph_t1917651748::get_offset_of__resizeProperties_51(),
	WMG_Axis_Graph_t1917651748::get_offset_of__useGroups_52(),
	WMG_Axis_Graph_t1917651748::get_offset_of__paddingLeftRight_53(),
	WMG_Axis_Graph_t1917651748::get_offset_of__paddingTopBottom_54(),
	WMG_Axis_Graph_t1917651748::get_offset_of__theOrigin_55(),
	WMG_Axis_Graph_t1917651748::get_offset_of__barWidth_56(),
	WMG_Axis_Graph_t1917651748::get_offset_of__barAxisValue_57(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoUpdateOrigin_58(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoUpdateBarWidth_59(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoUpdateBarWidthSpacing_60(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoUpdateSeriesAxisSpacing_61(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoUpdateBarAxisValue_62(),
	WMG_Axis_Graph_t1917651748::get_offset_of__axisWidth_63(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoShrinkAtPercent_64(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoGrowAndShrinkByPercent_65(),
	WMG_Axis_Graph_t1917651748::get_offset_of__tooltipEnabled_66(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoAnimationsEnabled_67(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoPaddingProperties_68(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoPaddingEnabled_69(),
	WMG_Axis_Graph_t1917651748::get_offset_of__autoPaddingAmount_70(),
	WMG_Axis_Graph_t1917651748::get_offset_of__tickSize_71(),
	WMG_Axis_Graph_t1917651748::get_offset_of__graphTitleString_72(),
	WMG_Axis_Graph_t1917651748::get_offset_of__graphTitleOffset_73(),
	WMG_Axis_Graph_t1917651748::get_offset_of__graphTitleSize_74(),
	WMG_Axis_Graph_t1917651748::get_offset_of_totalPointValues_75(),
	WMG_Axis_Graph_t1917651748::get_offset_of_maxSeriesPointCount_76(),
	WMG_Axis_Graph_t1917651748::get_offset_of_maxSeriesBarCount_77(),
	WMG_Axis_Graph_t1917651748::get_offset_of_numComboBarSeries_78(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origWidth_79(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origHeight_80(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origBarWidth_81(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origAxisWidth_82(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origAutoPaddingAmount_83(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origTickSize_84(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origPaddingLeftRight_85(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origPaddingTopBottom_86(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origGraphTitleOffset_87(),
	WMG_Axis_Graph_t1917651748::get_offset_of_origGraphTitleSize_88(),
	WMG_Axis_Graph_t1917651748::get_offset_of_cachedContainerWidth_89(),
	WMG_Axis_Graph_t1917651748::get_offset_of_cachedContainerHeight_90(),
	WMG_Axis_Graph_t1917651748::get_offset_of_autoAnim_91(),
	WMG_Axis_Graph_t1917651748::get_offset_of_hasInit_92(),
	WMG_Axis_Graph_t1917651748::get_offset_of_changeObjs_93(),
	WMG_Axis_Graph_t1917651748::get_offset_of_graphC_94(),
	WMG_Axis_Graph_t1917651748::get_offset_of_seriesCountC_95(),
	WMG_Axis_Graph_t1917651748::get_offset_of_seriesNoCountC_96(),
	WMG_Axis_Graph_t1917651748::get_offset_of_tooltipEnabledC_97(),
	WMG_Axis_Graph_t1917651748::get_offset_of_autoAnimEnabledC_98(),
	WMG_Axis_Graph_t1917651748::get_offset_of_orientationC_99(),
	WMG_Axis_Graph_t1917651748::get_offset_of_graphTypeC_100(),
	WMG_Axis_Graph_t1917651748::get_offset_of_autoPaddingC_101(),
	WMG_Axis_Graph_t1917651748::get_offset_of_GraphBackgroundChanged_102(),
	WMG_Axis_Graph_t1917651748::get_offset_of_U3CautoFitLabelsU3Ek__BackingField_103(),
	WMG_Axis_Graph_t1917651748::get_offset_of_U3CautoFitPaddingU3Ek__BackingField_104(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (graphTypes_t1920111748)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[7] = 
{
	graphTypes_t1920111748::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (orientationTypes_t133738122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[3] = 
{
	orientationTypes_t133738122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (axesTypes_t996308199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[15] = 
{
	axesTypes_t996308199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (ResizeProperties_t554172928)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[20] = 
{
	ResizeProperties_t554172928::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (AutoPaddingProperties_t4059460018)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[6] = 
{
	AutoPaddingProperties_t4059460018::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (GraphBackgroundChangedHandler_t2639770971), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (WMG_Bezier_Band_Graph_t3566287070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[26] = 
{
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__values_19(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_values_20(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__labels_21(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_labels_22(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__fillColors_23(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_fillColors_24(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_textureResolution_25(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_bandsParent_26(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_bandPrefab_27(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__bandLineColor_28(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__startHeightPercent_29(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__bandSpacing_30(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__bandLineWidth_31(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__cubicBezierP1_32(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__cubicBezierP2_33(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__numDecimals_34(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of__fontSize_35(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_U3CbandsU3Ek__BackingField_36(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_U3CTotalValU3Ek__BackingField_37(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_changeObjs_38(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_allC_39(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_cubicBezierC_40(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_labelsC_41(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_colorsC_42(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_fontC_43(),
	WMG_Bezier_Band_Graph_t3566287070::get_offset_of_hasInit_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (WMG_Grid_t2568926986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[28] = 
{
	WMG_Grid_t2568926986::get_offset_of_autoRefresh_19(),
	WMG_Grid_t2568926986::get_offset_of_gridType_20(),
	WMG_Grid_t2568926986::get_offset_of_nodePrefab_21(),
	WMG_Grid_t2568926986::get_offset_of_linkPrefab_22(),
	WMG_Grid_t2568926986::get_offset_of_gridNumNodesX_23(),
	WMG_Grid_t2568926986::get_offset_of_gridNumNodesY_24(),
	WMG_Grid_t2568926986::get_offset_of_gridLinkLengthX_25(),
	WMG_Grid_t2568926986::get_offset_of_gridLinkLengthY_26(),
	WMG_Grid_t2568926986::get_offset_of_createLinks_27(),
	WMG_Grid_t2568926986::get_offset_of_noVerticalLinks_28(),
	WMG_Grid_t2568926986::get_offset_of_noHorizontalLinks_29(),
	WMG_Grid_t2568926986::get_offset_of_linkColor_30(),
	WMG_Grid_t2568926986::get_offset_of_linkWidth_31(),
	WMG_Grid_t2568926986::get_offset_of_gridNodesXY_32(),
	WMG_Grid_t2568926986::get_offset_of_gridLinks_33(),
	WMG_Grid_t2568926986::get_offset_of_cachedGridType_34(),
	WMG_Grid_t2568926986::get_offset_of_cachedNodePrefab_35(),
	WMG_Grid_t2568926986::get_offset_of_cachedLinkPrefab_36(),
	WMG_Grid_t2568926986::get_offset_of_cachedGridNumNodesX_37(),
	WMG_Grid_t2568926986::get_offset_of_cachedGridNumNodesY_38(),
	WMG_Grid_t2568926986::get_offset_of_cachedGridLinkLengthX_39(),
	WMG_Grid_t2568926986::get_offset_of_cachedGridLinkLengthY_40(),
	WMG_Grid_t2568926986::get_offset_of_cachedCreateLinks_41(),
	WMG_Grid_t2568926986::get_offset_of_cachedNoVerticalLinks_42(),
	WMG_Grid_t2568926986::get_offset_of_cachedNoHorizontalLinks_43(),
	WMG_Grid_t2568926986::get_offset_of_cachedLinkColor_44(),
	WMG_Grid_t2568926986::get_offset_of_cachedLinkWidth_45(),
	WMG_Grid_t2568926986::get_offset_of_gridChanged_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (gridTypes_t1177259200)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[4] = 
{
	gridTypes_t1177259200::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (WMG_Hierarchical_Tree_t2894465612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[35] = 
{
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_nodeParent_19(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_linkParent_20(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_defaultNodePrefab_21(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_linkPrefab_22(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_numNodes_23(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_numLinks_24(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_nodePrefabs_25(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_nodeColumns_26(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_nodeRows_27(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_linkNodeFromIDs_28(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_linkNodeToIDs_29(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_invisibleNodePrefab_30(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_numInvisibleNodes_31(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_invisibleNodeColumns_32(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_invisibleNodeRows_33(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__gridLengthX_34(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__gridLengthY_35(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__nodeWidthHeight_36(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__nodeRadius_37(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__squareNodes_38(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__resizeEnabled_39(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of__resizeProperties_40(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_cachedContainerWidth_41(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_cachedContainerHeight_42(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_origWidth_43(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_origHeight_44(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_origNodeWidthHeight_45(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_origNodeRadius_46(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_changeObjs_47(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_treeC_48(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_resizeC_49(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_treeNodes_50(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_treeLinks_51(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_treeInvisibleNodes_52(),
	WMG_Hierarchical_Tree_t2894465612::get_offset_of_hasInit_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ResizeProperties_t3067266120)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1994[3] = 
{
	ResizeProperties_t3067266120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (WMG_Pie_Graph_t252836487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[67] = 
{
	WMG_Pie_Graph_t252836487::get_offset_of__sliceValues_19(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceValues_20(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceLabels_21(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceLabels_22(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceColors_23(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceColors_24(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceValuesDataSource_25(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceLabelsDataSource_26(),
	WMG_Pie_Graph_t252836487::get_offset_of_sliceColorsDataSource_27(),
	WMG_Pie_Graph_t252836487::get_offset_of_background_28(),
	WMG_Pie_Graph_t252836487::get_offset_of_backgroundCircle_29(),
	WMG_Pie_Graph_t252836487::get_offset_of_slicesParent_30(),
	WMG_Pie_Graph_t252836487::get_offset_of_legend_31(),
	WMG_Pie_Graph_t252836487::get_offset_of_legendEntryPrefab_32(),
	WMG_Pie_Graph_t252836487::get_offset_of_nodePrefab_33(),
	WMG_Pie_Graph_t252836487::get_offset_of__resizeEnabled_34(),
	WMG_Pie_Graph_t252836487::get_offset_of__resizeProperties_35(),
	WMG_Pie_Graph_t252836487::get_offset_of__leftRightPadding_36(),
	WMG_Pie_Graph_t252836487::get_offset_of__topBotPadding_37(),
	WMG_Pie_Graph_t252836487::get_offset_of__bgCircleOffset_38(),
	WMG_Pie_Graph_t252836487::get_offset_of__autoCenter_39(),
	WMG_Pie_Graph_t252836487::get_offset_of__autoCenterMinPadding_40(),
	WMG_Pie_Graph_t252836487::get_offset_of__sortBy_41(),
	WMG_Pie_Graph_t252836487::get_offset_of__swapColorsDuringSort_42(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceLabelType_43(),
	WMG_Pie_Graph_t252836487::get_offset_of__explodeLength_44(),
	WMG_Pie_Graph_t252836487::get_offset_of__explodeSymmetrical_45(),
	WMG_Pie_Graph_t252836487::get_offset_of__useDoughnut_46(),
	WMG_Pie_Graph_t252836487::get_offset_of__doughnutPercentage_47(),
	WMG_Pie_Graph_t252836487::get_offset_of__limitNumberSlices_48(),
	WMG_Pie_Graph_t252836487::get_offset_of__includeOthers_49(),
	WMG_Pie_Graph_t252836487::get_offset_of__maxNumberSlices_50(),
	WMG_Pie_Graph_t252836487::get_offset_of__includeOthersLabel_51(),
	WMG_Pie_Graph_t252836487::get_offset_of__includeOthersColor_52(),
	WMG_Pie_Graph_t252836487::get_offset_of__animationDuration_53(),
	WMG_Pie_Graph_t252836487::get_offset_of__sortAnimationDuration_54(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceLabelExplodeLength_55(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceLabelFontSize_56(),
	WMG_Pie_Graph_t252836487::get_offset_of__numberDecimalsInPercents_57(),
	WMG_Pie_Graph_t252836487::get_offset_of__sliceLabelColor_58(),
	WMG_Pie_Graph_t252836487::get_offset_of__hideZeroValueLegendEntry_59(),
	WMG_Pie_Graph_t252836487::get_offset_of__interactivityEnabled_60(),
	WMG_Pie_Graph_t252836487::get_offset_of_LabelToSliceMap_61(),
	WMG_Pie_Graph_t252836487::get_offset_of_origPieSize_62(),
	WMG_Pie_Graph_t252836487::get_offset_of_origSliceLabelExplodeLength_63(),
	WMG_Pie_Graph_t252836487::get_offset_of_origSliceLabelFontSize_64(),
	WMG_Pie_Graph_t252836487::get_offset_of_origAutoCenterPadding_65(),
	WMG_Pie_Graph_t252836487::get_offset_of_cachedContainerWidth_66(),
	WMG_Pie_Graph_t252836487::get_offset_of_cachedContainerHeight_67(),
	WMG_Pie_Graph_t252836487::get_offset_of_slices_68(),
	WMG_Pie_Graph_t252836487::get_offset_of_numSlices_69(),
	WMG_Pie_Graph_t252836487::get_offset_of_isOtherSlice_70(),
	WMG_Pie_Graph_t252836487::get_offset_of_otherSliceValue_71(),
	WMG_Pie_Graph_t252836487::get_offset_of_totalVal_72(),
	WMG_Pie_Graph_t252836487::get_offset_of_animSortSwap_73(),
	WMG_Pie_Graph_t252836487::get_offset_of_isAnimating_74(),
	WMG_Pie_Graph_t252836487::get_offset_of_colors_75(),
	WMG_Pie_Graph_t252836487::get_offset_of_origColors_76(),
	WMG_Pie_Graph_t252836487::get_offset_of_pieSprite_77(),
	WMG_Pie_Graph_t252836487::get_offset_of_changeObjs_78(),
	WMG_Pie_Graph_t252836487::get_offset_of_graphC_79(),
	WMG_Pie_Graph_t252836487::get_offset_of_resizeC_80(),
	WMG_Pie_Graph_t252836487::get_offset_of_doughnutC_81(),
	WMG_Pie_Graph_t252836487::get_offset_of_interactivityC_82(),
	WMG_Pie_Graph_t252836487::get_offset_of_hasInit_83(),
	WMG_Pie_Graph_t252836487::get_offset_of_setOrig_84(),
	WMG_Pie_Graph_t252836487::get_offset_of_doughnutHasInit_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (sortMethod_t345387417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1996[6] = 
{
	sortMethod_t345387417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (ResizeProperties_t2442552467)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[8] = 
{
	ResizeProperties_t2442552467::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (U3CUpdateVisualsU3Ec__AnonStorey0_t894148875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[2] = 
{
	U3CUpdateVisualsU3Ec__AnonStorey0_t894148875::get_offset_of_newI_0(),
	U3CUpdateVisualsU3Ec__AnonStorey0_t894148875::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (U3CshrinkSlicesU3Ec__AnonStorey1_t4228999192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[2] = 
{
	U3CshrinkSlicesU3Ec__AnonStorey1_t4228999192::get_offset_of_sliceNum_0(),
	U3CshrinkSlicesU3Ec__AnonStorey1_t4228999192::get_offset_of_U24this_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
