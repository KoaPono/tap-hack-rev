﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyScore
struct  DailyScore_t1940687647 
{
public:
	// System.DateTime DailyScore::date
	DateTime_t693205669  ___date_0;
	// System.Int32[] DailyScore::scores
	Int32U5BU5D_t3030399641* ___scores_1;

public:
	inline static int32_t get_offset_of_date_0() { return static_cast<int32_t>(offsetof(DailyScore_t1940687647, ___date_0)); }
	inline DateTime_t693205669  get_date_0() const { return ___date_0; }
	inline DateTime_t693205669 * get_address_of_date_0() { return &___date_0; }
	inline void set_date_0(DateTime_t693205669  value)
	{
		___date_0 = value;
	}

	inline static int32_t get_offset_of_scores_1() { return static_cast<int32_t>(offsetof(DailyScore_t1940687647, ___scores_1)); }
	inline Int32U5BU5D_t3030399641* get_scores_1() const { return ___scores_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_scores_1() { return &___scores_1; }
	inline void set_scores_1(Int32U5BU5D_t3030399641* value)
	{
		___scores_1 = value;
		Il2CppCodeGenWriteBarrier(&___scores_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DailyScore
struct DailyScore_t1940687647_marshaled_pinvoke
{
	DateTime_t693205669  ___date_0;
	int32_t* ___scores_1;
};
// Native definition for COM marshalling of DailyScore
struct DailyScore_t1940687647_marshaled_com
{
	DateTime_t693205669  ___date_0;
	int32_t* ___scores_1;
};
