﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationSounds
struct  AnimationSounds_t3025961406  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip AnimationSounds::swish
	AudioClip_t1932558630 * ___swish_2;
	// UnityEngine.AudioClip AnimationSounds::coin
	AudioClip_t1932558630 * ___coin_3;
	// UnityEngine.AudioSource AnimationSounds::source
	AudioSource_t1135106623 * ___source_4;
	// System.Single AnimationSounds::volumeUp
	float ___volumeUp_5;

public:
	inline static int32_t get_offset_of_swish_2() { return static_cast<int32_t>(offsetof(AnimationSounds_t3025961406, ___swish_2)); }
	inline AudioClip_t1932558630 * get_swish_2() const { return ___swish_2; }
	inline AudioClip_t1932558630 ** get_address_of_swish_2() { return &___swish_2; }
	inline void set_swish_2(AudioClip_t1932558630 * value)
	{
		___swish_2 = value;
		Il2CppCodeGenWriteBarrier(&___swish_2, value);
	}

	inline static int32_t get_offset_of_coin_3() { return static_cast<int32_t>(offsetof(AnimationSounds_t3025961406, ___coin_3)); }
	inline AudioClip_t1932558630 * get_coin_3() const { return ___coin_3; }
	inline AudioClip_t1932558630 ** get_address_of_coin_3() { return &___coin_3; }
	inline void set_coin_3(AudioClip_t1932558630 * value)
	{
		___coin_3 = value;
		Il2CppCodeGenWriteBarrier(&___coin_3, value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(AnimationSounds_t3025961406, ___source_4)); }
	inline AudioSource_t1135106623 * get_source_4() const { return ___source_4; }
	inline AudioSource_t1135106623 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(AudioSource_t1135106623 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier(&___source_4, value);
	}

	inline static int32_t get_offset_of_volumeUp_5() { return static_cast<int32_t>(offsetof(AnimationSounds_t3025961406, ___volumeUp_5)); }
	inline float get_volumeUp_5() const { return ___volumeUp_5; }
	inline float* get_address_of_volumeUp_5() { return &___volumeUp_5; }
	inline void set_volumeUp_5(float value)
	{
		___volumeUp_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
