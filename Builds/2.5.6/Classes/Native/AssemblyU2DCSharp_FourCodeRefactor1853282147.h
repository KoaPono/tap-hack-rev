﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// MenuRefactor
struct MenuRefactor_t148745019;
// ScoreTracker
struct ScoreTracker_t1973445098;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourCodeRefactor
struct  FourCodeRefactor_t1853282147  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] FourCodeRefactor::positionBorders
	GameObjectU5BU5D_t3057952154* ___positionBorders_2;
	// UnityEngine.Sprite[] FourCodeRefactor::correctSprites
	SpriteU5BU5D_t3359083662* ___correctSprites_3;
	// UnityEngine.Sprite[] FourCodeRefactor::incorrectSprites
	SpriteU5BU5D_t3359083662* ___incorrectSprites_4;
	// UnityEngine.Sprite FourCodeRefactor::emptySprite
	Sprite_t309593783 * ___emptySprite_5;
	// UnityEngine.UI.Image[] FourCodeRefactor::positionSprites
	ImageU5BU5D_t590162004* ___positionSprites_6;
	// UnityEngine.AudioSource FourCodeRefactor::colorButtonAudioSource
	AudioSource_t1135106623 * ___colorButtonAudioSource_7;
	// UnityEngine.AudioSource FourCodeRefactor::gameOverAudioSource
	AudioSource_t1135106623 * ___gameOverAudioSource_8;
	// UnityEngine.AudioSource FourCodeRefactor::completedCodeAudioCource
	AudioSource_t1135106623 * ___completedCodeAudioCource_9;
	// UnityEngine.AudioClip FourCodeRefactor::correctSound
	AudioClip_t1932558630 * ___correctSound_10;
	// UnityEngine.AudioClip FourCodeRefactor::incorrectSound
	AudioClip_t1932558630 * ___incorrectSound_11;
	// UnityEngine.AudioClip FourCodeRefactor::highscoreSound
	AudioClip_t1932558630 * ___highscoreSound_12;
	// UnityEngine.AudioClip FourCodeRefactor::endGameSound
	AudioClip_t1932558630 * ___endGameSound_13;
	// UnityEngine.AudioClip FourCodeRefactor::codeCorrectSound
	AudioClip_t1932558630 * ___codeCorrectSound_14;
	// UnityEngine.UI.Text FourCodeRefactor::timerText
	Text_t356221433 * ___timerText_15;
	// UnityEngine.UI.Text FourCodeRefactor::scoreText
	Text_t356221433 * ___scoreText_16;
	// UnityEngine.UI.Text FourCodeRefactor::finalScore
	Text_t356221433 * ___finalScore_17;
	// UnityEngine.UI.Text FourCodeRefactor::highScoreText
	Text_t356221433 * ___highScoreText_18;
	// UnityEngine.GameObject FourCodeRefactor::confetti1
	GameObject_t1756533147 * ___confetti1_19;
	// UnityEngine.GameObject FourCodeRefactor::confetti2
	GameObject_t1756533147 * ___confetti2_20;
	// UnityEngine.UI.Button FourCodeRefactor::restartButton
	Button_t2872111280 * ___restartButton_21;
	// UnityEngine.UI.Button FourCodeRefactor::menuButton
	Button_t2872111280 * ___menuButton_22;
	// System.Single FourCodeRefactor::startingTime
	float ___startingTime_23;
	// System.Single FourCodeRefactor::timeRemaining
	float ___timeRemaining_24;
	// System.Single FourCodeRefactor::addedTime
	float ___addedTime_25;
	// System.Int32 FourCodeRefactor::numberOfColors
	int32_t ___numberOfColors_26;
	// System.Byte FourCodeRefactor::colorsSet
	uint8_t ___colorsSet_27;
	// System.Byte FourCodeRefactor::positionsSet
	uint8_t ___positionsSet_28;
	// System.Collections.Generic.List`1<System.Int32> FourCodeRefactor::positionColor
	List_1_t1440998580 * ___positionColor_29;
	// System.Collections.Generic.List`1<System.Int32> FourCodeRefactor::codeColorID
	List_1_t1440998580 * ___codeColorID_30;
	// System.Int32 FourCodeRefactor::playerScore
	int32_t ___playerScore_31;
	// System.Int32 FourCodeRefactor::highscore
	int32_t ___highscore_32;
	// System.Boolean FourCodeRefactor::gameIsPlaying
	bool ___gameIsPlaying_33;
	// MenuRefactor FourCodeRefactor::menuController
	MenuRefactor_t148745019 * ___menuController_34;
	// ScoreTracker FourCodeRefactor::scoreTracker
	ScoreTracker_t1973445098 * ___scoreTracker_35;

public:
	inline static int32_t get_offset_of_positionBorders_2() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___positionBorders_2)); }
	inline GameObjectU5BU5D_t3057952154* get_positionBorders_2() const { return ___positionBorders_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_positionBorders_2() { return &___positionBorders_2; }
	inline void set_positionBorders_2(GameObjectU5BU5D_t3057952154* value)
	{
		___positionBorders_2 = value;
		Il2CppCodeGenWriteBarrier(&___positionBorders_2, value);
	}

	inline static int32_t get_offset_of_correctSprites_3() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___correctSprites_3)); }
	inline SpriteU5BU5D_t3359083662* get_correctSprites_3() const { return ___correctSprites_3; }
	inline SpriteU5BU5D_t3359083662** get_address_of_correctSprites_3() { return &___correctSprites_3; }
	inline void set_correctSprites_3(SpriteU5BU5D_t3359083662* value)
	{
		___correctSprites_3 = value;
		Il2CppCodeGenWriteBarrier(&___correctSprites_3, value);
	}

	inline static int32_t get_offset_of_incorrectSprites_4() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___incorrectSprites_4)); }
	inline SpriteU5BU5D_t3359083662* get_incorrectSprites_4() const { return ___incorrectSprites_4; }
	inline SpriteU5BU5D_t3359083662** get_address_of_incorrectSprites_4() { return &___incorrectSprites_4; }
	inline void set_incorrectSprites_4(SpriteU5BU5D_t3359083662* value)
	{
		___incorrectSprites_4 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectSprites_4, value);
	}

	inline static int32_t get_offset_of_emptySprite_5() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___emptySprite_5)); }
	inline Sprite_t309593783 * get_emptySprite_5() const { return ___emptySprite_5; }
	inline Sprite_t309593783 ** get_address_of_emptySprite_5() { return &___emptySprite_5; }
	inline void set_emptySprite_5(Sprite_t309593783 * value)
	{
		___emptySprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___emptySprite_5, value);
	}

	inline static int32_t get_offset_of_positionSprites_6() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___positionSprites_6)); }
	inline ImageU5BU5D_t590162004* get_positionSprites_6() const { return ___positionSprites_6; }
	inline ImageU5BU5D_t590162004** get_address_of_positionSprites_6() { return &___positionSprites_6; }
	inline void set_positionSprites_6(ImageU5BU5D_t590162004* value)
	{
		___positionSprites_6 = value;
		Il2CppCodeGenWriteBarrier(&___positionSprites_6, value);
	}

	inline static int32_t get_offset_of_colorButtonAudioSource_7() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___colorButtonAudioSource_7)); }
	inline AudioSource_t1135106623 * get_colorButtonAudioSource_7() const { return ___colorButtonAudioSource_7; }
	inline AudioSource_t1135106623 ** get_address_of_colorButtonAudioSource_7() { return &___colorButtonAudioSource_7; }
	inline void set_colorButtonAudioSource_7(AudioSource_t1135106623 * value)
	{
		___colorButtonAudioSource_7 = value;
		Il2CppCodeGenWriteBarrier(&___colorButtonAudioSource_7, value);
	}

	inline static int32_t get_offset_of_gameOverAudioSource_8() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___gameOverAudioSource_8)); }
	inline AudioSource_t1135106623 * get_gameOverAudioSource_8() const { return ___gameOverAudioSource_8; }
	inline AudioSource_t1135106623 ** get_address_of_gameOverAudioSource_8() { return &___gameOverAudioSource_8; }
	inline void set_gameOverAudioSource_8(AudioSource_t1135106623 * value)
	{
		___gameOverAudioSource_8 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverAudioSource_8, value);
	}

	inline static int32_t get_offset_of_completedCodeAudioCource_9() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___completedCodeAudioCource_9)); }
	inline AudioSource_t1135106623 * get_completedCodeAudioCource_9() const { return ___completedCodeAudioCource_9; }
	inline AudioSource_t1135106623 ** get_address_of_completedCodeAudioCource_9() { return &___completedCodeAudioCource_9; }
	inline void set_completedCodeAudioCource_9(AudioSource_t1135106623 * value)
	{
		___completedCodeAudioCource_9 = value;
		Il2CppCodeGenWriteBarrier(&___completedCodeAudioCource_9, value);
	}

	inline static int32_t get_offset_of_correctSound_10() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___correctSound_10)); }
	inline AudioClip_t1932558630 * get_correctSound_10() const { return ___correctSound_10; }
	inline AudioClip_t1932558630 ** get_address_of_correctSound_10() { return &___correctSound_10; }
	inline void set_correctSound_10(AudioClip_t1932558630 * value)
	{
		___correctSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___correctSound_10, value);
	}

	inline static int32_t get_offset_of_incorrectSound_11() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___incorrectSound_11)); }
	inline AudioClip_t1932558630 * get_incorrectSound_11() const { return ___incorrectSound_11; }
	inline AudioClip_t1932558630 ** get_address_of_incorrectSound_11() { return &___incorrectSound_11; }
	inline void set_incorrectSound_11(AudioClip_t1932558630 * value)
	{
		___incorrectSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectSound_11, value);
	}

	inline static int32_t get_offset_of_highscoreSound_12() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___highscoreSound_12)); }
	inline AudioClip_t1932558630 * get_highscoreSound_12() const { return ___highscoreSound_12; }
	inline AudioClip_t1932558630 ** get_address_of_highscoreSound_12() { return &___highscoreSound_12; }
	inline void set_highscoreSound_12(AudioClip_t1932558630 * value)
	{
		___highscoreSound_12 = value;
		Il2CppCodeGenWriteBarrier(&___highscoreSound_12, value);
	}

	inline static int32_t get_offset_of_endGameSound_13() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___endGameSound_13)); }
	inline AudioClip_t1932558630 * get_endGameSound_13() const { return ___endGameSound_13; }
	inline AudioClip_t1932558630 ** get_address_of_endGameSound_13() { return &___endGameSound_13; }
	inline void set_endGameSound_13(AudioClip_t1932558630 * value)
	{
		___endGameSound_13 = value;
		Il2CppCodeGenWriteBarrier(&___endGameSound_13, value);
	}

	inline static int32_t get_offset_of_codeCorrectSound_14() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___codeCorrectSound_14)); }
	inline AudioClip_t1932558630 * get_codeCorrectSound_14() const { return ___codeCorrectSound_14; }
	inline AudioClip_t1932558630 ** get_address_of_codeCorrectSound_14() { return &___codeCorrectSound_14; }
	inline void set_codeCorrectSound_14(AudioClip_t1932558630 * value)
	{
		___codeCorrectSound_14 = value;
		Il2CppCodeGenWriteBarrier(&___codeCorrectSound_14, value);
	}

	inline static int32_t get_offset_of_timerText_15() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___timerText_15)); }
	inline Text_t356221433 * get_timerText_15() const { return ___timerText_15; }
	inline Text_t356221433 ** get_address_of_timerText_15() { return &___timerText_15; }
	inline void set_timerText_15(Text_t356221433 * value)
	{
		___timerText_15 = value;
		Il2CppCodeGenWriteBarrier(&___timerText_15, value);
	}

	inline static int32_t get_offset_of_scoreText_16() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___scoreText_16)); }
	inline Text_t356221433 * get_scoreText_16() const { return ___scoreText_16; }
	inline Text_t356221433 ** get_address_of_scoreText_16() { return &___scoreText_16; }
	inline void set_scoreText_16(Text_t356221433 * value)
	{
		___scoreText_16 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_16, value);
	}

	inline static int32_t get_offset_of_finalScore_17() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___finalScore_17)); }
	inline Text_t356221433 * get_finalScore_17() const { return ___finalScore_17; }
	inline Text_t356221433 ** get_address_of_finalScore_17() { return &___finalScore_17; }
	inline void set_finalScore_17(Text_t356221433 * value)
	{
		___finalScore_17 = value;
		Il2CppCodeGenWriteBarrier(&___finalScore_17, value);
	}

	inline static int32_t get_offset_of_highScoreText_18() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___highScoreText_18)); }
	inline Text_t356221433 * get_highScoreText_18() const { return ___highScoreText_18; }
	inline Text_t356221433 ** get_address_of_highScoreText_18() { return &___highScoreText_18; }
	inline void set_highScoreText_18(Text_t356221433 * value)
	{
		___highScoreText_18 = value;
		Il2CppCodeGenWriteBarrier(&___highScoreText_18, value);
	}

	inline static int32_t get_offset_of_confetti1_19() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___confetti1_19)); }
	inline GameObject_t1756533147 * get_confetti1_19() const { return ___confetti1_19; }
	inline GameObject_t1756533147 ** get_address_of_confetti1_19() { return &___confetti1_19; }
	inline void set_confetti1_19(GameObject_t1756533147 * value)
	{
		___confetti1_19 = value;
		Il2CppCodeGenWriteBarrier(&___confetti1_19, value);
	}

	inline static int32_t get_offset_of_confetti2_20() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___confetti2_20)); }
	inline GameObject_t1756533147 * get_confetti2_20() const { return ___confetti2_20; }
	inline GameObject_t1756533147 ** get_address_of_confetti2_20() { return &___confetti2_20; }
	inline void set_confetti2_20(GameObject_t1756533147 * value)
	{
		___confetti2_20 = value;
		Il2CppCodeGenWriteBarrier(&___confetti2_20, value);
	}

	inline static int32_t get_offset_of_restartButton_21() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___restartButton_21)); }
	inline Button_t2872111280 * get_restartButton_21() const { return ___restartButton_21; }
	inline Button_t2872111280 ** get_address_of_restartButton_21() { return &___restartButton_21; }
	inline void set_restartButton_21(Button_t2872111280 * value)
	{
		___restartButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___restartButton_21, value);
	}

	inline static int32_t get_offset_of_menuButton_22() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___menuButton_22)); }
	inline Button_t2872111280 * get_menuButton_22() const { return ___menuButton_22; }
	inline Button_t2872111280 ** get_address_of_menuButton_22() { return &___menuButton_22; }
	inline void set_menuButton_22(Button_t2872111280 * value)
	{
		___menuButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___menuButton_22, value);
	}

	inline static int32_t get_offset_of_startingTime_23() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___startingTime_23)); }
	inline float get_startingTime_23() const { return ___startingTime_23; }
	inline float* get_address_of_startingTime_23() { return &___startingTime_23; }
	inline void set_startingTime_23(float value)
	{
		___startingTime_23 = value;
	}

	inline static int32_t get_offset_of_timeRemaining_24() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___timeRemaining_24)); }
	inline float get_timeRemaining_24() const { return ___timeRemaining_24; }
	inline float* get_address_of_timeRemaining_24() { return &___timeRemaining_24; }
	inline void set_timeRemaining_24(float value)
	{
		___timeRemaining_24 = value;
	}

	inline static int32_t get_offset_of_addedTime_25() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___addedTime_25)); }
	inline float get_addedTime_25() const { return ___addedTime_25; }
	inline float* get_address_of_addedTime_25() { return &___addedTime_25; }
	inline void set_addedTime_25(float value)
	{
		___addedTime_25 = value;
	}

	inline static int32_t get_offset_of_numberOfColors_26() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___numberOfColors_26)); }
	inline int32_t get_numberOfColors_26() const { return ___numberOfColors_26; }
	inline int32_t* get_address_of_numberOfColors_26() { return &___numberOfColors_26; }
	inline void set_numberOfColors_26(int32_t value)
	{
		___numberOfColors_26 = value;
	}

	inline static int32_t get_offset_of_colorsSet_27() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___colorsSet_27)); }
	inline uint8_t get_colorsSet_27() const { return ___colorsSet_27; }
	inline uint8_t* get_address_of_colorsSet_27() { return &___colorsSet_27; }
	inline void set_colorsSet_27(uint8_t value)
	{
		___colorsSet_27 = value;
	}

	inline static int32_t get_offset_of_positionsSet_28() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___positionsSet_28)); }
	inline uint8_t get_positionsSet_28() const { return ___positionsSet_28; }
	inline uint8_t* get_address_of_positionsSet_28() { return &___positionsSet_28; }
	inline void set_positionsSet_28(uint8_t value)
	{
		___positionsSet_28 = value;
	}

	inline static int32_t get_offset_of_positionColor_29() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___positionColor_29)); }
	inline List_1_t1440998580 * get_positionColor_29() const { return ___positionColor_29; }
	inline List_1_t1440998580 ** get_address_of_positionColor_29() { return &___positionColor_29; }
	inline void set_positionColor_29(List_1_t1440998580 * value)
	{
		___positionColor_29 = value;
		Il2CppCodeGenWriteBarrier(&___positionColor_29, value);
	}

	inline static int32_t get_offset_of_codeColorID_30() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___codeColorID_30)); }
	inline List_1_t1440998580 * get_codeColorID_30() const { return ___codeColorID_30; }
	inline List_1_t1440998580 ** get_address_of_codeColorID_30() { return &___codeColorID_30; }
	inline void set_codeColorID_30(List_1_t1440998580 * value)
	{
		___codeColorID_30 = value;
		Il2CppCodeGenWriteBarrier(&___codeColorID_30, value);
	}

	inline static int32_t get_offset_of_playerScore_31() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___playerScore_31)); }
	inline int32_t get_playerScore_31() const { return ___playerScore_31; }
	inline int32_t* get_address_of_playerScore_31() { return &___playerScore_31; }
	inline void set_playerScore_31(int32_t value)
	{
		___playerScore_31 = value;
	}

	inline static int32_t get_offset_of_highscore_32() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___highscore_32)); }
	inline int32_t get_highscore_32() const { return ___highscore_32; }
	inline int32_t* get_address_of_highscore_32() { return &___highscore_32; }
	inline void set_highscore_32(int32_t value)
	{
		___highscore_32 = value;
	}

	inline static int32_t get_offset_of_gameIsPlaying_33() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___gameIsPlaying_33)); }
	inline bool get_gameIsPlaying_33() const { return ___gameIsPlaying_33; }
	inline bool* get_address_of_gameIsPlaying_33() { return &___gameIsPlaying_33; }
	inline void set_gameIsPlaying_33(bool value)
	{
		___gameIsPlaying_33 = value;
	}

	inline static int32_t get_offset_of_menuController_34() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___menuController_34)); }
	inline MenuRefactor_t148745019 * get_menuController_34() const { return ___menuController_34; }
	inline MenuRefactor_t148745019 ** get_address_of_menuController_34() { return &___menuController_34; }
	inline void set_menuController_34(MenuRefactor_t148745019 * value)
	{
		___menuController_34 = value;
		Il2CppCodeGenWriteBarrier(&___menuController_34, value);
	}

	inline static int32_t get_offset_of_scoreTracker_35() { return static_cast<int32_t>(offsetof(FourCodeRefactor_t1853282147, ___scoreTracker_35)); }
	inline ScoreTracker_t1973445098 * get_scoreTracker_35() const { return ___scoreTracker_35; }
	inline ScoreTracker_t1973445098 ** get_address_of_scoreTracker_35() { return &___scoreTracker_35; }
	inline void set_scoreTracker_35(ScoreTracker_t1973445098 * value)
	{
		___scoreTracker_35 = value;
		Il2CppCodeGenWriteBarrier(&___scoreTracker_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
