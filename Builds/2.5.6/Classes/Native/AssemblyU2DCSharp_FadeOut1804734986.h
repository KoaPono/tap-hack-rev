﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeOut
struct  FadeOut_t1804734986  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image FadeOut::background
	Image_t2042527209 * ___background_2;
	// UnityEngine.UI.Image FadeOut::logo
	Image_t2042527209 * ___logo_3;
	// UnityEngine.Color FadeOut::fade
	Color_t2020392075  ___fade_4;
	// System.Boolean FadeOut::isFading
	bool ___isFading_5;

public:
	inline static int32_t get_offset_of_background_2() { return static_cast<int32_t>(offsetof(FadeOut_t1804734986, ___background_2)); }
	inline Image_t2042527209 * get_background_2() const { return ___background_2; }
	inline Image_t2042527209 ** get_address_of_background_2() { return &___background_2; }
	inline void set_background_2(Image_t2042527209 * value)
	{
		___background_2 = value;
		Il2CppCodeGenWriteBarrier(&___background_2, value);
	}

	inline static int32_t get_offset_of_logo_3() { return static_cast<int32_t>(offsetof(FadeOut_t1804734986, ___logo_3)); }
	inline Image_t2042527209 * get_logo_3() const { return ___logo_3; }
	inline Image_t2042527209 ** get_address_of_logo_3() { return &___logo_3; }
	inline void set_logo_3(Image_t2042527209 * value)
	{
		___logo_3 = value;
		Il2CppCodeGenWriteBarrier(&___logo_3, value);
	}

	inline static int32_t get_offset_of_fade_4() { return static_cast<int32_t>(offsetof(FadeOut_t1804734986, ___fade_4)); }
	inline Color_t2020392075  get_fade_4() const { return ___fade_4; }
	inline Color_t2020392075 * get_address_of_fade_4() { return &___fade_4; }
	inline void set_fade_4(Color_t2020392075  value)
	{
		___fade_4 = value;
	}

	inline static int32_t get_offset_of_isFading_5() { return static_cast<int32_t>(offsetof(FadeOut_t1804734986, ___isFading_5)); }
	inline bool get_isFading_5() const { return ___isFading_5; }
	inline bool* get_address_of_isFading_5() { return &___isFading_5; }
	inline void set_isFading_5(bool value)
	{
		___isFading_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
