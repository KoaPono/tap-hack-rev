﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_DateTime693205669.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WeeklyScore
struct  WeeklyScore_t1892813563 
{
public:
	// System.Int32 WeeklyScore::highScore
	int32_t ___highScore_0;
	// System.Int32 WeeklyScore::averageScore
	int32_t ___averageScore_1;
	// System.Int32 WeeklyScore::lowScore
	int32_t ___lowScore_2;
	// System.DateTime WeeklyScore::date
	DateTime_t693205669  ___date_3;

public:
	inline static int32_t get_offset_of_highScore_0() { return static_cast<int32_t>(offsetof(WeeklyScore_t1892813563, ___highScore_0)); }
	inline int32_t get_highScore_0() const { return ___highScore_0; }
	inline int32_t* get_address_of_highScore_0() { return &___highScore_0; }
	inline void set_highScore_0(int32_t value)
	{
		___highScore_0 = value;
	}

	inline static int32_t get_offset_of_averageScore_1() { return static_cast<int32_t>(offsetof(WeeklyScore_t1892813563, ___averageScore_1)); }
	inline int32_t get_averageScore_1() const { return ___averageScore_1; }
	inline int32_t* get_address_of_averageScore_1() { return &___averageScore_1; }
	inline void set_averageScore_1(int32_t value)
	{
		___averageScore_1 = value;
	}

	inline static int32_t get_offset_of_lowScore_2() { return static_cast<int32_t>(offsetof(WeeklyScore_t1892813563, ___lowScore_2)); }
	inline int32_t get_lowScore_2() const { return ___lowScore_2; }
	inline int32_t* get_address_of_lowScore_2() { return &___lowScore_2; }
	inline void set_lowScore_2(int32_t value)
	{
		___lowScore_2 = value;
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(WeeklyScore_t1892813563, ___date_3)); }
	inline DateTime_t693205669  get_date_3() const { return ___date_3; }
	inline DateTime_t693205669 * get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DateTime_t693205669  value)
	{
		___date_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of WeeklyScore
struct WeeklyScore_t1892813563_marshaled_pinvoke
{
	int32_t ___highScore_0;
	int32_t ___averageScore_1;
	int32_t ___lowScore_2;
	DateTime_t693205669  ___date_3;
};
// Native definition for COM marshalling of WeeklyScore
struct WeeklyScore_t1892813563_marshaled_com
{
	int32_t ___highScore_0;
	int32_t ___averageScore_1;
	int32_t ___lowScore_2;
	DateTime_t693205669  ___date_3;
};
