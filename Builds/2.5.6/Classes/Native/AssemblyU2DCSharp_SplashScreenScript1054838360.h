﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashScreenScript
struct  SplashScreenScript_t1054838360  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SplashScreenScript::sharkAnimation
	GameObject_t1756533147 * ___sharkAnimation_2;
	// System.Boolean SplashScreenScript::splashStarted
	bool ___splashStarted_3;
	// System.Boolean SplashScreenScript::isFading
	bool ___isFading_4;
	// UnityEngine.Color SplashScreenScript::fade
	Color_t2020392075  ___fade_5;

public:
	inline static int32_t get_offset_of_sharkAnimation_2() { return static_cast<int32_t>(offsetof(SplashScreenScript_t1054838360, ___sharkAnimation_2)); }
	inline GameObject_t1756533147 * get_sharkAnimation_2() const { return ___sharkAnimation_2; }
	inline GameObject_t1756533147 ** get_address_of_sharkAnimation_2() { return &___sharkAnimation_2; }
	inline void set_sharkAnimation_2(GameObject_t1756533147 * value)
	{
		___sharkAnimation_2 = value;
		Il2CppCodeGenWriteBarrier(&___sharkAnimation_2, value);
	}

	inline static int32_t get_offset_of_splashStarted_3() { return static_cast<int32_t>(offsetof(SplashScreenScript_t1054838360, ___splashStarted_3)); }
	inline bool get_splashStarted_3() const { return ___splashStarted_3; }
	inline bool* get_address_of_splashStarted_3() { return &___splashStarted_3; }
	inline void set_splashStarted_3(bool value)
	{
		___splashStarted_3 = value;
	}

	inline static int32_t get_offset_of_isFading_4() { return static_cast<int32_t>(offsetof(SplashScreenScript_t1054838360, ___isFading_4)); }
	inline bool get_isFading_4() const { return ___isFading_4; }
	inline bool* get_address_of_isFading_4() { return &___isFading_4; }
	inline void set_isFading_4(bool value)
	{
		___isFading_4 = value;
	}

	inline static int32_t get_offset_of_fade_5() { return static_cast<int32_t>(offsetof(SplashScreenScript_t1054838360, ___fade_5)); }
	inline Color_t2020392075  get_fade_5() const { return ___fade_5; }
	inline Color_t2020392075 * get_address_of_fade_5() { return &___fade_5; }
	inline void set_fade_5(Color_t2020392075  value)
	{
		___fade_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
