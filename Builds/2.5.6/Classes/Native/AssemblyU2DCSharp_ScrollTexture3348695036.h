﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollTexture
struct  ScrollTexture_t3348695036  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ScrollTexture::xSpeed
	float ___xSpeed_2;
	// System.Single ScrollTexture::ySpeed
	float ___ySpeed_3;
	// UnityEngine.Vector2 ScrollTexture::uvOffset
	Vector2_t2243707579  ___uvOffset_4;
	// UnityEngine.MeshRenderer ScrollTexture::rend
	MeshRenderer_t1268241104 * ___rend_5;
	// UnityEngine.Material ScrollTexture::mat
	Material_t193706927 * ___mat_6;

public:
	inline static int32_t get_offset_of_xSpeed_2() { return static_cast<int32_t>(offsetof(ScrollTexture_t3348695036, ___xSpeed_2)); }
	inline float get_xSpeed_2() const { return ___xSpeed_2; }
	inline float* get_address_of_xSpeed_2() { return &___xSpeed_2; }
	inline void set_xSpeed_2(float value)
	{
		___xSpeed_2 = value;
	}

	inline static int32_t get_offset_of_ySpeed_3() { return static_cast<int32_t>(offsetof(ScrollTexture_t3348695036, ___ySpeed_3)); }
	inline float get_ySpeed_3() const { return ___ySpeed_3; }
	inline float* get_address_of_ySpeed_3() { return &___ySpeed_3; }
	inline void set_ySpeed_3(float value)
	{
		___ySpeed_3 = value;
	}

	inline static int32_t get_offset_of_uvOffset_4() { return static_cast<int32_t>(offsetof(ScrollTexture_t3348695036, ___uvOffset_4)); }
	inline Vector2_t2243707579  get_uvOffset_4() const { return ___uvOffset_4; }
	inline Vector2_t2243707579 * get_address_of_uvOffset_4() { return &___uvOffset_4; }
	inline void set_uvOffset_4(Vector2_t2243707579  value)
	{
		___uvOffset_4 = value;
	}

	inline static int32_t get_offset_of_rend_5() { return static_cast<int32_t>(offsetof(ScrollTexture_t3348695036, ___rend_5)); }
	inline MeshRenderer_t1268241104 * get_rend_5() const { return ___rend_5; }
	inline MeshRenderer_t1268241104 ** get_address_of_rend_5() { return &___rend_5; }
	inline void set_rend_5(MeshRenderer_t1268241104 * value)
	{
		___rend_5 = value;
		Il2CppCodeGenWriteBarrier(&___rend_5, value);
	}

	inline static int32_t get_offset_of_mat_6() { return static_cast<int32_t>(offsetof(ScrollTexture_t3348695036, ___mat_6)); }
	inline Material_t193706927 * get_mat_6() const { return ___mat_6; }
	inline Material_t193706927 ** get_address_of_mat_6() { return &___mat_6; }
	inline void set_mat_6(Material_t193706927 * value)
	{
		___mat_6 = value;
		Il2CppCodeGenWriteBarrier(&___mat_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
