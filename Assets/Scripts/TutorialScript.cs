﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

    [Header("Position Game Objects")]
    public GameObject[] positionBorders;

    [Header("Text")]
    public Text score;

    [Header ("Sprites")]
    public Sprite[] correctSprites;
    public Sprite[] incorrectSprites;
    [Space()]
    public Sprite   emptySprite;

    [Space()]
    public Image[] positionSprites;

    [Header ("Tutorial Messages")]
    public GameObject[] tutorialMessages;

    private int tutorialStep = 0;
    private byte colorsSet;
    private byte positionsSet;

    private FourCodeRefactor mainGameScript;
    private MenuRefactor menuController;

    private void Start () {
        mainGameScript = GetComponent<FourCodeRefactor> ();
        menuController = GetComponent<MenuRefactor> ();
    }

    public void StartTutorial () {
        tutorialStep = 0;
        tutorialMessages [ 0 ].SetActive(true);

        positionsSet = 0;
        colorsSet = 0;
		score.text = "0";

        positionSprites [ 0 ].sprite = emptySprite;
        positionSprites [ 1 ].sprite = emptySprite;
        positionSprites [ 2 ].sprite = emptySprite;
    }

    // purple = 0, yellow = 1, blue = 2, reset = 3
    public void PressColor (int colorID) {
        switch (tutorialStep) {
            case 0:
                // Place incorrect button
                switch (colorID) {
                    case 0:
                        positionSprites [ 0 ].sprite = incorrectSprites [ 0 ];
                        mainGameScript.colorButtonAudioSource.clip = mainGameScript.incorrectSound;
                        mainGameScript.colorButtonAudioSource.Play ();
                        tutorialStep = 1;
                        tutorialMessages [ 0 ].SetActive (false);
                        tutorialMessages [ 1 ].SetActive (true);
                        break;
                    case 1:
                        positionSprites [ 0 ].sprite = incorrectSprites [ 1 ];
                        mainGameScript.colorButtonAudioSource.clip = mainGameScript.incorrectSound;
                        mainGameScript.colorButtonAudioSource.Play ();
                        tutorialStep = 1;
                        tutorialMessages [ 0 ].SetActive (false);
                        tutorialMessages [ 1 ].SetActive (true);
                        break;
                    case 2:
                        positionSprites [ 0 ].sprite = incorrectSprites [ 2 ];
                        mainGameScript.colorButtonAudioSource.clip = mainGameScript.incorrectSound;
                        mainGameScript.colorButtonAudioSource.Play ();
                        tutorialStep = 1;
                        tutorialMessages [ 0 ].SetActive (false);
                        tutorialMessages [ 1 ].SetActive (true);
                        break;
                    case 3:
                        // Do Nothing
                        break;
                }
                break;
            case 1:
                // Reset Colors
                switch ( colorID ) {
                    case 0:
                        // Do Nothing
                        break;
                    case 1:
                        // Do Nothing
                        break;
                    case 2:
                        // Do Nothing
                        break;
                    case 3:
                        positionSprites [ 0 ].sprite = emptySprite;
                        tutorialStep = 2;
                        tutorialMessages [ 1 ].SetActive (false);
                        break;
                }
                break;
            case 2:
                if (colorID < 3) {
                    tutorialMessages [ 2 ].SetActive (true);
                    if ( !GetBit (colorsSet, colorID) ) {
                        // Loop through each position until an empty position is found
                        for ( int i = 0; i < 3; i++ ) {
                            //Check if position is empty
                            if ( !GetBit (positionsSet, i) ) {

                                // Set position to "filled"
                                SetBit (ref positionsSet, i, true);

                                // Sets color to placed
                                SetBit (ref colorsSet, colorID, true);

                                positionSprites [ i ].sprite = correctSprites [ colorID ];

                                // Checks if final position is set
                                if ( GetBit (positionsSet, 3 - 1) ) {
                                    tutorialStep = 3;
                                    score.text = "1";
                                    mainGameScript.completedCodeAudioCource.Play ();
                                    tutorialMessages [ 2 ].SetActive (false);
                                    tutorialMessages [ 3 ].SetActive (true);
                                } else {
                                    mainGameScript.colorButtonAudioSource.clip = mainGameScript.correctSound;
                                    mainGameScript.colorButtonAudioSource.Play ();
                                }

                                // Once the position is filled break out of loop
                                return;
                            }
                        }
                    }
                }
                break;
            case 3:
                tutorialMessages [ 3 ].SetActive (false);
                tutorialMessages [ 4 ].SetActive (true);
                tutorialStep = 4;
                break;
            case 4:
                tutorialMessages [ 4 ].SetActive (false);
                tutorialStep = 0;
                menuController.Menu ();
                break;
        }
    }

    // Gets a bit at a specific location in a byte
    bool GetBit (byte b, int pos) {
        return ( b & ( 1 << pos ) ) != 0;
    }

    // Sets a bit at a specific position in a byte
    void SetBit (ref byte b, int pos, bool value) {
        if ( value ) {
            //left-shift 1, then bitwise OR
            b = (byte) ( b | ( 1 << pos ) );
        } else {
            //left-shift 1, then take complement, then bitwise AND
            b = (byte) ( b & ~( 1 << pos ) );
        }
    }

}
