﻿using System;
using UnityEngine;

public struct WeeklyScore {
    public int highScore;
    public int averageScore;
    public int lowScore;
    public DateTime date;
}

public struct DailyScore {
    public DateTime date;
    public int[] scores;
}

public class ScoreTracker : MonoBehaviour {

    public void SaveScore (int score, int level) {
        // Convert number of colors to level
        level = level - 2;

        // Get todays date
        DateTime todaysDate = DateTime.Today;
        string sTodaysDate = todaysDate.ToString("d");

        // Get last saved score
        string sDailyScore;
        if ( !PlayerPrefs.HasKey ("Daily Score" + level.ToString ()) ) {
            PlayerPrefs.SetString ("Daily Score" + level.ToString (), sTodaysDate + "," + score.ToString ());
        } else {
            sDailyScore = PlayerPrefs.GetString ("Daily Score" + level.ToString ());

            // Convert saved score
            DailyScore savedScore = ConvertStringToDailyScore (sDailyScore);
            string sSavedDate = savedScore.date.ToString("d");

            // Check saved scores date against todays date
            if ( sTodaysDate == sSavedDate ) {
                // If todays date is the same as the saved score, add score to saved score
                PlayerPrefs.SetString ("Daily Score" + level.ToString (), sDailyScore + "," + score);
            } else {
                // Check if todays date is in same week as saved scores week
                int dayOfTheWeek = (int)todaysDate.DayOfWeek;
                if ( todaysDate.Year == savedScore.date.Year &&
                    todaysDate.DayOfYear - dayOfTheWeek <= savedScore.date.DayOfYear ) {
                    // Save score and start new daily score
                    SaveDailyScoreToWeek (score, savedScore, todaysDate, level);
                    PlayerPrefs.SetString ("Daily Score" + level.ToString (), sTodaysDate + "," + score);
                } else {
                    //If date is not from same week
                    SaveDailyScoreToNewWeek (score, todaysDate, level);
                }
            }
        }        
    }

    DailyScore ConvertStringToDailyScore (string s) {
        string[] sArray = s.Split(',');

        // Convert string to integers
        int year, month, day;
        string[] date = sArray[0].Split('/');
        year = int.Parse (date [ 2 ]);
        month = int.Parse (date [ 0 ]);
        day = int.Parse (date [ 1 ]);
        // Convert integers to Date
        DateTime dt = new DateTime(year, month, day);

        // Convert score strings to integers
        int[] scores = new int[sArray.Length - 1];
        for ( int i = 1; i < sArray.Length; i++ ) {
            scores [ i - 1 ] = int.Parse (sArray [ i ]);
        }

        // Convert to Daily Score
        DailyScore dailyScore;
        dailyScore.date = dt;
        dailyScore.scores = scores;

        // Return Daily Score
        return dailyScore;
    }

    void SaveDailyScoreToWeek (int score, DailyScore savedScore, DateTime date, int level) {
        // Set high low and avg
        int high, avg, low;
        high = savedScore.scores [ 0 ];
        low = savedScore.scores [ 0 ];
        avg = 0;
        for ( int i = 0; i < savedScore.scores.Length; i++ ) {
            if ( savedScore.scores [ i ] > high ) {
                high = savedScore.scores [ i ];
            }
            if ( savedScore.scores [ i ] < low ) {
                low = savedScore.scores [ i ];
            }
            avg += savedScore.scores [ i ];
        }
        avg = avg / savedScore.scores.Length;

        string scoreOutput = "";
        if ( PlayerPrefs.GetString ("Weekly Score" + level.ToString ()) != "") {
            // Check last date of scores in week
            string[] week = PlayerPrefs.GetString ("Weekly Score" + level.ToString()).Split(',');
            string[] sSavedDate = week[week.Length - 4].Split('/');
            DateTime savedDate = new DateTime(int.Parse(sSavedDate[2]), int.Parse(sSavedDate[0]), int.Parse(sSavedDate[1]));
            int days = (date - savedDate).Days;
            // Place spacer "0"s if there are gaps in week
            
            if ( days > 1 ) {
                for ( int i = 1; i < days; i++ ) {
                    DateTime newDate = savedDate.AddDays(i);
                    scoreOutput += newDate.ToString ("d") + ",0,0,0";
                }
            }
        } else {
            // Create new weekly score
            SaveDailyScoreToNewWeek (score, date, level);
        }

        // Save Score to week
        scoreOutput += date.ToString ("d") + "," + high + "," + avg + "," + low;
        PlayerPrefs.SetString ("Weekly Score" + level.ToString(), scoreOutput);
    }

    void SaveDailyScoreToNewWeek (int score, DateTime date, int level) {
        // Check days since new week
        int daysSinceSunday = (int)date.DayOfWeek;
        // Place spacer scores
        string scoreOutput = "";
        if (daysSinceSunday > 1) {
            for (int i = daysSinceSunday; i < 0; i-- ) {
                scoreOutput += date.AddDays(-i).ToString("d") + ",0,0,0";
            }
        }
        PlayerPrefs.SetString ("Weekly Score" + level.ToString(), scoreOutput);

        // Start new daily score
        PlayerPrefs.SetString ("Daily Score" + level.ToString(), date.ToString("d") + "," + score);
    }
}
