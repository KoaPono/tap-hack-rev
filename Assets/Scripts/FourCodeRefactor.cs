﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FourCodeRefactor : MonoBehaviour {

    #region Public Variables
    [Header("Position Game Objects")]
    public GameObject[] positionBorders;

    [Header ("Sprites")]
    public Sprite[] correctSprites;
    public Sprite[] incorrectSprites;
    [Space()]
    public Sprite   emptySprite;

    [Space()]
    public Image[] positionSprites;

    [Header ("Audio Source")]
    public AudioSource colorButtonAudioSource;
    public AudioSource gameOverAudioSource;
    public AudioSource completedCodeAudioCource;

    [Header ("Audio Clips")]
    public AudioClip correctSound;
    public AudioClip incorrectSound;
    public AudioClip highscoreSound;
    public AudioClip endGameSound;
    public AudioClip codeCorrectSound;

    [Header ("Text")]
    public Text timerText;
    public Text scoreText;
    public Text finalScore;
    public Text highScoreText;

    [Header ("Confetti")]
    public GameObject confetti1;
    public GameObject confetti2;

    [Header ("End Buttons")]
    public Button restartButton;
    public Button menuButton;

    #endregion

    #region Private Variables
    //Timer Variables
    private float startingTime = 30.0f;
    private float timeRemaining;
    private float addedTime = 3.0f;

    //Game Variables
    [HideInInspector]
    public int numberOfColors;

    private Vector3 normalsize;

    private byte colorsSet;
    private byte positionsSet;

    private List<int> positionColor;
    public List<int> codeColorID;

    private int playerScore;
    private int highscore;

    private bool gameIsPlaying = false;

    private MenuRefactor menuController;
    private ScoreTracker scoreTracker;

    #endregion

    #region Unity Functions

    //Use this for initialization
    void Awake () {
        menuController = GetComponent<MenuRefactor> ();
        scoreTracker = GetComponent<ScoreTracker> ();
    }

    // Update is called once per frame
    void Update () {
        // Subtract time from time remaining
        timeRemaining -= Time.deltaTime;
        timerText.text = timeRemaining.ToString ("f0");

        // Update score text to player score
        scoreText.text = playerScore.ToString ();

        // If time remaining reaches 0, end game
        if ( timeRemaining <= 0 && gameIsPlaying ) {
            gameIsPlaying = false;

            //scoreTracker.SaveScore (playerScore, numberOfColors);

            // Set final score text to player's score
            finalScore.text = playerScore.ToString ();

            // If player has set a new highscore update highscore, save and play fanfare!
            if ( playerScore > highscore ) {

                // Set highscore to new highscore
                highscore = playerScore;

                // Save new Highscore
                PlayerPrefs.SetInt ("highscore" + numberOfColors, playerScore);

                // Update highscore text
                highScoreText.text = playerScore.ToString ();

                // Launch Confetti
                LaunchConfetti ();


                // set highscore sound
                gameOverAudioSource.clip = highscoreSound;

            } else {

                // If player did not set new highscore, play set end game sound
                gameOverAudioSource.clip = endGameSound;
            }

            // Play game over sound
            gameOverAudioSource.Play ();

            // Reset positions and color IDs
            ResetPositions ();

            // Reset text
            timerText.text = "30";
            scoreText.text = "0";

            // Opens Game Over Menu
            menuController.GameOver ();

            // Sets Game Over Buttons to be inactive to prevent accidently clicking them then resets them after 1 second
            restartButton.interactable = false;
            menuButton.interactable = false;
            StartCoroutine (EnableGameOverButtons ());
        }
    }

    #endregion

    #region new refactor

    public void StartGame () {
        //Initialize list with "zero values"
        if ( positionColor == null ) {
            positionColor = new List<int> ();
        } else {
            positionColor.Clear ();
        }
        for ( int i = 0; i < numberOfColors; i++ ) {
            positionColor.Add (numberOfColors + 1);
        }

        //Initialize list with "zero values"
        if ( codeColorID == null ) {
            codeColorID = new List<int> ();
        } else {
            codeColorID.Clear ();
        }
        for ( int i = 0; i < numberOfColors; i++ ) {
            codeColorID.Add (numberOfColors + 1);
        }

        normalsize = positionBorders [ 0 ].transform.localScale;

        StartTimer ();

        GenerateCode ();

        gameIsPlaying = true;


        if ( PlayerPrefs.HasKey ("highscore" + numberOfColors) ) {
            highscore = PlayerPrefs.GetInt ("highscore" + numberOfColors);
            highScoreText.text = highscore.ToString ();
        } else {
            PlayerPrefs.SetInt ("highscore" + numberOfColors, 0);
        }
    }

    public void Restart () {
        //Reset Game
        playerScore = 0;
        timerText.text = startingTime.ToString ();
        scoreText.text = "0";

        ResetPositions ();
    }

    public void ResetStats () {
        PlayerPrefs.SetInt ("highscore2", 0);
        PlayerPrefs.SetInt ("highscore3", 0);
        PlayerPrefs.SetInt ("highscore4", 0);
        PlayerPrefs.SetInt ("highscore5", 0);
        PlayerPrefs.SetInt ("highscore6", 0);

        highscore = 0;
        highScoreText.text = highscore.ToString ();

        PlayerPrefs.DeleteKey ("Daily Score0");
        PlayerPrefs.DeleteKey ("Daily Score1");
        PlayerPrefs.DeleteKey ("Daily Score2");
        PlayerPrefs.DeleteKey ("Daily Score3");
        PlayerPrefs.DeleteKey ("Daily Score4");
        PlayerPrefs.DeleteKey ("Daily Score5");

        PlayerPrefs.DeleteKey ("Weekly Score0");
        PlayerPrefs.DeleteKey ("Weekly Score1");
        PlayerPrefs.DeleteKey ("Weekly Score2");
        PlayerPrefs.DeleteKey ("Weekly Score3");
        PlayerPrefs.DeleteKey ("Weekly Score4");
        PlayerPrefs.DeleteKey ("Weekly Score5");
    }

    // Resets all color assets
    public void ResetPositions () {

        // Resets Colors that are set
        colorsSet = 0;

        // Resets positions that are set
        positionsSet = 0;

        // Resets color IDs
        for ( int i = 0; i < numberOfColors; i++ ) {
            positionColor [ i ] = numberOfColors + 1;
        }

        // Reset Image Sprites
        foreach ( Image img in positionSprites ) {
            img.sprite = emptySprite;
        }
    }

    // 0 = purple, 1 = yellow, 2 = green, 3 = blue
    public void PressColor (int colorID) {
        // Check if all positions are filled
        if ( GetBit (positionsSet, numberOfColors - 1) ) {
            ResetPositions ();

            // If not all positions are filled, check to see if color has been placed
        } else if ( !GetBit (colorsSet, colorID) ) {
            // Loop through each position until an empty position is found
            for ( int i = 0; i < numberOfColors; i++ ) {
                //Check if position is empty
                if ( !GetBit (positionsSet, i) ) {

                    // Set position to "filled"
                    SetBit (ref positionsSet, i, true);

                    // Sets color to placed
                    SetBit (ref colorsSet, colorID, true);

                    // Sets position color to color ID
                    positionColor [ i ] = colorID;

                    // If position color id is equal to code color id, place "correct" white center sprite
                    if ( positionColor [ i ] == codeColorID [ i ] ) {
                        positionSprites [ i ].sprite = correctSprites [ colorID ];

                        // If placing color on any position other than the last, play correct sound
                        if ( i != 3 ) {
                            // Switch sound clip to "correct clip" and play sound
                            colorButtonAudioSource.clip = correctSound;
                            colorButtonAudioSource.Play ();
                        }

                        // Visual cue to show player button has been pressed
                        StartCoroutine (Resize (positionBorders [ i ]));

                        // If position color id is not equal to code color id, place "incorrect" normal center sprite
                    } else {
                        positionSprites [ i ].sprite = incorrectSprites [ colorID ];

                        // Switch sound clip to "incorrect clip" and play sound
                        colorButtonAudioSource.clip = incorrectSound;
                        colorButtonAudioSource.Play ();
                    }

                    // Checks if final position is set
                    if ( GetBit (positionsSet, numberOfColors - 1) ) {

                        //If all color IDs match code color IDs, 
                        if ( CheckCode () ) {

                            // Resize all positions
                            for ( int j = 0; j < numberOfColors - 1; j++ ) {
                                StartCoroutine (Resize (positionBorders [ j ]));
                            }

                            // Add to player score
                            playerScore++;

                            // Regenerate a new puzzle
                            GenerateCode ();

                            if ( numberOfColors < 5 && playerScore == 15 ) {
                                addedTime = 1;
                            }

                            // Add time to player timer
                            timeRemaining += addedTime;

                            // Reset all positions and color IDs
                            ResetPositions ();

                            // Play code complete sound
                            completedCodeAudioCource.Play ();
                        }
                    }

                    // Once the position is filled break out of loop
                    return;
                }
            }
        }
    }

    bool CheckCode () {
        for ( int i = 0; i < numberOfColors; i++ ) {
            if ( positionColor [ i ] != codeColorID [ i ] ) {
                return false;
            }
        }
        return true;
    }

    //Starts Game Timer
    void StartTimer () {
        timeRemaining = startingTime;
    }

    //Generates a 4 digit code with no repeating digits
    void GenerateCode () {
        if ( numberOfColors != 2 ) {
            //Fills array with all used values
            for ( int i = 0; i < numberOfColors; i++ ) {
                codeColorID [ i ] = i;
            }

            //Shuffles array using Fisher-Yates shuffle
            for ( int i = 0; i < codeColorID.Count; i++ ) {
                int r = UnityEngine.Random.Range (0, codeColorID.Count- 1);
                int tempColorID = codeColorID[r];
                codeColorID [ r ] = codeColorID [ i ];
                codeColorID [ i ] = tempColorID;
            }
        } else {
            int r = UnityEngine.Random.Range (0, 2);
            codeColorID [ 0 ] = r;
            codeColorID [ 1 ] = ( r == 1 ) ? 0 : 1;
        }
    }

    // Turns on particle systems to "launch confetti"
    void LaunchConfetti () {
        confetti1.SetActive (true);
        confetti2.SetActive (true);

        // Waits for confetti to finish then resets paritcle systems
        StartCoroutine (ResetConfetti ());
    }

    // Gets a bit at a specific location in a byte
    bool GetBit (byte b, int pos) {
        return ( b & ( 1 << pos ) ) != 0;
    }

    // Sets a bit at a specific position in a byte
    void SetBit (ref byte b, int pos, bool value) {
        if ( value ) {
            //left-shift 1, then bitwise OR
            b = (byte) ( b | ( 1 << pos ) );
        } else {
            //left-shift 1, then take complement, then bitwise AND
            b = (byte) ( b & ~( 1 << pos ) );
        }
    }

    #region Coroutines

    // Resizes borders to pop!
    IEnumerator Resize (GameObject position) {
        float expandNumber = normalsize.x + (normalsize.x * 0.3f);
        Vector3 expandedSize = new Vector3 (expandNumber, expandNumber, 1);

        // Sets size to larger size
        position.transform.localScale = expandedSize;

        // Waits 0.2 seconds
        yield return new WaitForSeconds (0.2f);

        // Returns to normal size
        position.transform.localScale = normalsize;
    }

    // Waits for confetti to finish then resets paritcle systems
    IEnumerator ResetConfetti () {
        // Waits for 7 seconds
        yield return new WaitForSeconds (7f);

        // Turns off particle systems "resetting" them
        confetti1.SetActive (false);
        confetti2.SetActive (false);
    }

    // Reenables game over screen buttons after wait time to prevent unwanted clicking of buttons
    IEnumerator EnableGameOverButtons () {
        // Wait for one second
        yield return new WaitForSeconds (1f);

        // Enable buttons
        restartButton.interactable = true;
        menuButton.interactable = true;
    }

    #endregion

    #endregion
}
