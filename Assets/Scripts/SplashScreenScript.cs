﻿using System.Collections;
using UnityEngine;

public class SplashScreenScript : MonoBehaviour {

    public GameObject sharkAnimation;

    bool splashStarted = false;
    bool isFading = false;
    Color fade;

    private void Start () {
        fade = Color.white;
    }

    // Update is called once per frame
    void FixedUpdate () {
        while ( Application.isShowingSplashScreen ) {
            return;
        }
        if ( !splashStarted ) {
            splashStarted = true;
            StartCoroutine (WaitStartLogoAnimation ());
            StartCoroutine (LoadNextScene ());
            StartCoroutine (FadeShark ());
        }
        if (isFading) {
            fade.a -= 0.04f;
            sharkAnimation.GetComponent<SpriteRenderer> ().color = fade;
        }
    }

    IEnumerator WaitStartLogoAnimation () {
        yield return new WaitForSeconds (0.5f);
        sharkAnimation.SetActive (true);
    }

    IEnumerator LoadNextScene () {
        yield return new WaitForSeconds (3.5f);
        Application.LoadLevel (1);
    }

    IEnumerator FadeShark () {
        yield return new WaitForSeconds (3f);
        isFading = true;
    }
}
