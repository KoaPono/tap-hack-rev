﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grapher : MonoBehaviour {
    public GameObject noScoresText;
    public Text LevelIndicator;

    public WMG_Axis_Graph graph;

    public int level = 0;

    private List<Vector2> series1Data;
    private List<Vector2> series2Data;
    private List<Vector2> series3Data;
    private WMG_Series series1;
    private WMG_Series series2;
    private WMG_Series series3;

    private void Start () {
        series1Data = new List<Vector2> ();
        series2Data = new List<Vector2> ();
        series3Data = new List<Vector2> ();
    }

    public void GetDailyScores () {
        // Clear Old data
        series1Data.Clear ();

        // Get todays date
        DateTime today = DateTime.Today;

        // Check if scores are from today
        string dailyScoreString = PlayerPrefs.GetString("Daily Score" + level.ToString());
        string [ ] dailyScores = dailyScoreString.Split(',');
        if ( today.ToString ("d") == dailyScores [ 0 ] ) {
            noScoresText.SetActive (false);

            // Save scores to list
            for ( int i = 1; i < dailyScores.Length; i++ ) {
                Vector2 score = new Vector2();
                score.x = i;
                score.y = int.Parse (dailyScores [ i ]);
                series1Data.Add (score);
            }

            // Update graph with list
            if ( graph.lineSeries.Count < 1 ) {
                series1 = graph.addSeries ();
            } else {
                for ( int i = 0; i < graph.lineSeries.Count; i++ ) {
                    graph.deleteSeries ();
                }
                series1 = graph.addSeries ();
            }

            series1.pointWidthHeight = 30;
            series1.UseXDistBetweenToSpace = true;
            series1.AutoUpdateXDistBetween = true;

            series1.pointValues.SetList (series1Data);
        } else {
            noScoresText.SetActive (true);
        }
        graph.xAxis.hideLabels = true;
        graph.useGroups = false;
    }

    public void GetWeeklyScores () {
        // Clear Old data
        series1Data.Clear ();
        series2Data.Clear ();
        series3Data.Clear ();

        if ( PlayerPrefs.GetString ("Weekly Score" + level.ToString ()) != "") {
            // Get todays date
            DateTime today = DateTime.Today;

            // Check if scores are from last week
            string weeklyScoreString = PlayerPrefs.GetString("Weekly Score" + level.ToString());
            string [] weeklyScores = weeklyScoreString.Split(',');
            string [] sLastSavedDate = weeklyScores[0].Split('/');
            int day, month, year;
            month = int.Parse (sLastSavedDate [ 0 ]);
            day = int.Parse (sLastSavedDate [ 1 ]);
            year = int.Parse (sLastSavedDate [ 2 ]);
            DateTime lastSavedDate = new DateTime(year, month, day);
            if ( today.Year == lastSavedDate.Year &&
                 today.DayOfYear - (int) today.DayOfWeek - 7 <= lastSavedDate.DayOfYear) {

                // Save scores to list
                for ( int i = 1; i < weeklyScores.Length; i += 3 ) {
                    series1Data.Add (new Vector2 (series1Data.Count + 1, int.Parse (weeklyScores [ i ])));
                    series2Data.Add (new Vector2 (series1Data.Count + 1, int.Parse (weeklyScores [ i + 1 ])));
                    series2Data.Add (new Vector2 (series1Data.Count + 1, int.Parse (weeklyScores [ i + 2 ])));
                }

                // Update graph with list
                if ( graph.lineSeries.Count == 0 ) {
                    series1 = graph.addSeries ();
                    series2 = graph.addSeries ();
                    series3 = graph.addSeries ();
                } else {
                    for ( int i = 0; i < graph.lineSeries.Count; i++ ) {
                        graph.deleteSeries ();
                    }
                    series1 = graph.addSeries ();
                    series2 = graph.addSeries ();
                    series3 = graph.addSeries ();
                }

                series1.pointWidthHeight = 30;
                series1.UseXDistBetweenToSpace = false;
                series1.pointColor = Color.green;

                series2.pointWidthHeight = 30;
                series2.UseXDistBetweenToSpace = false;
                series2.pointColor = Color.yellow;

                series3.pointWidthHeight = 30;
                series3.UseXDistBetweenToSpace = false;
                series3.pointColor = Color.red;

                series1.pointValues.SetList (series1Data);
                if ( graph.lineSeries.Count > 1 ) {
                    series2.pointValues.SetList (series2Data);
                    series3.pointValues.SetList (series3Data);
                }
            } else {
                noScoresText.SetActive (true);
            }
        } else {
            noScoresText.SetActive (true);
        }

        graph.xAxis.hideLabels = false;
        graph.useGroups = true;
    }

    public void PreviousLevel () {
        if ( level > 0 ) {
            level--;
        }
        LevelIndicator.text = "Level " + level.ToString ();
        GetDailyScores ();
    }

    public void NextLevel () {
        if ( level < 5 ) {
            level++;
        }
        LevelIndicator.text = "Level " + level.ToString ();
        GetDailyScores ();
    }

}
