﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AnimationSounds : MonoBehaviour {

    public AudioClip swish;
    public AudioClip coin;
    public AudioSource source;

    private float volumeUp = 0;
    bool left = false;

    public void PlaySwishSound() {
        // Slowly increase volume and pan sound
        volumeUp += 0.13f;
        source.volume = Mathf.Pow (volumeUp, 1 / 1.15f) * ( Mathf.PI / 2 );
        source.panStereo = left ? 0.5f : -0.5f;
        left = !left;

        source.clip = swish;
        source.Play ();
    }

    public void PlayCoinSound() {
        source.volume = 0.5f;
        source.clip = coin;
        source.Play ();
    }
}
