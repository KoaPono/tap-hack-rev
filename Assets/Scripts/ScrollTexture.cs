﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;

    private Vector2 uvOffset = Vector2.zero;
    private MeshRenderer rend;
    private Material mat;

    void Start () {
        rend = GetComponent<MeshRenderer> ();
        mat = rend.material;
    }

	// Update is called once per frame
	void Update () {
        uvOffset.x += xSpeed;
        uvOffset.y += ySpeed;

        mat.mainTextureOffset = uvOffset;
	}
}
