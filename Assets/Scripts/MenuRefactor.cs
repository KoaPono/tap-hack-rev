﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuRefactor : MonoBehaviour {

    [Header("Menu Objects")]
    public GameObject gameMenu;
    public GameObject gameItems;
    public GameObject gameOver;
    public GameObject optionsMenu;
    public GameObject creditsMenu;
    public GameObject yesNoMenu;
    public GameObject levelMenu;
    public GameObject tutorial;
    public GameObject stats;

    [Header("Buttons")]
    public GameObject color1Button;
    public GameObject color2Button;
    public GameObject color3Button;
    public GameObject color4Button;
    public GameObject color5Button;
    public GameObject color6Button;

    [Header("Positions")]
    public HorizontalLayoutGroup layoutGroup;
    public GameObject pos1;
    public GameObject pos2;
    public GameObject pos3;
    public GameObject pos4;
    public GameObject pos5;
    public GameObject pos6;

    [Header("Sound")]
    public AudioSource buttonSound;
    public AudioSource musicSource;
    public AudioMixer mixer;

    [Header("Toggles")]
    public Toggle soundFX;
    public Toggle music;

    FourCodeRefactor controller;
    TutorialScript tutorialScript;
    Grapher graphScript;

    void Start () {
        controller = GetComponent<FourCodeRefactor> ();
        tutorialScript = GetComponent<TutorialScript> ();
        graphScript = GetComponent<Grapher> ();

        if ( PlayerPrefs.HasKey ("soundFX") ) {
            if ( PlayerPrefs.GetInt ("soundFX") == 1 ) {
                soundFX.isOn = true;
                mixer.SetFloat ("volume", 0f);
            } else {
                soundFX.isOn = false;
                mixer.SetFloat ("volume", -80f);
            }
        } else {
            PlayerPrefs.SetInt ("soundFX", 1);
        }

        if ( PlayerPrefs.HasKey ("music") ) {
            if ( PlayerPrefs.GetInt ("music") == 1 ) {
                music.isOn = true;
                musicSource.mute = false;
            } else {
                music.isOn = false;
                musicSource.mute = true;
            }
        } else {
            PlayerPrefs.SetInt ("music", 1);
        }

        gameMenu.SetActive (true);
        gameItems.SetActive (false);
        gameOver.SetActive (false);
        optionsMenu.SetActive (false);
        creditsMenu.SetActive (false);
        levelMenu.SetActive (false);
        tutorial.SetActive (false);
        stats.SetActive (false);

        ResetButtons ();
    }

    public void ChangeSoundFX (bool set) {
        if ( set ) {
            PlayerPrefs.SetInt ("soundFX", 1);
            mixer.SetFloat ("volume", 0f);
            buttonSound.Play ();
        } else {
            PlayerPrefs.SetInt ("soundFX", 0);
            mixer.SetFloat ("volume", -80f);
        }
    }

    public void ChangeMusic (bool set) {
        if ( set ) {
            PlayerPrefs.SetInt ("music", 1);
            musicSource.mute = false;
        } else {
            PlayerPrefs.SetInt ("music", 0);
            musicSource.mute = true;
        }
    }

    public void PlayGame () {
        buttonSound.Play ();

        gameMenu.SetActive (false);
        levelMenu.SetActive (true);
    }

    public void OnLevel0 () {
        controller.numberOfColors = 2;

        layoutGroup.spacing = -50;

        // Set button positions
        color1Button.transform.localPosition = new Vector3 (-200f, -150f, 0);
        color2Button.transform.localPosition = new Vector3 (200f, -150f, 0);

        // Turn off unused positions
        pos3.SetActive (false);
        pos4.SetActive (false);
        pos5.SetActive (false);
        pos6.SetActive (false);

        // Turn off unused buttons
        color3Button.SetActive (false);
        color4Button.SetActive (false);
        color5Button.SetActive (false);
        color6Button.SetActive (false);

        buttonSound.Play ();

        levelMenu.SetActive (false);
        gameItems.SetActive (true);
        controller.StartGame ();
        controller.Restart ();
    }

    public void OnLevel1() {
        controller.numberOfColors = 3;

        // Turn off unused positions
        pos4.SetActive (false);
        pos5.SetActive (false);
        pos6.SetActive (false);

        // Turn off unused buttons
        color4Button.SetActive (false);
        color5Button.SetActive (false);
        color6Button.SetActive (false);

        // Set button positions
        color2Button.transform.localPosition = new Vector3 (200f, -239f, 0);

        buttonSound.Play ();

        levelMenu.SetActive (false);
        gameItems.SetActive (true);
        controller.StartGame ();
        controller.Restart ();
    }

    public void OnLevel2 () {
        controller.numberOfColors = 4;

        // Turn off unused positions
        pos5.SetActive (false);
        pos6.SetActive (false);

        // Turn off unused buttons
        color6Button.SetActive (false);
        color5Button.SetActive (false);
        color4Button.SetActive (true);

        buttonSound.Play ();

        levelMenu.SetActive (false);
        gameItems.SetActive (true);
        controller.StartGame ();
        controller.Restart ();
    }

    public void OnLevel3 () {
        controller.numberOfColors = 5;

        pos6.SetActive (false);

        // Turn off unused buttons
        color6Button.SetActive (false);

        // Set button positions
        color1Button.transform.localPosition = new Vector3 (-230f,  -31f, 0);
        color2Button.transform.localPosition = new Vector3 ( 230f,  -31f, 0);
        color3Button.transform.localPosition = new Vector3 (-230f, -490f, 0);
        color4Button.transform.localPosition = new Vector3 ( 230f, -490f, 0);
        color5Button.transform.localPosition = new Vector3 (   0f, -260f, 0);

        // Set button rotation
        color1Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.4f, 0.9f);
        color2Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.4f, 0.9f);
        color3Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.4f, 0.9f);
        color4Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.4f, 0.9f);
        color5Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.4f, 0.9f);

        // Set position scale
        pos1.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
        pos2.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
        pos3.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
        pos4.transform.localScale = new Vector3 (0.9f, 0.9f, 1);

        buttonSound.Play ();

        levelMenu.SetActive (false);
        gameItems.SetActive (true);
        controller.StartGame ();
        controller.Restart ();
    }

    public void OnLevel4 () {
        controller.numberOfColors = 6;

        // Set button positions
        color1Button.transform.localPosition = new Vector3 (-200f,   25f, 0);
        color2Button.transform.localPosition = new Vector3 ( 200f,   25f, 0);
        color3Button.transform.localPosition = new Vector3 (-200f, -250f, 0);
        color4Button.transform.localPosition = new Vector3 ( 200f, -250f, 0);
        color5Button.transform.localPosition = new Vector3 (-200f, -530f, 0);
        color6Button.transform.localPosition = new Vector3 ( 200f, -530f, 0);

        // Set button scale
        color1Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
        color2Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
        color3Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
        color4Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
        color5Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
        color6Button.transform.localScale = new Vector3 (0.85f, 0.85f, 1);

        // Set position scale
        pos1.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
        pos2.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
        pos3.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
        pos4.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
        pos5.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
        pos6.transform.localScale = new Vector3 (0.7f, 0.7f, 1);

        layoutGroup.spacing = -25;

        buttonSound.Play ();

        levelMenu.SetActive (false);
        gameItems.SetActive (true);
        controller.StartGame ();
        controller.Restart ();
    }

    public void GameOver () {
        gameItems.SetActive (false);
        gameOver.SetActive (true);
    }

    public void Menu () {
        buttonSound.Play ();

        gameOver.SetActive (false);
        optionsMenu.SetActive (false);
        creditsMenu.SetActive (false);
        gameMenu.SetActive (true);
        tutorial.SetActive (false);
        stats.SetActive (false);

        ResetButtons ();
    }

    public void Options () {
        buttonSound.Play ();

        optionsMenu.SetActive (true);
        gameMenu.SetActive (false);
        yesNoMenu.SetActive (false);
    }

    public void Credits () {
        buttonSound.Play ();

        creditsMenu.SetActive (true);
        gameMenu.SetActive (false);
    }

    public void RestartGame () {
        buttonSound.Play ();

        gameOver.SetActive (false);
        gameItems.SetActive (true);
        controller.Restart ();
        controller.StartGame ();
    }

    public void AreYouSure () {
        buttonSound.Play ();

        optionsMenu.SetActive (false);
        yesNoMenu.SetActive (true);
    }

    public void Tutorial () {
        buttonSound.Play ();

        tutorial.SetActive (true);
        gameMenu.SetActive (false);
        tutorialScript.StartTutorial ();
    }

    public void Stats () {
        buttonSound.Play ();

        stats.SetActive (true);
        gameMenu.SetActive (false);

        graphScript.GetDailyScores ();
    }

    public void PlayButtonSound() {
        buttonSound.Play ();
    }

    public void ResetButtons () {
        // Turn on all buttons by default
        color4Button.SetActive (true);
        color5Button.SetActive (true);

        // Set button positions
        color1Button.transform.localPosition = new Vector3 (-200f, -51f,  0);
        color2Button.transform.localPosition = new Vector3 ( 200f, -51f,  0);
        color3Button.transform.localPosition = new Vector3 (-200f, -428f, 0);
        color4Button.transform.localPosition = new Vector3 ( 200f, -428f, 0);
        color5Button.transform.localPosition = new Vector3 (-200f, -800f, 0);
        color6Button.transform.localPosition = new Vector3 ( 200f, -800f, 0);

        // Set position scale
        color1Button.transform.localScale = Vector3.one;
        color2Button.transform.localScale = Vector3.one;
        color3Button.transform.localScale = Vector3.one;
        color4Button.transform.localScale = Vector3.one;
        color5Button.transform.localScale = Vector3.one;
        color6Button.transform.localScale = Vector3.one;

        // Set button Rotations
        color1Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);
        color2Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);
        color3Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);
        color4Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);
        color5Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);
        color6Button.transform.rotation = new Quaternion (0.0f, 0.0f, 0.0f, 1f);

        // Turn on all positions by default
        pos4.SetActive (true);
        pos5.SetActive (true);
        pos6.SetActive (true);

        // Set position scale
        pos1.transform.localScale = Vector3.one;
        pos2.transform.localScale = Vector3.one;
        pos3.transform.localScale = Vector3.one;
        pos4.transform.localScale = Vector3.one;

        //Set position sizes
        pos1.transform.localScale = Vector3.one;
        pos2.transform.localScale = Vector3.one;
        pos3.transform.localScale = Vector3.one;
        pos4.transform.localScale = Vector3.one;
    }
}
