﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour {

    public Image background;
    public Image logo;

    public GameObject source;

    private Color fade = Color.white;
    private bool isFading = false;

    private void Start () {
        isFading = true;
    }

 

	void FixedUpdate () {
		if (isFading) {
            fade.a -= 0.03f;
            background.color = fade;
            //logo.color = fade;
            if (fade.a <= 0) {
                source.SetActive (true);
                isFading = false;
            }
        }
	}
}
